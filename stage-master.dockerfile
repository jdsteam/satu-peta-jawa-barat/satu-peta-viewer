FROM  node:14.15.0-alpine AS build-master
WORKDIR /app/
COPY package*.json /app/
RUN apk update && apk upgrade && apk add --no-cache git
RUN npm install -g @angular/cli 
RUN npm install
COPY ./ /app/
ARG configuration=production
RUN npm run build-prod


FROM nginx:1.13.3-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build-master /app/dist/out/ /usr/share/nginx/html/
