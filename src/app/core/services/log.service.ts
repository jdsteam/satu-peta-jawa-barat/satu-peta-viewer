import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { isPlatformBrowser } from '@angular/common';
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

import { CookieService } from 'ngx-cookie';

@Injectable({
  providedIn: 'root',
})
export class LogService {
  // Variable
  public isBrowser: any;
  random: number;
  index: number;
  token: string;
  log: {};

  // API URL
  ipUrl = [
    {
      name: 'ipapi',
      url: 'https://jsonip.com',
    },
  ];

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({}),
  };

  constructor(
    private http: HttpClient,
    @Inject(PLATFORM_ID) private platformId,
    private cookieService: CookieService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  getIP(): Observable<any> {
    try {
      return new Observable((observer) => {
        fetch('https://jsonip.com', { mode: 'cors' })
          .then((resp) => resp.json())
          .then((res) => {
            observer.next({
              uuid: uuidv4(),
              ip: res.ip,
              country_code: null,
              country_name: null,
              region_code: null,
              region_name: null,
              city_code: null,
              city_name: null,
              latitude: null,
              longitude: null,
            });
            observer.complete();
          });
      });
    } catch (error) {
      return of({
        uuid: uuidv4(),
        ip: null,
        country_code: null,
        country_name: null,
        region_code: null,
        region_name: null,
        city_code: null,
        city_name: null,
        latitude: null,
        longitude: null,
      });
    }
  }
  // tslint:disable-next-line: typedef
  errorHandler(error: HttpErrorResponse) {
    return throwError({
      uuid: uuidv4(),
      ip: null,
      country_code: null,
      country_name: null,
      region_code: null,
      region_name: null,
      city_code: null,
      city_name: null,
      latitude: null,
      longitude: null,
    });
  }

  getDevice(device: any): any {
    return {
      user_agent: device.userAgent,
      os: device.os,
      browser: device.browser,
      device: device.device,
      os_version: device.os_version,
      browser_version: device.browser_version,
      device_type: device.deviceType,
      orientation: device.orientation,
      is_desktop: device.isDesktop,
      is_mobile: device.isMobile,
      is_tablet: device.isTablet,
    };
  }

  getLog() {
    return this.log;
  }

  getToken() {
    return this.token;
  }

  setLog(data) {
    this.log = data;
  }

  setToken(data) {
    this.token = data;
  }
}
