import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';
import * as moment from 'moment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  apiUrl = `${environment.apiData}`;
  redirectUrl: string;

  constructor(private cookieService: CookieService, private http: HttpClient) {}

  login(username, pass) {
    return this.http
      .post<any>(this.apiUrl + 'auth/login2', { username, password: pass, app_code: environment.appCode })
      .pipe(
        map((user) => {
          console.log(user);
          this.setSession(user.data, user.data.jwt);
          return user;
        })
      );
  }

  setSession(user, token) {
    localStorage.setItem('satudata-user', JSON.stringify(user));
    localStorage.setItem('satudata-token', token);
  }

  logout() {
    this.cookieService.delete('arcgis-token');

    // remove user from local storage to log user out
    localStorage.removeItem('satudata-user');

    // remove jwt from local storage to log user out
    localStorage.removeItem('satudata-token');
    return true;
  }

  public isLoggedIn() {
    const currentTime = moment().unix();
    const expiresAt = this.getExpiration();
    if (currentTime < expiresAt) {
      return true;
    } else {
      return false;
    }
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const token = localStorage.getItem('satudata-token');

    if (token === null) {
      return 0;
    } else {
      const tokenDetail = jwt_decode(token);

      const expiration = tokenDetail['exp'];
      return JSON.parse(expiration);
    }
  }
}
