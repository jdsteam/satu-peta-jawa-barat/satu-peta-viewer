import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MapsetLawService {
  apiUrl = `${environment.apiData}mapset_law`;
  token = localStorage.getItem('satudata-token');
  headerDict = {
    Authorization: 'Bearer ' + this.token,
    'Content-Type': 'application/json',
  };

  httpOptions = {
    headers: new HttpHeaders(this.headerDict),
  };

  constructor(public http: HttpClient) {}

  getItem(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + params, this.httpOptions);
  }

  getTotal(params: any): Observable<any> {
    return this.http
      .get<any>(`${this.apiUrl + params}&count=true`, this.httpOptions);
  }
}
