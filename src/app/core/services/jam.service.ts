import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class JamService {
  apiUrl = `${environment.apiWaze}jams-today`;
  headerDict = {
    'Content-Type': 'application/json',
    'api-key': environment.apiWazeKey,
  };

  httpOptions = {
    headers: new HttpHeaders(this.headerDict),
  };

  constructor(public http: HttpClient) {}

  getItem(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + params, this.httpOptions);
  }
}
