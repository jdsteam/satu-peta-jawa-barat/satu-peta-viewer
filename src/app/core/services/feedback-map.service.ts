import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FeedbackMapService {
  // API URL
  apiUrl = `${environment.apiData}feedback_map`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  postFeedback(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}`, JSON.stringify(data), this.httpOptions);
  }

  getInfo(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/info`, this.httpOptions);
  }
}
