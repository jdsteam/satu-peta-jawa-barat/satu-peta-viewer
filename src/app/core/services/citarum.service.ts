import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CitarumService {
  apiUrl = `${environment.apiCitarum}`;
  apiKey = `${environment.apiCitarumKey}`;

  constructor(public http: HttpClient) {}

  getItem(url: string = ''): Observable<any> {
    return this.http.post<any>(url, { app_key: this.apiKey });
  }
}
