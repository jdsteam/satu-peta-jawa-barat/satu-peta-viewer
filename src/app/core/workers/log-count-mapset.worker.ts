/// <reference lib="webworker" />
import { environment } from 'src/environments/environment';
import API from 'fetch-worker';

const apiUrl = `${environment.apiData}`;

addEventListener('message', ({ data }) => {
  API.fetch(`${apiUrl}mapset/counter/${data}?category=view`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });
});
