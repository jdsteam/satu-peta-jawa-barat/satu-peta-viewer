/// <reference lib="webworker" />
import { environment } from 'src/environments/environment';
import API from 'fetch-worker';

const apiUrl = `${environment.apiLog}`;

addEventListener('message', ({ data }) => {
  API.fetch(`${apiUrl}satupeta/page`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + data.token,
    },
    body: JSON.stringify(data.log),
  });
});
