import { Component } from '@angular/core';

@Component({
  selector: 'app-container-home',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
})
export class ContainerHomeComponent {}
