import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-navbar-home',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarHomeComponent implements OnInit {
  isMenuCollapsed = true;
  menu = [
    {
      name: 'Dataset Geospasial Populer',
      url: 'dataset-popular',
      isScrollTo: true,
      children: [],
    },
    {
      name: 'Story Maps',
      url: 'story-maps',
      isScrollTo: true,
      children: [],
    },
    {
      name: 'Penghargaan',
      url: 'appreciation',
      isScrollTo: true,
      children: [],
    },
    {
      name: 'Produk Hukum',
      url: 'law',
      isScrollTo: true,
      children: [],
    },
    {
      name: 'Mitra',
      url: 'partner',
      isScrollTo: true,
      children: [],
    },
    {
      name: 'Peta Tematik',
      url: '/thematic',
      isScrollTo: false,
      children: [],
    },
    {
      name: 'Penjaminan Kualitas',
      isScrollTo: false,
      children: [
        {
          name: 'Sistem Informasi Penjaminan Kualitas',
          url: environment.sipentasURL,
        },
        {
          name: 'Sistem Katalog Metadata Berbasis Informasi Geospasial',
          url: environment.sikambingURL,
        },
      ],
    },
  ];

  constructor(private router: Router) {}

  ngOnInit() {}

  scrollToSection(className: string): void {
    if (this.router.url !== '/home') {
      this.router.navigate(['/home']);
    }

    setTimeout(() => {
      const element = document.querySelector('.' + className) as HTMLElement;
      if (element !== null) {
        window.scrollTo({
          top: element.offsetTop - 80,
          behavior: 'smooth',
        });
      }
    }, 200);
  }
}
