import { Component, OnInit } from '@angular/core';
import { faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-footer-home',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterHomeComponent implements OnInit {
  icons = {
    faPhone,
    faEnvelope,
  };

  constructor() {}

  ngOnInit() {}
}
