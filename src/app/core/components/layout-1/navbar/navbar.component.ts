import { Component, OnInit } from '@angular/core';
import { NavbarItem } from './item/item';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  isMenuCollapsed = true;
  menu: any;
  constructor(public navbarItem: NavbarItem) {}

  ngOnInit() {
    this.menu = this.navbarItem.getData();
  }
}
