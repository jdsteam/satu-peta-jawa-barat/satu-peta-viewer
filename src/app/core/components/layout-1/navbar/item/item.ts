import { Injectable } from '@angular/core';

const ITEM = [
  {
    name: 'Tentang Satu Peta Jabar',
    url: 'about',
    children: [],
  },
  {
    name: 'Bantuan',
    url: 'help',
    children: [],
  },
];

@Injectable()
export class NavbarItem {
  getData() {
    return ITEM;
  }
}
