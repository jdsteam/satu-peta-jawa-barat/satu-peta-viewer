import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import {
  faChevronRight,
  faChevronLeft,
  faSearch,
  faEnvelope,
  faPhone,
} from '@fortawesome/free-solid-svg-icons';

// PLUGIN
import { Store, select } from '@ngrx/store';

import * as _ from 'lodash';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginationComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @Input() type: string;

  @Input() set filterInput(value: any) {
    this.filter = value;
  }
  get filterInput() {
    return this.filter;
  }

  @Output() filterOutputPerPage = new EventEmitter<any>();
  defaultInputs$ = new BehaviorSubject<any>({
    type: null,
    filterInput: null,
  });

  // Variable
  filter: any;
  per10to25Option: any[] = [
    { value: 10, label: '10' },
    { value: 20, label: '20' },
  ];
  pageNumberOption: any[] = [{ value: 1, label: '1' }];
  icons = {
    faChevronLeft,
    faChevronRight,
  };

  // Data
  isLoadingList$: Observable<boolean>;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<any>
  ) {}

  ngOnInit(): void {
    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...this.checkInputs() });

    this.getPagination();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      if (item === 'filterInput') {
        this.filter = changes[item].currentValue;
        this.getPagination();
      }

      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
  }

  ngOnDestroy(): void {
    this.filter.perPage = null;
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  checkInputs(): any {
    const inputs = {};

    if (!_.isNil(this.type)) {
      _.assign(inputs, { type: this.type });
    }

    if (!_.isNil(this.filterInput)) {
      _.assign(inputs, { filterInput: this.filterInput });
    }

    return inputs;
  }

  getPagination() {
    const pageNumberOption = [];
    for (let i = 1; i <= this.filter.totalPage; i++) {
      pageNumberOption.push({ value: i, label: i });
    }
    this.pageNumberOption = pageNumberOption;
  }

  getPaginationCoreData() {
    // this.isLoadingList$ = this.store.pipe(select(selectCoreDataIsLoadingList));
    // this.pagination$ = this.store.pipe(select(selectCoreDataPagination));
  }

  filterPerPage(event: any) {
    const value = _.isEmpty(event) === false ? event.value : null;

    this.filter.perPage = value;
    this.filter.currentPage = 1;
    this.filterOutputPerPage.emit(this.filter);
  }

  filterPage(event: any) {
    const value = _.isEmpty(event) === false ? event.value : null;

    this.filter.currentPage = value;
    // this.filterOutputPage.emit(this.filter);
  }
}
