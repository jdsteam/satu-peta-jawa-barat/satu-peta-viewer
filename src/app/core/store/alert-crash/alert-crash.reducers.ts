import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { AlertData } from '@models';
import { fromAlertCrashActions } from '@store/alert-crash/alert-crash.actions';

export const ENTITY_FEATURE_KEY = 'alertCrash';

export interface State extends EntityState<AlertData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<AlertData> = createEntityAdapter<AlertData>({
  selectId: (item) => item.id,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromAlertCrashActions.loadAlertCrash, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromAlertCrashActions.loadAlertCrashSuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromAlertCrashActions.loadAlertCrashFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromAlertCrashActions.clearAlertCrash, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function AlertCrashReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
