import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromAlertCrashActions } from '@store/alert-crash/alert-crash.actions';
import { AlertService } from '@services';

@Injectable()
export class AlertCrashEffects {
  // Get One
  loadAlertCrash$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAlertCrashActions.loadAlertCrash),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromAlertCrashActions.loadAlertCrashSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromAlertCrashActions.loadAlertCrashFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: AlertService) {}
}
