import { createAction, props } from '@ngrx/store';
import { AlertData } from '@models';

export enum AlertCrashActionTypes {
  LoadAlertCrash = '[ALERTCRASH] Load AlertCrash',
  LoadAlertCrashSuccess = '[ALERTCRASH] Load AlertCrash Success',
  LoadAlertCrashFailure = '[ALERTCRASH] Load AlertCrash Failure',
  ClearAlertCrash = '[ALERTCRASH] Clear AlertCrash',
}

export const loadAlertCrash = createAction(AlertCrashActionTypes.LoadAlertCrash, props<{ params: string }>());

export const loadAlertCrashSuccess = createAction(
  AlertCrashActionTypes.LoadAlertCrashSuccess,
  props<{
    data: AlertData[];
  }>()
);

export const loadAlertCrashFailure = createAction(
  AlertCrashActionTypes.LoadAlertCrashFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearAlertCrash = createAction(AlertCrashActionTypes.ClearAlertCrash);

export const fromAlertCrashActions = {
  loadAlertCrash,
  loadAlertCrashSuccess,
  loadAlertCrashFailure,
  clearAlertCrash,
};
