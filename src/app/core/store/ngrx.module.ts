import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';

import { AggregateJamEffects } from './aggregate-jam/aggregate-jam.effects';
import { AggregateJamReducer } from './aggregate-jam/aggregate-jam.reducers';
import { AlertCrashEffects } from './alert-crash/alert-crash.effects';
import { AlertCrashReducer } from './alert-crash/alert-crash.reducers';
import { AlertFloodEffects } from './alert-flood/alert-flood.effects';
import { AlertFloodReducer } from './alert-flood/alert-flood.reducers';
import { AlertPotholeEffects } from './alert-pothole/alert-pothole.effects';
import { AlertPotholeReducer } from './alert-pothole/alert-pothole.reducers';
import { IpEffects } from './ip/ip.effects';
import { IpReducer } from './ip/ip.reducers';
import { IrregularityEffects } from './irregularity/irregularity.effects';
import { IrregularityReducer } from './irregularity/irregularity.reducers';
import { JamEffects } from './jam/jam.effects';
import { JamReducer } from './jam/jam.reducers';
import { MapsetEffects } from './mapset/mapset.effects';
import { MapsetReducer } from './mapset/mapset.reducers';
import { MapsetStatisticEffects } from './mapset-statistic/mapset-statistic.effects';
import { MapsetStatisticReducer } from './mapset-statistic/mapset-statistic.reducers';
import { MapsetLawEffects } from './mapset-law/mapset-law.effects';
import { MapsetLawReducer } from './mapset-law/mapset-law.reducers';
import { MapsetStoryEffects } from './mapset-story/mapset-story.effects';
import { MapsetStoryReducer } from './mapset-story/mapset-story.reducers';
import { MapsetAwardEffects } from './mapset-award/mapset-award.effects';
import { MapsetAwardReducer } from './mapset-award/mapset-award.reducers';
import { WebgisEffects } from './webgis/webgis.effects';
import { WebgisReducer } from './webgis/webgis.reducers';
import { SkpdEffects } from './skpd/skpd.effects';
import { SkpdReducer } from './skpd/skpd.reducers';
import { SektoralEffects } from './sektoral/sektoral.effects';
import { SektoralReducer } from './sektoral/sektoral.reducers';
import { MapsetThematicEffects } from './mapset-thematic/mapset-thematic.effects';
import { MapsetThematicReducer } from './mapset-thematic/mapset-thematic.reducers';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({
      aggregateJam: AggregateJamReducer,
      alertCrash: AlertCrashReducer,
      alertFlood: AlertFloodReducer,
      alertPothole: AlertPotholeReducer,
      ip: IpReducer,
      irregularity: IrregularityReducer,
      jam: JamReducer,
      mapset: MapsetReducer,
      mapsetStatistic: MapsetStatisticReducer,
      mapsetLaw: MapsetLawReducer,
      mapsetStory: MapsetStoryReducer,
      mapsetAward: MapsetAwardReducer,
      webgis: WebgisReducer,
      skpd: SkpdReducer,
      sektoral: SektoralReducer,
      mapsetThematic: MapsetThematicReducer,
    }),
    EffectsModule.forRoot([
      AggregateJamEffects,
      AlertCrashEffects,
      AlertFloodEffects,
      AlertPotholeEffects,
      IpEffects,
      IrregularityEffects,
      JamEffects,
      MapsetEffects,
      MapsetStatisticEffects,
      MapsetLawEffects,
      MapsetStoryEffects,
      MapsetAwardEffects,
      WebgisEffects,
      SkpdEffects,
      SektoralEffects,
      MapsetThematicEffects,
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  exports: [StoreModule, EffectsModule, StoreDevtoolsModule],
})
export class NgrxModule {}
