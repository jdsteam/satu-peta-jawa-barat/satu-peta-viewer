import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MapsetLawData } from '@models';
import { fromMapsetLawActions } from '@store/mapset-law/mapset-law.actions';

export const ENTITY_FEATURE_KEY = 'mapsetLaw';

export interface State extends EntityState<MapsetLawData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<MapsetLawData> = createEntityAdapter<MapsetLawData>({
  selectId: (item) => item.id || item.kode_skpd,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromMapsetLawActions.loadMapsetLaw, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromMapsetLawActions.loadMapsetLawSuccess, (state, { data, metadata }) => {
    return adapter.setAll(data, {
      ...state,
      metadata,
      isLoading: false,
      error: null,
    });
  }),
  on(fromMapsetLawActions.loadMapsetLawFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromMapsetLawActions.clearMapsetLaw, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function MapsetLawReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
