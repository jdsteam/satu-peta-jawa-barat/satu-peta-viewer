import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromMapsetLawActions } from '@store/mapset-law/mapset-law.actions';
import { MapsetLawService } from '@services';

@Injectable()
export class MapsetLawEffects {
  // Get One
  loadMapsetLaw$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMapsetLawActions.loadMapsetLaw),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromMapsetLawActions.loadMapsetLawSuccess({
                  metadata: result.metadata,
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromMapsetLawActions.loadMapsetLawFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: MapsetLawService) {}
}
