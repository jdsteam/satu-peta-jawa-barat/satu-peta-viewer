import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State, adapter, ENTITY_FEATURE_KEY } from '@store/mapset-law/mapset-law.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll } = adapter.getSelectors();

// select the array of ids
export const selectMapsetLawIds = createSelector(getState, selectIds);

// select the array
export const selectAllMapsetLaw = createSelector(getState, selectAll);

// select list single
export const selectMapsetLaw = createSelector(getState, (state: State) => state.entities[0]);

// select loaded flag
export const selectIsLoading = createSelector(getState, (state) => state.isLoading);

// select metadata
export const selectMetadata = createSelector(getState, (state) => state.metadata);

// select error
export const selectError = createSelector(getState, (state) => state.error);
