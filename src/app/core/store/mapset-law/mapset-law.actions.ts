import { createAction, props } from '@ngrx/store';
import { MapsetLawData, MapsetLawMetadata } from '@models';

export enum MapsetLawActionTypes {
  LoadMapsetLaw = '[MAPSET LAW] Load Mapset Law',
  LoadMapsetLawSuccess = '[MAPSET LAW] Load Mapset Law Success',
  LoadMapsetLawFailure = '[MAPSET LAW] Load Mapset Law Failure',
  ClearMapsetLaw = '[MAPSET LAW] Clear Mapset Law',
}

export const loadMapsetLaw = createAction(MapsetLawActionTypes.LoadMapsetLaw, props<{ params: string }>());

export const loadMapsetLawSuccess = createAction(
  MapsetLawActionTypes.LoadMapsetLawSuccess,
  props<{
    metadata: MapsetLawMetadata;
    data: MapsetLawData[];
  }>()
);

export const loadMapsetLawFailure = createAction(
  MapsetLawActionTypes.LoadMapsetLawFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearMapsetLaw = createAction(MapsetLawActionTypes.ClearMapsetLaw);

export const fromMapsetLawActions = {
  loadMapsetLaw,
  loadMapsetLawSuccess,
  loadMapsetLawFailure,
  clearMapsetLaw,
};
