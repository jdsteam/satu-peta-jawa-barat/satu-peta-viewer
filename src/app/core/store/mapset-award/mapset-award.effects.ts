import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromMapsetAwardActions } from '@store/mapset-award/mapset-award.actions';
import { MapsetAwardService } from '@services';

@Injectable()
export class MapsetAwardEffects {
  // Get One
  loadMapsetAward$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMapsetAwardActions.loadMapsetAward),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromMapsetAwardActions.loadMapsetAwardSuccess({
                  metadata: result.metadata,
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromMapsetAwardActions.loadMapsetAwardFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: MapsetAwardService) {}
}
