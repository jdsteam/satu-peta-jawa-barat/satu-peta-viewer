import { createAction, props } from '@ngrx/store';
import { MapsetAwardData, MapsetAwardMetadata } from '@models';

export enum MapsetAwardActionTypes {
  LoadMapsetAward = '[MAPSET AWARD] Load Mapset Award',
  LoadMapsetAwardSuccess = '[MAPSET AWARD] Load Mapset Award Success',
  LoadMapsetAwardFailure = '[MAPSET AWARD] Load Mapset Award Failure',
  ClearMapsetAward = '[MAPSET AWARD] Clear Mapset Award',
}

export const loadMapsetAward = createAction(
  MapsetAwardActionTypes.LoadMapsetAward,
  props<{ params: string }>()
);

export const loadMapsetAwardSuccess = createAction(
  MapsetAwardActionTypes.LoadMapsetAwardSuccess,
  props<{
    metadata: MapsetAwardMetadata;
    data: MapsetAwardData[];
  }>()
);

export const loadMapsetAwardFailure = createAction(
  MapsetAwardActionTypes.LoadMapsetAwardFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearMapsetAward = createAction(MapsetAwardActionTypes.ClearMapsetAward);

export const fromMapsetAwardActions = {
  loadMapsetAward,
  loadMapsetAwardSuccess,
  loadMapsetAwardFailure,
  clearMapsetAward,
};
