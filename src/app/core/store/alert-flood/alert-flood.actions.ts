import { createAction, props } from '@ngrx/store';
import { AlertData } from '@models';

export enum AlertFloodActionTypes {
  LoadAlertFlood = '[ALERTFLOOD] Load AlertFlood',
  LoadAlertFloodSuccess = '[ALERTFLOOD] Load AlertFlood Success',
  LoadAlertFloodFailure = '[ALERTFLOOD] Load AlertFlood Failure',
  ClearAlertFlood = '[ALERTFLOOD] Clear AlertFlood',
}

export const loadAlertFlood = createAction(AlertFloodActionTypes.LoadAlertFlood, props<{ params: string }>());

export const loadAlertFloodSuccess = createAction(
  AlertFloodActionTypes.LoadAlertFloodSuccess,
  props<{
    data: AlertData[];
  }>()
);

export const loadAlertFloodFailure = createAction(
  AlertFloodActionTypes.LoadAlertFloodFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearAlertFlood = createAction(AlertFloodActionTypes.ClearAlertFlood);

export const fromAlertFloodActions = {
  loadAlertFlood,
  loadAlertFloodSuccess,
  loadAlertFloodFailure,
  clearAlertFlood,
};
