import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { AlertData } from '@models';
import { fromAlertFloodActions } from '@store/alert-flood/alert-flood.actions';

export const ENTITY_FEATURE_KEY = 'alertFlood';

export interface State extends EntityState<AlertData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<AlertData> = createEntityAdapter<AlertData>({
  selectId: (item) => item.id,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromAlertFloodActions.loadAlertFlood, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromAlertFloodActions.loadAlertFloodSuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromAlertFloodActions.loadAlertFloodFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromAlertFloodActions.clearAlertFlood, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function AlertFloodReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
