import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromAlertFloodActions } from '@store/alert-flood/alert-flood.actions';
import { AlertService } from '@services';

@Injectable()
export class AlertFloodEffects {
  // Get One
  loadAlertFlood$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAlertFloodActions.loadAlertFlood),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromAlertFloodActions.loadAlertFloodSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromAlertFloodActions.loadAlertFloodFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: AlertService) {}
}
