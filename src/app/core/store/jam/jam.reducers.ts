import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { AlertData } from '@models';
import { fromJamActions } from '@store/jam/jam.actions';

export const ENTITY_FEATURE_KEY = 'jam';

export interface State extends EntityState<AlertData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<AlertData> = createEntityAdapter<AlertData>({
  selectId: (item) => item.id,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromJamActions.loadJam, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromJamActions.loadJamSuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromJamActions.loadJamFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromJamActions.clearJam, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function JamReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
