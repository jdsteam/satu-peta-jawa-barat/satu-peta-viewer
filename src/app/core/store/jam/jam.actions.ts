import { createAction, props } from '@ngrx/store';
import { AlertData } from '@models';

export enum JamActionTypes {
  LoadJam = '[JAM] Load Jam',
  LoadJamSuccess = '[JAM] Load Jam Success',
  LoadJamFailure = '[JAM] Load Jam Failure',
  ClearJam = '[JAM] Clear Jam',
}

export const loadJam = createAction(JamActionTypes.LoadJam, props<{ params: string }>());

export const loadJamSuccess = createAction(
  JamActionTypes.LoadJamSuccess,
  props<{
    data: AlertData[];
  }>()
);

export const loadJamFailure = createAction(JamActionTypes.LoadJamFailure, props<{ error: Error | any }>());

// Clear
export const clearJam = createAction(JamActionTypes.ClearJam);

export const fromJamActions = {
  loadJam,
  loadJamSuccess,
  loadJamFailure,
  clearJam,
};
