import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromJamActions } from '@store/jam/jam.actions';
import { JamService } from '@services';

@Injectable()
export class JamEffects {
  // Get One
  loadJam$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromJamActions.loadJam),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromJamActions.loadJamSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromJamActions.loadJamFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: JamService) {}
}
