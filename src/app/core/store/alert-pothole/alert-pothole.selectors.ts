import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State, adapter, ENTITY_FEATURE_KEY } from '@store/alert-pothole/alert-pothole.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll } = adapter.getSelectors();

// select the array of ids
export const selectAlertPotholeIds = createSelector(getState, selectIds);

// select the array
export const selectAllAlertPothole = createSelector(getState, selectAll);

// select list single
export const selectAlertPothole = createSelector(getState, (state: State) => state.entities[0]);

// select loaded flag
export const selectIsLoading = createSelector(getState, (state) => state.isLoading);

// select error
export const selectError = createSelector(getState, (state) => state.error);
