import { createAction, props } from '@ngrx/store';
import { AlertData } from '@models';

export enum AlertPotholeActionTypes {
  LoadAlertPothole = '[ALERTPOTHOLE] Load AlertPothole',
  LoadAlertPotholeSuccess = '[ALERTPOTHOLE] Load AlertPothole Success',
  LoadAlertPotholeFailure = '[ALERTPOTHOLE] Load AlertPothole Failure',
  ClearAlertPothole = '[ALERTPOTHOLE] Clear AlertPothole',
}

export const loadAlertPothole = createAction(
  AlertPotholeActionTypes.LoadAlertPothole,
  props<{ params: string }>()
);

export const loadAlertPotholeSuccess = createAction(
  AlertPotholeActionTypes.LoadAlertPotholeSuccess,
  props<{
    data: AlertData[];
  }>()
);

export const loadAlertPotholeFailure = createAction(
  AlertPotholeActionTypes.LoadAlertPotholeFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearAlertPothole = createAction(AlertPotholeActionTypes.ClearAlertPothole);

export const fromAlertPotholeActions = {
  loadAlertPothole,
  loadAlertPotholeSuccess,
  loadAlertPotholeFailure,
  clearAlertPothole,
};
