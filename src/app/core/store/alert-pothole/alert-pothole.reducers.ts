import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { AlertData } from '@models';
import { fromAlertPotholeActions } from '@store/alert-pothole/alert-pothole.actions';

export const ENTITY_FEATURE_KEY = 'alertPothole';

export interface State extends EntityState<AlertData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<AlertData> = createEntityAdapter<AlertData>({
  selectId: (item) => item.id,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromAlertPotholeActions.loadAlertPothole, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromAlertPotholeActions.loadAlertPotholeSuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromAlertPotholeActions.loadAlertPotholeFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromAlertPotholeActions.clearAlertPothole, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function AlertPotholeReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
