import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromAlertPotholeActions } from '@store/alert-pothole/alert-pothole.actions';
import { AlertService } from '@services';

@Injectable()
export class AlertPotholeEffects {
  // Get One
  loadAlertPothole$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAlertPotholeActions.loadAlertPothole),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromAlertPotholeActions.loadAlertPotholeSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromAlertPotholeActions.loadAlertPotholeFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: AlertService) {}
}
