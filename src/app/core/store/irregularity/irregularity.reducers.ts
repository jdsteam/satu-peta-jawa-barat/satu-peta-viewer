import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { AlertData } from '@models';
import { fromIrregularityActions } from '@store/irregularity/irregularity.actions';

export const ENTITY_FEATURE_KEY = 'irregularity';

export interface State extends EntityState<AlertData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<AlertData> = createEntityAdapter<AlertData>({
  selectId: (item) => item.id,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromIrregularityActions.loadIrregularity, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromIrregularityActions.loadIrregularitySuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromIrregularityActions.loadIrregularityFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromIrregularityActions.clearIrregularity, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function IrregularityReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
