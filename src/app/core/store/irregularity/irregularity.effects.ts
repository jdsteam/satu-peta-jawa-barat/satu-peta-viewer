import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromIrregularityActions } from '@store/irregularity/irregularity.actions';
import { IrregularityService } from '@services';

@Injectable()
export class IrregularityEffects {
  // Get One
  loadIrregularity$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIrregularityActions.loadIrregularity),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromIrregularityActions.loadIrregularitySuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromIrregularityActions.loadIrregularityFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: IrregularityService) {}
}
