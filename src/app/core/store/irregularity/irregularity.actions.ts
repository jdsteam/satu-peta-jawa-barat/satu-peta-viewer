import { createAction, props } from '@ngrx/store';
import { AlertData } from '@models';

export enum IrregularityActionTypes {
  LoadIrregularity = '[IRREGULARITY] Load Irregularity',
  LoadIrregularitySuccess = '[IRREGULARITY] Load Irregularity Success',
  LoadIrregularityFailure = '[IRREGULARITY] Load Irregularity Failure',
  ClearIrregularity = '[IRREGULARITY] Clear Irregularity',
}

export const loadIrregularity = createAction(
  IrregularityActionTypes.LoadIrregularity,
  props<{ params: string }>()
);

export const loadIrregularitySuccess = createAction(
  IrregularityActionTypes.LoadIrregularitySuccess,
  props<{
    data: AlertData[];
  }>()
);

export const loadIrregularityFailure = createAction(
  IrregularityActionTypes.LoadIrregularityFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearIrregularity = createAction(IrregularityActionTypes.ClearIrregularity);

export const fromIrregularityActions = {
  loadIrregularity,
  loadIrregularitySuccess,
  loadIrregularityFailure,
  clearIrregularity,
};
