import { createAction, props } from '@ngrx/store';
import { MapsetThematicData } from '@models';

export enum MapsetThematicActionTypes {
  LoadMapsetThematic = '[MAPSET THEMATIC] Load MapsetThematic',
  LoadMapsetThematicSuccess = '[MAPSET THEMATIC] Load MapsetThematic Success',
  LoadMapsetThematicFailure = '[MAPSET THEMATIC] Load MapsetThematic Failure',
  ClearMapsetThematic = '[MAPSET THEMATIC] Clear MapsetThematic',
}

export const loadMapsetThematic = createAction(
  MapsetThematicActionTypes.LoadMapsetThematic,
  props<{ params: string }>()
);

export const loadMapsetThematicSuccess = createAction(
  MapsetThematicActionTypes.LoadMapsetThematicSuccess,
  props<{
    data: MapsetThematicData[];
  }>()
);

export const loadMapsetThematicFailure = createAction(
  MapsetThematicActionTypes.LoadMapsetThematicFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearMapsetThematic = createAction(MapsetThematicActionTypes.ClearMapsetThematic);

export const fromMapsetThematicActions = {
  loadMapsetThematic,
  loadMapsetThematicSuccess,
  loadMapsetThematicFailure,
  clearMapsetThematic,
};
