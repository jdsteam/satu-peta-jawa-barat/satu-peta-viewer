import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MapsetThematicData } from '@models';
import { fromMapsetThematicActions } from '@store/mapset-thematic/mapset-thematic.actions';

export const ENTITY_FEATURE_KEY = 'mapsetThematic';

export interface State extends EntityState<MapsetThematicData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<MapsetThematicData> = createEntityAdapter<MapsetThematicData>({
  selectId: (item) => item.id,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromMapsetThematicActions.loadMapsetThematic, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromMapsetThematicActions.loadMapsetThematicSuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromMapsetThematicActions.loadMapsetThematicFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromMapsetThematicActions.clearMapsetThematic, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function MapsetThematicReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
