import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromMapsetThematicActions } from '@store/mapset-thematic/mapset-thematic.actions';
import { MapsetThematicService } from '@services';

@Injectable()
export class MapsetThematicEffects {
  // Get One
  loadMapsetThematic$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMapsetThematicActions.loadMapsetThematic),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromMapsetThematicActions.loadMapsetThematicSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromMapsetThematicActions.loadMapsetThematicFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: MapsetThematicService) {}
}
