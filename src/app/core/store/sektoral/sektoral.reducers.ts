import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { SektoralData } from '@models';
import { fromSektoralActions } from '@store/sektoral/sektoral.actions';

export const ENTITY_FEATURE_KEY = 'sektoral';

export interface State extends EntityState<SektoralData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<SektoralData> = createEntityAdapter<SektoralData>({
  selectId: (item) => item.id || item.kode_sektoral,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromSektoralActions.loadSektoral, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromSektoralActions.loadSektoralSuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromSektoralActions.loadSektoralFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromSektoralActions.clearSektoral, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function SektoralReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
