import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromSektoralActions } from '@store/sektoral/sektoral.actions';
import { SektoralService } from '@services';

@Injectable()
export class SektoralEffects {
  // Get One
  loadSektoral$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromSektoralActions.loadSektoral),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromSektoralActions.loadSektoralSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromSektoralActions.loadSektoralFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: SektoralService) {}
}
