import { createAction, props } from '@ngrx/store';
import { SektoralData } from '@models';

export enum SektoralActionTypes {
  LoadSektoral = '[SEKTORAL] Load Sektoral',
  LoadSektoralSuccess = '[SEKTORAL] Load Sektoral Success',
  LoadSektoralFailure = '[SEKTORAL] Load Sektoral Failure',
  ClearSektoral = '[SEKTORAL] Clear Sektoral',
}

export const loadSektoral = createAction(SektoralActionTypes.LoadSektoral, props<{ params: string }>());

export const loadSektoralSuccess = createAction(
  SektoralActionTypes.LoadSektoralSuccess,
  props<{
    data: SektoralData[];
  }>()
);

export const loadSektoralFailure = createAction(
  SektoralActionTypes.LoadSektoralFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearSektoral = createAction(SektoralActionTypes.ClearSektoral);

export const fromSektoralActions = {
  loadSektoral,
  loadSektoralSuccess,
  loadSektoralFailure,
  clearSektoral,
};
