import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromWebgisActions } from '@store/webgis/webgis.actions';
import { WebgisService } from '@services';

@Injectable()
export class WebgisEffects {
  // Get One
  loadWebgis$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromWebgisActions.loadWebgis),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromWebgisActions.loadWebgisSuccess({
                  metadata: result.metadata,
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromWebgisActions.loadWebgisFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: WebgisService) {}
}
