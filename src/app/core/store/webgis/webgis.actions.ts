import { createAction, props } from '@ngrx/store';
import { WebgisData, WebgisMetadata } from '@models';

export enum WebgisActionTypes {
  LoadWebgis = '[WEBGIS] Load Webgis',
  LoadWebgisSuccess = '[WEBGIS] Load Webgis Success',
  LoadWebgisFailure = '[WEBGIS] Load Webgis Failure',
  ClearWebgis = '[WEBGIS] Clear Webgis',
}

export const loadWebgis = createAction(WebgisActionTypes.LoadWebgis, props<{ params: string }>());

export const loadWebgisSuccess = createAction(
  WebgisActionTypes.LoadWebgisSuccess,
  props<{
    metadata: WebgisMetadata;
    data: WebgisData[];
  }>()
);

export const loadWebgisFailure = createAction(
  WebgisActionTypes.LoadWebgisFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearWebgis = createAction(WebgisActionTypes.ClearWebgis);

export const fromWebgisActions = {
  loadWebgis,
  loadWebgisSuccess,
  loadWebgisFailure,
  clearWebgis,
};
