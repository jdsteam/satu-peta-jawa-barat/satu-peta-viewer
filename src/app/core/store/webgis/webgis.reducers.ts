import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { WebgisData } from '@models';
import { fromWebgisActions } from '@store/webgis/webgis.actions';

export const ENTITY_FEATURE_KEY = 'webgis';

export interface State extends EntityState<WebgisData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<WebgisData> = createEntityAdapter<WebgisData>({
  selectId: (item) => item.id || item.kode_skpd,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromWebgisActions.loadWebgis, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromWebgisActions.loadWebgisSuccess, (state, { data, metadata }) => {
    return adapter.setAll(data, {
      ...state,
      metadata,
      isLoading: false,
      error: null,
    });
  }),
  on(fromWebgisActions.loadWebgisFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromWebgisActions.clearWebgis, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function WebgisReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
