import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromMapsetActions } from '@store/mapset/mapset.actions';
import { MapsetService } from '@services';

@Injectable()
export class MapsetEffects {
  // Get One
  loadMapset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMapsetActions.loadMapset),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromMapsetActions.loadMapsetSuccess({
                  metadata: result.metadata,
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromMapsetActions.loadMapsetFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: MapsetService) {}
}
