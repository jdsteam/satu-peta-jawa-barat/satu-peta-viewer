import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MapsetData } from '@models';
import { fromMapsetActions } from '@store/mapset/mapset.actions';

export const ENTITY_FEATURE_KEY = 'mapset';

export interface State extends EntityState<MapsetData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<MapsetData> = createEntityAdapter<MapsetData>({
  selectId: (item) => item.id || item.kode_skpd,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromMapsetActions.loadMapset, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromMapsetActions.loadMapsetSuccess, (state, { data, metadata }) => {
    return adapter.setAll(data, {
      ...state,
      metadata,
      isLoading: false,
      error: null,
    });
  }),
  on(fromMapsetActions.loadMapsetFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromMapsetActions.clearMapset, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function MapsetReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
