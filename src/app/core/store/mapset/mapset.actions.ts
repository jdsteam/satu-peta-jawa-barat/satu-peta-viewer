import { createAction, props } from '@ngrx/store';
import { MapsetData, MapsetMetadata } from '@models';

export enum MapsetActionTypes {
  LoadMapset = '[MAPSET] Load Mapset',
  LoadMapsetSuccess = '[MAPSET] Load Mapset Success',
  LoadMapsetFailure = '[MAPSET] Load Mapset Failure',
  ClearMapset = '[MAPSET] Clear Mapset',
}

export const loadMapset = createAction(MapsetActionTypes.LoadMapset, props<{ params: string }>());

export const loadMapsetSuccess = createAction(
  MapsetActionTypes.LoadMapsetSuccess,
  props<{
    metadata: MapsetMetadata;
    data: MapsetData[];
  }>()
);

export const loadMapsetFailure = createAction(
  MapsetActionTypes.LoadMapsetFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearMapset = createAction(MapsetActionTypes.ClearMapset);

export const fromMapsetActions = {
  loadMapset,
  loadMapsetSuccess,
  loadMapsetFailure,
  clearMapset,
};
