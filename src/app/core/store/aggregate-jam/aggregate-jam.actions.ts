import { createAction, props } from '@ngrx/store';
import { AlertData } from '@models';

export enum AggregateJamActionTypes {
  LoadAggregateJam = '[JAM] Load Aggregate Jam',
  LoadAggregateJamSuccess = '[JAM] Load Aggregate Jam Success',
  LoadAggregateJamFailure = '[JAM] Load Aggregate Jam Failure',
  ClearAggregateJam = '[JAM] Clear AggregateJam',
}

export const loadAggregateJam = createAction(
  AggregateJamActionTypes.LoadAggregateJam,
  props<{ params: string }>()
);

export const loadAggregateJamSuccess = createAction(
  AggregateJamActionTypes.LoadAggregateJamSuccess,
  props<{
    data: AlertData[];
  }>()
);

export const loadAggregateJamFailure = createAction(
  AggregateJamActionTypes.LoadAggregateJamFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearAggregateJam = createAction(AggregateJamActionTypes.ClearAggregateJam);

export const fromAggregateJamActions = {
  loadAggregateJam,
  loadAggregateJamSuccess,
  loadAggregateJamFailure,
  clearAggregateJam,
};
