import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromAggregateJamActions } from '@store/aggregate-jam/aggregate-jam.actions';
import { AggregateJamService } from '@services';

@Injectable()
export class AggregateJamEffects {
  // Get One
  loadAggregateJam$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAggregateJamActions.loadAggregateJam),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromAggregateJamActions.loadAggregateJamSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromAggregateJamActions.loadAggregateJamFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: AggregateJamService) {}
}
