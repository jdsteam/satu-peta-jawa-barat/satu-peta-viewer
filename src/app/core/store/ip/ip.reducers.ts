import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { IpData } from '@models';
import { fromIpActions } from '@store/ip/ip.actions';

export const ENTITY_FEATURE_KEY = 'ip';

export interface State extends EntityState<IpData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<IpData> = createEntityAdapter<IpData>({
  selectId: (item) => String(item.ip),
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load Single
  on(fromIpActions.loadIp, (state) => {
    return {
      ...state,
      isLoadingRead: true,
    };
  }),
  on(fromIpActions.loadIpSuccess, (state, { data }) => {
    return adapter.setOne(data, {
      ...state,
      isLoadingRead: false,
      error: { error: false },
    });
  }),
  on(fromIpActions.loadIpFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingRead: false,
      error,
    };
  }),

  // Clear
  on(fromIpActions.clearIp, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  })
);

export function IpReducer(state: State | undefined, action: Action): any {
  return reducer(state, action);
}
