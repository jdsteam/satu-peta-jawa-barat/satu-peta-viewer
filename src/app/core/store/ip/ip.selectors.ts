import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State, adapter, ENTITY_FEATURE_KEY } from '@store/ip/ip.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll } = adapter.getSelectors();

// select the array of ids
export const selectIpIds = createSelector(getState, selectIds);

// select the array
export const selectAllIp = createSelector(getState, selectAll);

// select the by id
export const selectIp = createSelector(
  getState,
  (state: State, prop: { id: number }) => state.entities[prop.id]
);

// select loaded flag
export const selectIsLoadingList = createSelector(getState, (state) => state.isLoadingList);
export const selectIsLoadingCreate = createSelector(getState, (state) => state.isLoadingCreate);
export const selectIsLoadingRead = createSelector(getState, (state) => state.isLoadingRead);
export const selectIsLoadingUpdate = createSelector(getState, (state) => state.isLoadingUpdate);
export const selectIsLoadingDelete = createSelector(getState, (state) => state.isLoadingDelete);
export const selectIsLoadingCounter = createSelector(getState, (state) => state.isLoadingCounter);

// select metadata
export const selectMetadata = createSelector(getState, (state) => state.metadata);

// select pagination
export const selectPagination = createSelector(getState, (state) => state.pagination);

// select error
export const selectError = createSelector(getState, (state) => state.error);
