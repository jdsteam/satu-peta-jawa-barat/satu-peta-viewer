import { createAction, props } from '@ngrx/store';
import { IpData } from '@models';

export enum IpActionTypes {
  LoadIp = '[IP] Load Ip',
  LoadIpSuccess = '[IP] Load Ip Success',
  LoadIpFailure = '[IP] Load Ip Failure',
  ClearIp = '[IP] Clear Ip',
}

// Load Single
export const loadIp = createAction(
  IpActionTypes.LoadIp
);

export const loadIpSuccess = createAction(
  IpActionTypes.LoadIpSuccess,
  props<{ data: IpData }>()
);

export const loadIpFailure = createAction(
  IpActionTypes.LoadIpFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearIp = createAction(IpActionTypes.ClearIp);

export const fromIpActions = {
  loadIp,
  loadIpSuccess,
  loadIpFailure,
  clearIp,
};
