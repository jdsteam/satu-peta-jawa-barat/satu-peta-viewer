import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromIpActions } from '@store/ip/ip.actions';
import { LogService } from '@services';

@Injectable()
export class IpEffects {
  name = 'IP';

  loadIp$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIpActions.loadIp),
      switchMap((action) => {
        return this.logService.getIP().pipe(
          map((result: any) =>
            fromIpActions.loadIpSuccess({
              data: result,
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromIpActions.loadIpFailure({
                error: {
                  error,
                },
              })
            )
          )
        );
      })
    )
  );

  constructor(private actions$: Actions, private logService: LogService) {}
}
