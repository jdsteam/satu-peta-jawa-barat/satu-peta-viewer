import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromMapsetStoryActions } from '@store/mapset-story/mapset-story.actions';
import { MapsetStoryService } from '@services';

@Injectable()
export class MapsetStoryEffects {
  // Get One
  loadMapsetStory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMapsetStoryActions.loadMapsetStory),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromMapsetStoryActions.loadMapsetStorySuccess({
                  metadata: result.metadata,
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromMapsetStoryActions.loadMapsetStoryFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: MapsetStoryService) {}
}
