import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MapsetStoryData } from '@models';
import { fromMapsetStoryActions } from '@store/mapset-story/mapset-story.actions';

export const ENTITY_FEATURE_KEY = 'mapsetStory';

export interface State extends EntityState<MapsetStoryData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<MapsetStoryData> = createEntityAdapter<MapsetStoryData>({
  selectId: (item) => item.id || item.kode_skpd,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromMapsetStoryActions.loadMapsetStory, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromMapsetStoryActions.loadMapsetStorySuccess, (state, { data, metadata }) => {
    return adapter.setAll(data, {
      ...state,
      metadata,
      isLoading: false,
      error: null,
    });
  }),
  on(fromMapsetStoryActions.loadMapsetStoryFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromMapsetStoryActions.clearMapsetStory, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function MapsetStoryReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
