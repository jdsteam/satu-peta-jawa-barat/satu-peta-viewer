import { createAction, props } from '@ngrx/store';
import { MapsetStoryData, MapsetStoryMetadata } from '@models';

export enum MapsetStoryActionTypes {
  LoadMapsetStory = '[MAPSET STORY] Load Mapset Story',
  LoadMapsetStorySuccess = '[MAPSET STORY] Load Mapset Story Success',
  LoadMapsetStoryFailure = '[MAPSET STORY] Load Mapset Story Failure',
  ClearMapsetStory = '[MAPSET STORY] Clear Mapset Story',
}

export const loadMapsetStory = createAction(
  MapsetStoryActionTypes.LoadMapsetStory,
  props<{ params: string }>()
);

export const loadMapsetStorySuccess = createAction(
  MapsetStoryActionTypes.LoadMapsetStorySuccess,
  props<{
    metadata: MapsetStoryMetadata;
    data: MapsetStoryData[];
  }>()
);

export const loadMapsetStoryFailure = createAction(
  MapsetStoryActionTypes.LoadMapsetStoryFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearMapsetStory = createAction(MapsetStoryActionTypes.ClearMapsetStory);

export const fromMapsetStoryActions = {
  loadMapsetStory,
  loadMapsetStorySuccess,
  loadMapsetStoryFailure,
  clearMapsetStory,
};
