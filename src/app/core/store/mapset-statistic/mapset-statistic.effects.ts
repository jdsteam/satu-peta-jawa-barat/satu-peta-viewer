import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromMapsetStatisticActions } from '@store/mapset-statistic/mapset-statistic.actions';
import { MapsetStatisticService } from '@services';

@Injectable()
export class MapsetStatisticEffects {
  // Get One
  loadMapsetStatistic$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMapsetStatisticActions.loadMapsetStatistic),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromMapsetStatisticActions.loadMapsetStatisticSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromMapsetStatisticActions.loadMapsetStatisticFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: MapsetStatisticService) {}
}
