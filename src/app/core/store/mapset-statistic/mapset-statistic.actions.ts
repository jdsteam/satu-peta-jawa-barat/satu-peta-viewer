import { createAction, props } from '@ngrx/store';
import { MapsetStatisticData } from '@models';

export enum MapsetStatisticActionTypes {
  LoadMapsetStatistic = '[MAPSETSTATISTIC] Load Mapset Statistic',
  LoadMapsetStatisticSuccess = '[MAPSETSTATISTIC] Load Mapset Statistic Success',
  LoadMapsetStatisticFailure = '[MAPSETSTATISTIC] Load Mapset Statistic Failure',
  ClearMapsetStatistic = '[MAPSETSTATISTIC] Clear Mapset Statistic',
}

export const loadMapsetStatistic = createAction(
  MapsetStatisticActionTypes.LoadMapsetStatistic,
  props<{ params: string }>()
);

export const loadMapsetStatisticSuccess = createAction(
  MapsetStatisticActionTypes.LoadMapsetStatisticSuccess,
  props<{
    data: MapsetStatisticData;
  }>()
);

export const loadMapsetStatisticFailure = createAction(
  MapsetStatisticActionTypes.LoadMapsetStatisticFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearMapsetStatistic = createAction(MapsetStatisticActionTypes.ClearMapsetStatistic);

export const fromMapsetStatisticActions = {
  loadMapsetStatistic,
  loadMapsetStatisticSuccess,
  loadMapsetStatisticFailure,
  clearMapsetStatistic,
};
