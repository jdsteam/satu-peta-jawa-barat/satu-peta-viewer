import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MapsetStatisticData } from '@models';
import { fromMapsetStatisticActions } from '@store/mapset-statistic/mapset-statistic.actions';

export const ENTITY_FEATURE_KEY = 'mapsetStatistic';

export interface State extends EntityState<MapsetStatisticData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<MapsetStatisticData> = createEntityAdapter<MapsetStatisticData>({
  selectId: (item) => 0,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromMapsetStatisticActions.loadMapsetStatistic, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromMapsetStatisticActions.loadMapsetStatisticSuccess, (state, { data }) => {
    return adapter.setOne(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromMapsetStatisticActions.loadMapsetStatisticFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromMapsetStatisticActions.clearMapsetStatistic, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function MapsetStatisticReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
