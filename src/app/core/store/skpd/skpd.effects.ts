import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { fromSkpdActions } from '@store/skpd/skpd.actions';
import { SkpdService } from '@services';

@Injectable()
export class SkpdEffects {
  // Get One
  loadSkpd$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromSkpdActions.loadSkpd),
      switchMap((action) =>
        timer(0, environment.refreshData).pipe(
          switchMap(() =>
            this.connService.getItem(action.params).pipe(
              map((result) =>
                fromSkpdActions.loadSkpdSuccess({
                  data: result.data,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromSkpdActions.loadSkpdFailure({
                    error: error.statusText,
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private connService: SkpdService) {}
}
