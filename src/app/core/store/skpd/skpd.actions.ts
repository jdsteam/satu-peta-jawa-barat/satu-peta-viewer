import { createAction, props } from '@ngrx/store';
import { SkpdData } from '@models';

export enum SkpdActionTypes {
  LoadSkpd = '[SKPD] Load Skpd',
  LoadSkpdSuccess = '[SKPD] Load Skpd Success',
  LoadSkpdFailure = '[SKPD] Load Skpd Failure',
  ClearSkpd = '[SKPD] Clear Skpd',
}

export const loadSkpd = createAction(SkpdActionTypes.LoadSkpd, props<{ params: string }>());

export const loadSkpdSuccess = createAction(
  SkpdActionTypes.LoadSkpdSuccess,
  props<{
    data: SkpdData[];
  }>()
);

export const loadSkpdFailure = createAction(SkpdActionTypes.LoadSkpdFailure, props<{ error: Error | any }>());

// Clear
export const clearSkpd = createAction(SkpdActionTypes.ClearSkpd);

export const fromSkpdActions = {
  loadSkpd,
  loadSkpdSuccess,
  loadSkpdFailure,
  clearSkpd,
};
