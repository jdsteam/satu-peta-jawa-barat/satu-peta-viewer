import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { SkpdData } from '@models';
import { fromSkpdActions } from '@store/skpd/skpd.actions';

export const ENTITY_FEATURE_KEY = 'skpd';

export interface State extends EntityState<SkpdData> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<SkpdData> = createEntityAdapter<SkpdData>({
  selectId: (item) => item.id || item.kode_skpd,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: true,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromSkpdActions.loadSkpd, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(fromSkpdActions.loadSkpdSuccess, (state, { data }) => {
    return adapter.setAll(data, {
      ...state,
      isLoading: false,
      error: null,
    });
  }),
  on(fromSkpdActions.loadSkpdFailure, (state, { error }) => {
    return {
      ...state,
      isLoading: false,
      error,
    };
  }),
  on(fromSkpdActions.clearSkpd, (state) => {
    return adapter.removeAll({ ...state });
  })
);

export function SkpdReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
