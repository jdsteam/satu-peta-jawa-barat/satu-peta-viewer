import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { concat, Observable } from 'rxjs';
import { first, map, skip, take, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { IpData } from '@models';

// PLUGIN
import { Store, select } from '@ngrx/store';

// STORE
import { selectAllIp } from '@store/ip/ip.selectors';
import { fromIpActions } from '@store/ip/ip.actions';

import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class IpGuard implements CanActivate {
  moment: any = moment;
  env = environment;

  constructor(private router: Router, private store: Store<any>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return concat(this.loadIfRequired(), this.hasIpInStore()).pipe(skip(1));
  }

  private get ipState$(): Observable<IpData[]> {
    return this.store.pipe(select(selectAllIp));
  }

  private loadIfRequired(): Observable<any> {
    return this.ipState$.pipe(
      map((state) => state.length !== 0),
      take(1),
      tap((load) => {
        if (!load) {
          this.store.dispatch(fromIpActions.loadIp());
        }
      })
    );
  }

  private hasIpInStore(): Observable<boolean> {
    return this.ipState$.pipe(
      first((state) => state.length !== 0),
      map((result) => {
        if (!!result) {
          return true;
        }

        this.router.navigate([]);
        return false;
      })
    );
  }
}
