import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root',
})
@NgModule({
  imports: [CommonModule],
})
export class AuthGuard implements CanActivate {
  private user: any;
  constructor(private authService: AuthenticationService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    let isLoggedIn = this.authService.isLoggedIn();

    if (isLoggedIn) {
      return isLoggedIn;
    } else {
      // Store the attempted URL for redirecting
      this.authService.redirectUrl = url;

      this.authService.logout();

      // Navigate to the login page with extras
      this.router.navigate(['/login']);

      return false;
    }
  }
}
