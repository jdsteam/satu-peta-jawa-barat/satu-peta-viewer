import { Deserializable, RegionalData } from '@models';

export class Skpd implements Deserializable {
  [x: string]: any;
  message: string;
  data: SkpdData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class SkpdData implements Deserializable {
  [x: string]: any;
  id: string;
  regional_id: number;
  regional: RegionalData;
  kode_skpd: string;
  nama_skpd: string;
  nama_skpd_alias: string;
  description: string;
  phone: string;
  notes: string;
  address: string;
  email: string;
  logo: string;
  media_website: string;
  media_twitter: string;
  media_youtube: string;
  media_facebook: string;
  media_instagram: string;
  count_dataset: number;
  count_infographic: number;
  count_visualization: number;
  count_dataset_public: number;
  count_infographic_public: number;
  count_visualization_public: number;
  datecount: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
