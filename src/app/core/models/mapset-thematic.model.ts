import { BidangUrusanData, Deserializable, MapsetThematicSourceData, SektoralData } from '@models';

export class MapsetThematic implements Deserializable {
  [x: string]: any;
  error: number;
  data: MapsetThematicData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetThematicData implements Deserializable {
  [x: string]: any;
  id: number;
  title: string; // tslint:disable-line: variable-name
  image: string; // tslint:disable-line: variable-name
  description: string; // tslint:disable-line: variable-name
  year: string; // tslint:disable-line: variable-name
  map_type: string; // tslint:disable-line: variable-name
  sektoral: SektoralData; // tslint:disable-line: variable-name
  bidang_urusan: BidangUrusanData[]; // tslint:disable-line: variable-name
  source: MapsetThematicSourceData[]; // tslint:disable-line: variable-name
  metadata: MapsetThematicMetadataData[]; // tslint:disable-line: variable-name
  cdate: string; // tslint:disable-line: variable-name
  mdate: string; // tslint:disable-line: variable-name
  is_active: boolean; // tslint:disable-line: variable-name
  is_deleted: boolean; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getMapType(val): any {
    switch (val) {
      case 'interactive':
        return 'Peta Interaktif';
      case 'layout':
        return 'Peta Layout';
      default:
        break;
    }
  }

  getDataType(datatype: string): string {
    switch (datatype) {
      case 'base':
        return 'Peta Dasar';
      case 'thematic':
        return 'Peta Tematik';
      default:
        return '-';
    }
  }
}

export class MapsetThematicMetadataData implements Deserializable {
  [x: string]: any;
  id: number;
  description: string; // tslint:disable-line: variable-name
  key: string; // tslint:disable-line: variable-name
  value: string; // tslint:disable-line: variable-name
  mapset_thematic_id: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetThematicMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
