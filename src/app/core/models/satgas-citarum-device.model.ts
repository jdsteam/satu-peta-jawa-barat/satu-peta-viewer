import { Deserializable } from '@models';

export class SatgasCitarumDevice implements Deserializable {
  [x: string]: any;
  result: any;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class SatgasCitarumDeviceData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_level_id: number; // tslint:disable-line: variable-name
  kode_bps: number; // tslint:disable-line: variable-name
  kode_kemendagri: number; // tslint:disable-line: variable-name
  nama_bps: string; // tslint:disable-line: variable-name
  nama_kemendagri: string; // tslint:disable-line: variable-name
  notes: null;
  mapset: [];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
