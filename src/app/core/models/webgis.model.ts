import { Deserializable } from '@models';

export class Webgis implements Deserializable {
  [x: string]: any;
  error: number;
  data: WebgisData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class WebgisData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string; // tslint:disable-line: variable-name
  skpd: string; // tslint:disable-line: variable-name
  link: string; // tslint:disable-line: variable-name
  notes: null;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class WebgisMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
