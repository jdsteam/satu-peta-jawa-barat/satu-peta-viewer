import { Deserializable } from '@models';

export class MapsetStory implements Deserializable {
  [x: string]: any;
  error: number;
  data: MapsetStoryData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetStoryData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string; // tslint:disable-line: variable-name
  url: string; // tslint:disable-line: variable-name
  description: string; // tslint:disable-line: variable-name
  image: string; // tslint:disable-line: variable-name
  count_view: number; // tslint:disable-line: variable-name
  is_active: boolean; // tslint:disable-line: variable-name
  is_deleted: boolean; // tslint:disable-line: variable-name
  cdate: string; // tslint:disable-line: variable-name
  mdate: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetStoryMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
