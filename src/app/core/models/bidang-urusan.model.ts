import { Deserializable } from '@models';

export class BidangUrusan implements Deserializable {
  [x: string]: any;
  error: number;
  data: BidangUrusanData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class BidangUrusanData implements Deserializable {
  [x: string]: any;
  id: number;
  kode_bidang_urusan: string; // tslint:disable-line: variable-name
  nama_bidang_urusan: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class BidangUrusanMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
