import { Deserializable } from '@models';

export class FeedbackMap implements Deserializable {
  [x: string]: any;
  score: string;
  tujuan: [
    {
      nama: string;
    }
  ];
  tujuan_tercapai: boolean; // tslint:disable-line: variable-name
  tujuan_mudah_ditemukan: boolean; // tslint:disable-line: variable-name
  sektor: [
    {
      nama: string;
    }
  ];
  saran: string;
  masalah: [
    {
      nama: string;
    }
  ];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
