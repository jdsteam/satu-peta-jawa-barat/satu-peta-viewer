import { Deserializable } from '@models';

export class MapsetLaw implements Deserializable {
  [x: string]: any;
  error: number;
  data: MapsetLawData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetLawData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string; // tslint:disable-line: variable-name
  year: number; // tslint:disable-line: variable-name
  url: number; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetLawMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
