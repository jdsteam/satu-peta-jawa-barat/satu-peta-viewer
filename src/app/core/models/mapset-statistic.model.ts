import { Deserializable } from '@models';

export class MapsetStatistic implements Deserializable {
  [x: string]: any;
  error: number;
  data: MapsetStatisticData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetStatisticData implements Deserializable {
  [x: string]: any;
  id: number;
  mapset: number; // tslint:disable-line: variable-name
  skpd: number; // tslint:disable-line: variable-name
  regional: number; // tslint:disable-line: variable-name
  metadata_sikambing: number; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetStatisticMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
