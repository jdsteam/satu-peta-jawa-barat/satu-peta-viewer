import { Deserializable, RegionalData } from '@models';

export class Sektoral implements Deserializable {
  [x: string]: any;
  message: string;
  data: SektoralData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class SektoralData implements Deserializable {
  [x: string]: any;
  id: string;
  name: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
