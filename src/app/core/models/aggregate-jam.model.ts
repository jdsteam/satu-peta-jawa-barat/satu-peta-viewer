import { Deserializable } from '@models';

export class AggregateJam implements Deserializable {
  [x: string]: any;
  error: number;
  data: AggregateJamData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class AggregateJamData implements Deserializable {
  [x: string]: any;
  id: number;
  time: string;
  kemendagri_kabupaten_kode: string;
  kemendagri_kabupaten_nama: string;
  street: string;
  level: string;
  avg_delay: number;
  avg_length: number;
  avg_speed_kmh: number;
  total_records: number;
  date: string;
  geometry: object;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
