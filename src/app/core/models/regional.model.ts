import { Deserializable, RegionalLevel } from '@models';

export class Regional implements Deserializable {
  [x: string]: any;
  message: string;
  data: RegionalData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RegionalData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_level: RegionalLevel;
  kode_bps: number;
  nama_bps: string;
  kode_kemendagri: string;
  nama_kemendagri: string;
  status_interoperabilitas: string;
  image: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
