import { Deserializable } from '@models';

export class MapsetAward implements Deserializable {
  [x: string]: any;
  error: number;
  data: MapsetAwardData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetAwardData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string; // tslint:disable-line: variable-name
  organizer_name: string; // tslint:disable-line: variable-name
  description: string; // tslint:disable-line: variable-name
  image: string; // tslint:disable-line: variable-name
  cdate: string; // tslint:disable-line: variable-name
  mdate: string; // tslint:disable-line: variable-name
  is_active: boolean; // tslint:disable-line: variable-name
  is_deleted: boolean; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetAwardMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
