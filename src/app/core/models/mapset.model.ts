import { Deserializable } from '@models';

export class Mapset implements Deserializable {
  [x: string]: any;
  error: number;
  data: MapsetData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_level_id: number; // tslint:disable-line: variable-name
  kode_bps: number; // tslint:disable-line: variable-name
  kode_kemendagri: number; // tslint:disable-line: variable-name
  nama_bps: string; // tslint:disable-line: variable-name
  nama_kemendagri: string; // tslint:disable-line: variable-name
  notes: null;
  mapset: [];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
