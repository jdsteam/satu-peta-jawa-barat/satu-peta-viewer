import { Deserializable } from '@models';

export class IpData implements Deserializable {
  [x: string]: any;
  ip: string;
  version: string;
  city: string;
  region: string;
  region_code: string;
  country: string;
  country_name: string;
  country_code: string;
  country_code_iso3: string;
  country_capital: string;
  country_tld: string;
  continent_code: string;
  in_eu: boolean;
  postal: string;
  latitude: string;
  longitude: string;
  timezone: string;
  utc_offset: string;
  country_calling_code: string;
  currency: string;
  currency_name: string;
  languages: string;
  country_area: string;
  country_population: string;
  asn: string;
  org: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
