import { Deserializable, SkpdData } from '@models';

export class MapsetThematicSource implements Deserializable {
  [x: string]: any;
  error: number;
  data: MapsetThematicSourceData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetThematicSourceData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string; // tslint:disable-line: variable-name
  datatype: string; // tslint:disable-line: variable-name
  year: string; // tslint:disable-line: variable-name
  mapset_thematic_id: string; // tslint:disable-line: variable-name
  kode_skpd: string; // tslint:disable-line: variable-name
  skpd: SkpdData[]; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetThematicSourceMetadata implements Deserializable {
  [x: string]: any;
  last_update: string; // tslint:disable-line: variable-name
  data_source: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
