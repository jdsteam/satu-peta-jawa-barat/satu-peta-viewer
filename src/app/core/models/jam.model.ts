import { Deserializable } from '@models';

export class Jam implements Deserializable {
  [x: string]: any;
  error: number;
  data: JamData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class JamData implements Deserializable {
  [x: string]: any;
  id: number;
  pub_millis: number; // tslint:disable-line: variable-name
  pub_utc_date: number; // tslint:disable-line: variable-name
  pub_locale_date: number; // tslint:disable-line: variable-name
  start_node: string; // tslint:disable-line: variable-name
  end_node: string; // tslint:disable-line: variable-name
  road_type: number; // tslint:disable-line: variable-name
  street: string;
  city: string;
  country: string;
  delay: string;
  speed: string;
  speed_kmh: string; // tslint:disable-line: variable-name
  length: string;
  turn_type: string; // tslint:disable-line: variable-name
  level: string;
  blocking_alert_uuid: string; // tslint:disable-line: variable-name
  line: [];
  type: string;
  turn_line: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
