import { Deserializable } from '@models';

export class Alert implements Deserializable {
  [x: string]: any;
  error: number;
  data: AlertData;
  message: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class AlertData implements Deserializable {
  [x: string]: any;
  id: number;
  pub_millis: number; // tslint:disable-line: variable-name
  pub_utc_date: number; // tslint:disable-line: variable-name
  pub_locale_date: number; // tslint:disable-line: variable-name
  road_type: number; // tslint:disable-line: variable-name
  location: {
    x: number;
    y: number;
  };
  street: string;
  city: string;
  country: string;
  magvar: string;
  reliability: string;
  report_description: string; // tslint:disable-line: variable-name
  report_rating: string; // tslint:disable-line: variable-name
  confidence: string;
  type: string;
  subtype: string;
  report_by_municipality_user: string; // tslint:disable-line: variable-name
  n_thumbs_up: string; // tslint:disable-line: variable-name

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
