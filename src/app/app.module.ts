import { CommonModule } from '@angular/common';
import { NgModule, ErrorHandler, APP_INITIALIZER } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgrxModule } from '@store/ngrx.module';
import { CookieModule } from 'ngx-cookie';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
import { LottieModule } from 'ngx-lottie';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// layout about and help
import { NavbarComponent } from './core/components/layout-1/navbar/navbar.component';
import { FooterComponent } from './core/components/layout-1/footer/footer.component';
import { ContainerComponent } from './core/components/layout-1/container/container.component';
import { NavbarItem } from './core/components/layout-1/navbar/item/item';

// layout home
import { ContainerHomeComponent } from './core/components/layout-2/container/container.component';
import { NavbarHomeComponent } from './core/components/layout-2/navbar/navbar.component';
import { FooterHomeComponent } from './core/components/layout-2/footer/footer.component';

import { CookieService } from 'ngx-cookie-service';
import * as Sentry from '@sentry/angular';
import player from 'lottie-web';

import { environment } from '../environments/environment';
import { NgxHotjarModule, NgxHotjarRouterModule } from 'ngx-hotjar';
import { RedirectGuard } from './core/guards/redirect.guard';

// Note we need a separate function as it's required
// by the AOT compiler.
export function playerFactory() {
  return player;
}
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    ContainerComponent,
    ContainerHomeComponent,
    NavbarHomeComponent,
    FooterHomeComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    CookieModule.forRoot(),
    RouterModule.forRoot(AppRoutingModule, {
      scrollPositionRestoration: 'top',
      useHash: true,
      relativeLinkResolution: 'legacy',
    }),
    NgSelectModule,
    NgbModule,
    BrowserAnimationsModule,
    LottieModule.forRoot({ player: playerFactory }),
    SweetAlert2Module.forRoot(),
    NgrxModule,
    NgxHotjarModule.forRoot(environment.hotjarId),
    NgxHotjarRouterModule,
    ToastrModule.forRoot(), // ToastrModule added
    FontAwesomeModule,
  ],
  exports: [NgbModule, NgSelectModule],
  bootstrap: [AppComponent],
  providers: [
    RedirectGuard,
    NavbarItem,
    CookieService,
    {
      provide: ErrorHandler,
      useValue: Sentry.createErrorHandler({
        showDialog: false,
      }),
    },
    {
      provide: Sentry.TraceService,
      deps: [Router],
    },
    {
      provide: APP_INITIALIZER,
      useFactory: () => () => {},
      deps: [Sentry.TraceService],
      multi: true,
    },
  ],
})
export class AppModule {}
