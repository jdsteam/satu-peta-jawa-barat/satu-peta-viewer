import { Component } from '@angular/core';
import { faAngleUp, faAngleDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent {
  public isCollapsed = {
    goals: false,
    regulation: false,
  };

  icon = {
    faAngleUp,
    faAngleDown,
  };
}
