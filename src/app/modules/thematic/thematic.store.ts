import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';

export interface FilterThematic {
  sort: string;
  skip: number;
  limit: number;
  search: string | null;
  kode_skpd: [];
  sektoral_id: [];
  map_type: [];
}

export interface Pagination {
  perPage: number;
  currentPage: number;
  totalItems: number;
  totalPage: number;
}

export interface ThematicState {
  filter: FilterThematic;
  pagination: Pagination;
}

const DEFAULT_STATE: ThematicState = {
  filter: {
    sort: 'mdate:desc',
    skip: 0,
    limit: 20,
    search: '',
    kode_skpd: [],
    sektoral_id: [],
    map_type: [],
  },
  pagination: {
    perPage: 20,
    currentPage: 1,
    totalItems: 1,
    totalPage: 1,
  },
};

@Injectable()
export class ThematicStore extends ComponentStore<ThematicState> {
  constructor(private router: Router, private route: ActivatedRoute) {
    super(DEFAULT_STATE);
  }

  // *********** Updaters *********** //
  readonly setFilter = this.updater((state, value: FilterThematic) => ({
    ...state,
    filter: value,
  }));

  readonly setPagination = this.updater((state, value: Pagination) => ({
    ...state,
    pagination: value,
  }));

  // *********** Selectors *********** //
  readonly getFilter$ = this.select(({ filter }) => filter);
  readonly getPagination$ = this.select(({ pagination }) => pagination);

  // ViewModel of Paginator component
  readonly vm$ = this.select(this.state$, this.getFilter$, (state, getFilter) => ({
    filter: state.filter,
    getFilter,
  }));
}
