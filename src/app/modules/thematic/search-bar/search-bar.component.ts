import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { isEmpty } from 'lodash';
import { Observable, of } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as _ from 'lodash';
import { select, Store } from '@ngrx/store';

// SERVICE
import { MapsetThematicService } from '@services';

// MODEL
import { SkpdData, SektoralData } from '@models';

// STORE
import {
  selectAllSkpd,
  selectIsLoading as selectIsLoadingSkpd,
  selectError as selectErrorSkpd,
} from '@store/skpd/skpd.selectors';
import { fromSkpdActions } from '@store/skpd/skpd.actions';

import {
  selectAllSektoral,
  selectIsLoading as selectIsLoadingSektoral,
  selectError as selectErrorSektoral,
} from '@store/sektoral/sektoral.selectors';
import { fromSektoralActions } from '@store/sektoral/sektoral.actions';
import { ThematicStore } from '../thematic.store';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  maptypeOptions = [
    {
      label: 'Peta Layout',
      value: 'layout',
    },
    {
      label: 'Peta Interaktif',
      value: 'interactive',
    },
  ];

  filter: any;

  // DATA
  skpdData$: Observable<SkpdData[]>;
  skpdIsLoading$: Observable<boolean>;
  skpdIsError$: Observable<boolean>;
  skpdIsError = false;
  skpdErrorMessage: string;

  sektoralData$: Observable<SektoralData[]>;
  sektoralIsLoading$: Observable<boolean>;
  sektoralIsError$: Observable<boolean>;
  sektoralIsError = false;
  sektoralErrorMessage: string;

  constructor(
    private store: Store<any>,
    private cdRef: ChangeDetectorRef,
    private mapsetThematicService: MapsetThematicService,
    private thematicStore: ThematicStore
  ) {
    this.thematicStore.getFilter$.subscribe((val) => {
      this.filter = val;
    });

    this.skpdData$ = this.store.pipe(
      select(selectAllSkpd),
      filter((val) => val.length !== 0)
    );
    this.skpdIsLoading$ = this.store.pipe(select(selectIsLoadingSkpd));

    this.sektoralData$ = this.store.pipe(
      select(selectAllSektoral),
      filter((val) => val.length !== 0)
    );
    this.sektoralIsLoading$ = this.store.pipe(select(selectIsLoadingSektoral));
  }

  ngOnInit(): void {
    this.initData();
  }

  initData(): void {
    this.getDataSektoral();
    this.getDataSkpd();
  }

  filterSearch(event: string): void {
    const value = isEmpty(event) === false ? event : '';

    this.filter.search = value;
    this.filter = Object.assign({}, this.filter);
    this.thematicStore.setFilter(this.filter);
    this.cdRef.detectChanges();
  }

  filterSkpd(event: any[]): void {
    if (event instanceof Array) {
      const pick = _.map(event, (object) => {
        return _.values(_.pick(object, ['kode_skpd'])).toString();
      });

      this.filter.kode_skpd = pick;
      this.filter = Object.assign({}, this.filter);
      this.thematicStore.setFilter(this.filter);
      this.cdRef.detectChanges();
    }
  }

  filterSektoral(event: any[]): void {
    if (event instanceof Array) {
      const pick = _.map(event, (object) => {
        return _.values(_.pick(object, ['id'])).toString();
      });

      this.filter.sektoral_id = pick.map((b) => {
        return Number(b);
      });

      this.filter = Object.assign({}, this.filter);
      this.thematicStore.setFilter(this.filter);
      this.cdRef.detectChanges();
    }
  }

  filterMaptype(event: any[]): void {
    if (event instanceof Array) {
      const pick = _.map(event, (object) => {
        return _.values(_.pick(object, ['value'])).toString();
      });

      this.filter.map_type = pick;
      this.filter = Object.assign({}, this.filter);
      this.thematicStore.setFilter(this.filter);
      this.cdRef.detectChanges();
    }
  }

  filterClearAll(type: string, element: any) {
    element.close();

    if (type === 'skpd') {
      this.filterSkpd([]);
    } else if (type === 'sektoral') {
      this.filterSektoral([]);
    } else if (type === 'map_type') {
      this.filterMaptype([]);
    }
  }

  // getter
  getDataSkpd(): void {
    const pSort = `nama_skpd:asc`;
    const pWhere = JSON.stringify({
      is_deleted: false,
    });

    const params = new URLSearchParams({
      sort: pSort,
      where: pWhere,
    }).toString();

    this.store.dispatch(
      fromSkpdActions.loadSkpd({
        params: '?' + params,
      })
    );
  }

  getDataSektoral(): void {
    const pSort = `name:asc`;
    const pWhere = JSON.stringify({
      is_deleted: false,
      is_satupeta: true,
    });

    const params = new URLSearchParams({
      sort: pSort,
      where: pWhere,
    }).toString();

    this.store.dispatch(
      fromSektoralActions.loadSektoral({
        params: '?' + params,
      })
    );
  }
}
