import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { environment } from '../../../../environments/environment';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DetailComponent } from './../detail/detail.component';

// STORE
import { ThematicStore } from '../thematic.store';
import {
  selectAllMapsetThematic,
  selectIsLoading,
  selectError,
} from '@store/mapset-thematic/mapset-thematic.selectors';
import { MapsetThematicData } from '@models';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  detailComponent = DetailComponent;
  detailModalRef: NgbModalRef;
  apiData = environment.apiData;
  sortOptions = [
    {
      label: 'Abjad',
      value: 'title:asc',
    },
    {
      label: 'Terbaru',
      value: 'mdate:desc',
    },
  ];
  filter: any;
  pagination: any;

  // DATA
  data$: Observable<MapsetThematicData[]>;
  isLoading$: Observable<boolean>;

  constructor(
    private store: Store<any>,
    private thematicStore: ThematicStore,
    private modalService: NgbModal
  ) {
    this.thematicStore.getFilter$.subscribe((val) => {
      this.filter = val;
    });

    this.thematicStore.getPagination$.subscribe((val) => {
      this.pagination = val;
    });

    this.data$ = this.store.pipe(select(selectAllMapsetThematic));
    this.isLoading$ = this.store.pipe(select(selectIsLoading));
  }

  ngOnInit(): void {}

  filterSort(event: string): void {
    this.filter = Object.assign({}, this.filter);
    this.thematicStore.setFilter(this.filter);
  }

  openDetail(data: MapsetThematicData) {
    this.detailModalRef = this.modalService.open(this.detailComponent, {
      size: 'lg',
      backdropClass: 'modal-backdrop',
      windowClass: 'modal-detail-thematic',
      ariaLabelledBy: 'modal-detail-thematic',
      centered: true,
    });
    this.detailModalRef.componentInstance.data = data;
  }
}
