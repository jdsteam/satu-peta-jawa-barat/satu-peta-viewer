import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  @Input() public data;
  cursorNav = 'general';
  apiData = environment.apiData;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {}

  onClickNav(val) {
    this.cursorNav = val;
  }

  download(url: string): void {
    this.http
      .get(url, {
        responseType: 'blob',
      })
      .subscribe((res) => {
        saveAs(res);
      });
  }

  getMapType(val): any {
    switch (val) {
      case 'interactive':
        return 'Peta Interaktif';
      case 'layout':
        return 'Peta Layout';
      default:
        break;
    }
  }

  getDataType(datatype: string): string {
    switch (datatype) {
      case 'base':
        return 'Peta Dasar';
      case 'thematic':
        return 'Peta Tematik';
      default:
        return '-';
    }
  }
}
