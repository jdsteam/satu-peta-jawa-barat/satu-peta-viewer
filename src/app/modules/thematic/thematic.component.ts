import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { MapsetThematicService } from '@services';
import { fromMapsetThematicActions } from '@store/mapset-thematic/mapset-thematic.actions';
import { ThematicStore } from './thematic.store';

@Component({
  selector: 'app-thematic',
  templateUrl: './thematic.component.html',
  styleUrls: ['./thematic.component.scss'],
  providers: [ThematicStore],
})
export class ThematicComponent implements OnInit {
  filter: any;
  pagination: any;

  constructor(
    private mapsetThematicService: MapsetThematicService,
    private thematicStore: ThematicStore,
    private store: Store<any>
  ) {
    this.thematicStore.getFilter$.subscribe((val) => {
      this.filter = val;
      console.log();

      this.getDataMapsetThematic();
    });

    this.thematicStore.getPagination$.subscribe((val) => {
      this.pagination = val;
    });
  }

  ngOnInit(): void {}

  filterPagination(): void {
    this.pagination = Object.assign({}, this.pagination);
    this.thematicStore.setPagination(this.pagination);

    this.filter.limit = this.pagination.perPage;
    this.filter.skip = this.pagination.perPage * (this.pagination.currentPage - 1);
    this.filter = Object.assign({}, this.filter);
    this.thematicStore.setFilter(this.filter);
  }

  getDataMapsetThematic(): void {
    const pSort = this.filter.sort;
    const pWhere = JSON.stringify({
      is_deleted: false,
      sektoral_id: this.filter.sektoral_id,
      map_type: this.filter.map_type,
    });

    const params = new URLSearchParams({
      sort: pSort,
      where: pWhere,
      search: this.filter.search,
      skip: this.filter.skip,
      limit: this.filter.limit,
    }).toString();

    this.mapsetThematicService.getTotal('?' + params).subscribe((res) => {
      this.pagination.totalItems = res.data.count;
      this.thematicStore.setPagination(this.pagination);
    });

    this.store.dispatch(
      fromMapsetThematicActions.loadMapsetThematic({
        params: '?' + params,
      })
    );
  }
}
