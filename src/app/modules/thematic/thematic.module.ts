import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ThematicComponent } from './thematic.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { ThematicStore } from './thematic.store';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';

export const routes: Routes = [
  {
    path: '',
    component: ThematicComponent,
  },
];

@NgModule({
  declarations: [ThematicComponent, SearchBarComponent, ListComponent, DetailComponent],
  entryComponents: [DetailComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes), NgbNavModule],
  providers: [ThematicStore],
})
export class ThematicModule {}
