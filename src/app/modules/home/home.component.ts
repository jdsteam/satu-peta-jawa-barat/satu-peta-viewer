import { Component, OnInit } from '@angular/core';
import {
  faChevronRight,
  faChevronLeft,
  faSearch,
  faEnvelope,
  faPhone,
  faSpinner,
  faArrowRight,
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { environment } from './../../../environments/environment';

import {
  selectIsLoading as selectMapsetIsLoading,
  selectError as selectMapsetError,
} from '@store/mapset/mapset.selectors';
import { fromMapsetActions } from '@store/mapset/mapset.actions';
import {
  selectAllMapsetLaw,
  selectIsLoading as selectMapsetLawIsLoading,
  selectError as selectMapsetLawError,
} from '@store/mapset-law/mapset-law.selectors';
import { fromMapsetLawActions } from '@store/mapset-law/mapset-law.actions';
import { MapsetLawService } from '@services';
import {
  selectIsLoading as selectMapsetStoryIsLoading,
  selectError as selectMapsetStoryError,
} from '@store/mapset-story/mapset-story.selectors';
import { fromMapsetStoryActions } from '@store/mapset-story/mapset-story.actions';
import {
  selectIsLoading as selectMapsetAwardIsLoading,
  selectError as selectMapsetAwardError,
} from '@store/mapset-award/mapset-award.selectors';
import { fromMapsetAwardActions } from '@store/mapset-award/mapset-award.actions';
import {
  selectIsLoading as selectMapsetStatisticIsLoading,
  selectError as selectMapsetStatisticError,
} from '@store/mapset-statistic/mapset-statistic.selectors';
import { fromMapsetStatisticActions } from '@store/mapset-statistic/mapset-statistic.actions';
import {
  selectIsLoading as selectWebgisIsLoading,
  selectError as selectWebgisError,
} from '@store/webgis/webgis.selectors';
import { fromWebgisActions } from '@store/webgis/webgis.actions';

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation, Swiper } from 'swiper';

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  mySwiper = new Swiper('.swiper', {});

  apiData = environment.apiData;
  icons = {
    faChevronLeft,
    faChevronRight,
    faSearch,
    faEnvelope,
    faPhone,
    faSpinner,
    faArrowLeft,
    faArrowRight,
  };

  filterMapsetLaw = {
    perPage: 10,
    currentPage: 1,
    totalItems: 1,
    totalPage: 1,
  };

  popularMapsets = [];
  mapsetLaws = [];
  mapsetStories = [];
  mapsetAwards = [];

  errors: {
    mapset$: Observable<string>;
    mapsetLaw$: Observable<string>;
    mapsetStory$: Observable<string>;
    mapsetAward$: Observable<string>;
    mapsetStatistic$: Observable<string>;
  } = {
    mapset$: null,
    mapsetLaw$: null,
    mapsetStory$: null,
    mapsetAward$: null,
    mapsetStatistic$: null,
  };

  loadings: {
    mapset$: Observable<boolean>;
    mapsetLaw$: Observable<boolean>;
    mapsetStory$: Observable<boolean>;
    mapsetAward$: Observable<boolean>;
    mapsetStatistic$: Observable<boolean>;
  } = {
    mapset$: null,
    mapsetLaw$: null,
    mapsetStory$: null,
    mapsetAward$: null,
    mapsetStatistic$: null,
  };

  constructor(private store: Store<any>, private mapsetLawService: MapsetLawService) {}

  ngOnInit(): void {
    this.initSubscribtion();
    this.getAll();
  }

  initSubscribtion() {
    this.subscribeMapsetLaw();
  }

  filterPageMapsetLaw(): void {
    this.getMapsetLaw();
  }

  subscribeMapsetLaw() {
    this.store
      .pipe(
        select(selectAllMapsetLaw),
        filter((val) => val.length !== 0)
      )
      .subscribe(async (result) => {
        this.mapsetLaws = result;
      });
  }

  getAll(): void {
    this.getMapsetPopular();
    this.getMapsetLaw();
    this.getMapsetStory();
    this.getMapsetAward();
    this.getMapsetStatistic();
    this.getWebgis();
  }

  getMapsetPopular(): void {
    const params = `?where={ 'is_popular' : true}`;

    this.store.dispatch(
      fromMapsetActions.loadMapset({
        params,
      })
    );

    this.errors.mapset$ = this.store.pipe(select(selectMapsetError));
    this.loadings.mapset$ = this.store.pipe(select(selectMapsetIsLoading));
  }

  getMapsetLaw(): void {
    let params = `?`;
    params += `limit=${this.filterMapsetLaw.perPage}&`;
    params += `skip=${this.filterMapsetLaw.perPage * (this.filterMapsetLaw.currentPage - 1)}&`;

    this.store.dispatch(
      fromMapsetLawActions.loadMapsetLaw({
        params,
      })
    );

    this.errors.mapsetLaw$ = this.store.pipe(select(selectMapsetLawError));
    this.loadings.mapsetLaw$ = this.store.pipe(select(selectMapsetLawIsLoading));

    this.mapsetLawService.getTotal(params).subscribe((res) => {
      this.filterMapsetLaw.totalItems = res.data.count;
      this.filterMapsetLaw = Object.assign({}, this.filterMapsetLaw);
    });
  }

  getMapsetStory(): void {
    let params = `?`;

    this.store.dispatch(
      fromMapsetStoryActions.loadMapsetStory({
        params,
      })
    );

    this.errors.mapsetStory$ = this.store.pipe(select(selectMapsetStoryError));
    this.loadings.mapsetStory$ = this.store.pipe(select(selectMapsetStoryIsLoading));
  }

  getMapsetAward(): void {
    let params = `?`;

    this.store.dispatch(
      fromMapsetAwardActions.loadMapsetAward({
        params,
      })
    );

    this.errors.mapsetAward$ = this.store.pipe(select(selectMapsetAwardError));
    this.loadings.mapsetAward$ = this.store.pipe(select(selectMapsetAwardIsLoading));
  }

  getMapsetStatistic(): void {
    let params = `?`;

    this.store.dispatch(
      fromMapsetStatisticActions.loadMapsetStatistic({
        params,
      })
    );

    this.errors.mapsetStatistic$ = this.store.pipe(select(selectMapsetStatisticError));
    this.loadings.mapsetStatistic$ = this.store.pipe(select(selectMapsetStatisticIsLoading));
  }

  getWebgis(): void {
    let params = `?`;

    this.store.dispatch(
      fromWebgisActions.loadWebgis({
        params,
      })
    );
  }
}
