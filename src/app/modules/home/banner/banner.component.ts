import { Component, OnInit } from '@angular/core';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { select, Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  selectMapsetStatistic,
  selectIsLoading as selectMapsetStatisticIsLoading,
  selectError as selectMapsetStatisticError,
} from '@store/mapset-statistic/mapset-statistic.selectors';
import { fromMapsetStatisticActions } from '@store/mapset-statistic/mapset-statistic.actions';
import { MapsetStatisticData } from 'src/app/core/models/mapset-statistic.model';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  error$: Observable<string> = null;
  loading$: Observable<boolean> = null;
  mapsetStatistic: MapsetStatisticData;

  icons = {
    faChevronRight,
  };

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.subscribeMapsetStatistic();

    this.error$ = this.store.pipe(select(selectMapsetStatisticError));
    this.loading$ = this.store.pipe(select(selectMapsetStatisticIsLoading));
  }

  subscribeMapsetStatistic() {
    this.store
      .pipe(
        select(selectMapsetStatistic),
        filter((val) => val !== undefined)
      )
      .subscribe(async (result) => {
        this.mapsetStatistic = result;
      });
  }

  getMapsetStatistic(): void {
    let params = `?`;

    this.store.dispatch(
      fromMapsetStatisticActions.loadMapsetStatistic({
        params,
      })
    );
  }
}
