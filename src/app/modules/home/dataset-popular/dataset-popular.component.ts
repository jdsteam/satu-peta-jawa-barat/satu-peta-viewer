import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import * as watchUtils from '@arcgis/core/core/watchUtils';
import FeatureLayer from '@arcgis/core/layers/FeatureLayer';
import { faChevronRight, faSearch } from '@fortawesome/free-solid-svg-icons';
import {
  selectAllMapset,
  selectIsLoading as selectMapsetIsLoading,
  selectError as selectMapsetError,
} from '@store/mapset/mapset.selectors';

import { fromMapsetActions } from '@store/mapset/mapset.actions';
@Component({
  selector: 'app-dataset-popular',
  templateUrl: './dataset-popular.component.html',
  styleUrls: ['./dataset-popular.component.scss'],
})
export class DatasetPopularComponent implements OnInit {
  popularMapsets = [];
  error$: Observable<string> = null;
  loading$: Observable<boolean> = null;
  icons = {
    faChevronRight,
    faSearch,
  };
  maps: {
    popular0: any;
    popular1: any;
    popular2: any;
  } = {
    popular0: null,
    popular1: null,
    popular2: null,
  };
  mapViews: {
    popular0: any;
    popular1: any;
    popular2: any;
  } = {
    popular0: null,
    popular1: null,
    popular2: null,
  };

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.subscribeMapsetPopular();
    this.error$ = this.store.pipe(select(selectMapsetError));
    this.loading$ = this.store.pipe(select(selectMapsetIsLoading));
  }

  async createMap(index) {
    await this.mapInitBasemap(index);
    if (this.popularMapsets[index]) {
      this.mapAddLayer(index, this.popularMapsets[index].app_link);
    }
  }

  mapAddLayer(index, url) {
    if (url !== undefined) {
      const layer = new FeatureLayer({
        url,
      });

      this.maps['popular' + index].add(layer);
      // this.mapFadeVisibility(this.mapViews[index], layer);
    }
  }

  mapInitBasemap(index) {
    this.maps['popular' + index] = new Map({
      basemap: 'topo',
    });

    this.mapViews['popular' + index] = new MapView({
      container: 'mapView' + index,
      map: this.maps['popular' + index],
      center: [107.60981, -6.914844],
      zoom: 6,
      ui: {
        components: ['attribution'],
      },
    });

    this.mapDisableZoom(this.mapViews['popular' + index]);
  }

  mapDisableZoom(mapView) {
    // disable +/- gestures
    mapView.on('key-down', (event) => {
      const prohibitedKeys = ['+', '-', 'Shift', '_', '='];
      const keyPressed = event.key;
      if (prohibitedKeys.indexOf(keyPressed) !== -1) {
        event.stopPropagation();
      }
    });

    // disable scroll
    mapView.on('mouse-wheel', (event) => {
      event.stopPropagation();
    });

    // disable double click
    mapView.on('double-click', (event) => {
      event.stopPropagation();
    });

    // disable drag
    mapView.on('drag', (event) => {
      event.stopPropagation();
    });
  }

  mapFadeVisibility(mapView, layer) {
    let animating = true;
    let opacity = 0;
    const finalOpacity = layer.opacity;
    layer.opacity = opacity;
    layer.when(() => {
      mapView.whenLayerView(layer).then((layerView) => {
        function incrementOpacityByFrame() {
          if (opacity >= finalOpacity && animating) {
            animating = false;
            return;
          }

          layer.opacity = opacity;
          opacity += 0.05;

          requestAnimationFrame(incrementOpacityByFrame);
        }

        watchUtils.whenFalseOnce(layerView, 'updating', (updating) => {
          requestAnimationFrame(incrementOpacityByFrame);
        });
      });
    });
  }

  subscribeMapsetPopular() {
    this.store
      .pipe(
        select(selectAllMapset),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        this.popularMapsets = result;

        for (let i = 0; i < result.length; i++) {
          setTimeout(() => this.createMap(i), 1000);
        }
      });
  }

  getMapsetPopular(): void {
    const params = `?where={ 'is_popular' : true}`;

    this.store.dispatch(
      fromMapsetActions.loadMapset({
        params,
      })
    );
  }
}
