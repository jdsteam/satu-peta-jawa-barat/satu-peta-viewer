import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetPopularComponent } from './dataset-popular.component';

describe('DatasetPopularComponent', () => {
  let component: DatasetPopularComponent;
  let fixture: ComponentFixture<DatasetPopularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatasetPopularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatasetPopularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
