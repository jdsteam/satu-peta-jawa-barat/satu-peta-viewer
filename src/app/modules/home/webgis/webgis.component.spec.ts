import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebgisComponent } from './webgis.component';

describe('WebgisComponent', () => {
  let component: WebgisComponent;
  let fixture: ComponentFixture<WebgisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebgisComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WebgisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
