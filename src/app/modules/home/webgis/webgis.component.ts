import { Component, OnInit } from '@angular/core';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { select, Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { selectAllWebgis, selectIsLoading, selectError } from '@store/webgis/webgis.selectors';
import { fromWebgisActions } from '@store/webgis/webgis.actions';
import { WebgisService } from '@services';
import { orderBy } from 'lodash';

@Component({
  selector: 'app-webgis',
  templateUrl: './webgis.component.html',
  styleUrls: ['./webgis.component.scss'],
})
export class WebgisComponent implements OnInit {
  error$: Observable<string> = null;
  loading$: Observable<boolean> = null;
  webgis = [];
  icons = {
    faChevronRight,
  };
  totalColumn = new Array(3);
  filterWebgis = {
    perPage: 10,
    currentPage: 1,
    totalItems: 1,
    totalPage: 1,
    sort: 'mdate',
    direction: 'desc',
    search: null,
  };

  constructor(private store: Store<any>, private webgisService: WebgisService) {}

  ngOnInit(): void {
    this.subscribeWebgis();
    this.error$ = this.store.pipe(select(selectError));
    this.loading$ = this.store.pipe(select(selectIsLoading));
  }

  filterPage(): void {
    this.getWebgis();
  }

  filterSort(sort): void {
    if (this.filterWebgis.sort === sort) {
      if (this.filterWebgis.direction === 'desc') {
        this.filterWebgis.direction = 'asc';
      } else if (this.filterWebgis.direction === 'asc') {
        this.filterWebgis.direction = 'desc';
      }
    } else {
      this.filterWebgis.direction = 'desc';
    }
    this.filterWebgis.sort = sort;

    this.sortData();
    // this.getWebgis();
  }

  sortData(): void {
    this.webgis = orderBy(this.webgis, [this.filterWebgis.sort], [this.filterWebgis.direction]);
  }

  subscribeWebgis() {
    this.store
      .pipe(
        select(selectAllWebgis),
        filter((val) => val.length !== 0)
      )
      .subscribe(async (result) => {
        this.webgis = result;
        this.filterWebgis.totalItems = result.length;
        this.filterWebgis = Object.assign({}, this.filterWebgis);
      });
  }

  getWebgis(): void {
    let params = `?`;
    params += `limit=${this.filterWebgis.perPage}&`;
    params += `skip=${this.filterWebgis.perPage * (this.filterWebgis.currentPage - 1)}&`;

    this.store.dispatch(
      fromWebgisActions.loadWebgis({
        params,
      })
    );

    // this.webgisService.getTotal(params).subscribe((res) => {
    //   this.filterWebgis.totalItems = res.data.count;
    //   this.filterWebgis = Object.assign({}, this.filterWebgis);
    // });
  }
}
