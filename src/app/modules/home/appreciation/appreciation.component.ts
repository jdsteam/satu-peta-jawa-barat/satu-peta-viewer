import { Component, OnInit } from '@angular/core';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { select, Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  selectAllMapsetAward,
  selectIsLoading as selectMapsetAwardIsLoading,
  selectError as selectMapsetAwardError,
} from '@store/mapset-award/mapset-award.selectors';
import { fromMapsetAwardActions } from '@store/mapset-award/mapset-award.actions';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-appreciation',
  templateUrl: './appreciation.component.html',
  styleUrls: ['./appreciation.component.scss'],
})
export class AppreciationComponent implements OnInit {
  error$: Observable<string> = null;
  loading$: Observable<boolean> = null;
  apiData = environment.apiData;
  mapsetAwards = [];
  icons = {
    faChevronRight,
  };

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.subscribeMapsetAward();

    this.error$ = this.store.pipe(select(selectMapsetAwardError));
    this.loading$ = this.store.pipe(select(selectMapsetAwardIsLoading));
  }

  subscribeMapsetAward() {
    this.store
      .pipe(
        select(selectAllMapsetAward),
        filter((val) => val.length !== 0)
      )
      .subscribe(async (result) => {
        this.mapsetAwards = result;
      });
  }

  getMapsetAward(): void {
    let params = `?`;

    this.store.dispatch(
      fromMapsetAwardActions.loadMapsetAward({
        params,
      })
    );
  }
}
