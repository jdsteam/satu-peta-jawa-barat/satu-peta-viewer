import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { HomeComponent } from './home.component';
import { BannerComponent } from './banner/banner.component';
import { DatasetPopularComponent } from './dataset-popular/dataset-popular.component';
import { StoryMapsComponent } from './story-maps/story-maps.component';
import { AppreciationComponent } from './appreciation/appreciation.component';
import { WebgisComponent } from './webgis/webgis.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
];

@NgModule({
  declarations: [
    HomeComponent,
    BannerComponent,
    DatasetPopularComponent,
    StoryMapsComponent,
    AppreciationComponent,
    WebgisComponent,
  ],
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
  bootstrap: [HomeComponent],
})
export class HomeModule {}
