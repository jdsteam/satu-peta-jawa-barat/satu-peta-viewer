import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoryMapsComponent } from './story-maps.component';

describe('StoryMapsComponent', () => {
  let component: StoryMapsComponent;
  let fixture: ComponentFixture<StoryMapsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoryMapsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StoryMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
