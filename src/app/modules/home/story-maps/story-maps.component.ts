import { Component, OnInit } from '@angular/core';
import { faArrowLeft, faArrowRight, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { select, Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  selectAllMapsetStory,
  selectIsLoading as selectMapsetStoryIsLoading,
  selectError as selectMapsetStoryError,
} from '@store/mapset-story/mapset-story.selectors';
import { fromMapsetStoryActions } from '@store/mapset-story/mapset-story.actions';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-story-maps',
  templateUrl: './story-maps.component.html',
  styleUrls: ['./story-maps.component.scss'],
})
export class StoryMapsComponent implements OnInit {
  error$: Observable<string> = null;
  loading$: Observable<boolean> = null;
  mapsetStories = [];
  apiData = environment.apiData;

  icons = {
    faArrowRight,
    faArrowLeft,
    faChevronRight,
  };

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.subscribeMapsetStory();
    this.error$ = this.store.pipe(select(selectMapsetStoryError));
    this.loading$ = this.store.pipe(select(selectMapsetStoryIsLoading));
  }

  subscribeMapsetStory() {
    this.store
      .pipe(
        select(selectAllMapsetStory),
        filter((val) => val.length !== 0)
      )
      .subscribe(async (result) => {
        this.mapsetStories = result;
      });
  }

  getMapsetStory(): void {
    let params = `?`;

    this.store.dispatch(
      fromMapsetStoryActions.loadMapsetStory({
        params,
      })
    );
  }
}
