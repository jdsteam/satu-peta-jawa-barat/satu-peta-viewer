import { Component, OnDestroy, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss'],
})
export class FormLoginComponent implements OnDestroy {
  @ViewChild('failSwal') private failSwal: SwalComponent;
  @ViewChild('successSwal') private successSwal: SwalComponent;

  loading = false;
  loginSubcription: any;
  flashMessage = 'Username / password tidak sesuai';
  icon = {
    faChevronRight,
  };

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient,
    private authenticationService: AuthenticationService
  ) {}

  ngOnDestroy(): void {
    this.loginSubcription.unsubscribe();
  }

  onClickLogin(formData) {
    this.loading = true;
    if (formData.username && formData.pass) {
      this.loginSubcription = this.authenticationService.login(formData.username, formData.pass).subscribe(
        (data) => {
          if (data.error === 0) {
            this.generateToken(formData);
          } else {
            this.loading = false;
            this.flashMessage = 'Terjadi kesalahan, silahkan coba login kembali';

            this.failSwal.fire();
          }
        },
        (err) => {
          this.flashMessage = 'Terjadi kesalahan, silahkan coba login kembali';
          this.failSwal.fire();

          this.loading = false;
        }
      );
    } else {
      this.loading = false;
      this.flashMessage = 'Username / password tidak sesuai';
      this.failSwal.fire();
    }
  }

  generateToken(data) {
    const formData: any = new FormData();
    formData.append('username', data.user);
    formData.append('password', data.pass);
    formData.append('f', 'json');
    formData.append('referer', 'https://arcgis.jabarprov.go.id/portal/sharing/rest');

    this.httpClient
      .post('https://arcgis.jabarprov.go.id/portal/sharing/rest/generateToken', formData)
      .subscribe(
        (response: { token: ''; expires: {} }) => {
          this.cookieService.set('arcgis-token', response.token, response.expires);
          this.loading = false;
          console.log(response);
          this.successSwal.fire().then((result) => {
            location.reload();
          });
        },
        (error) => {
          this.loading = false;
          this.successSwal.fire().then((result) => {
            location.reload();
          });
          console.log(error);
        }
      );
  }
}
