import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ElementRef,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  Type,
  OnDestroy,
  AfterViewInit,
  Renderer2,
} from '@angular/core';
import { faAngleRight, faAngleLeft, faLayerGroup } from '@fortawesome/free-solid-svg-icons';
import { FiltercardComponent } from '../filtercard/filtercard.component';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Subject } from 'rxjs';
import { ExploreMapsetService } from '../../services/explore-mapset.service';
import { UtilLayerService } from '../../services/util-layer.service';
import { ExploreMapsetComponent } from './../explore-mapset/explore-mapset.component';
import { filter, takeUntil } from 'rxjs/operators';
import { Options } from 'ng5-slider';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { LogService } from '@services';
import { WazeService } from '../../services/waze.service';
import { fromMapsetStatisticActions } from '@store/mapset-statistic/mapset-statistic.actions';
import { select, Store } from '@ngrx/store';
import { WazeInfoComponent } from './../waze-info/waze-info.component';
import {
  selectAllMapsetStatistic,
  selectMapsetStatistic,
} from '@store/mapset-statistic/mapset-statistic.selectors';
import { CookieService } from 'ngx-cookie-service';
import { NavigationService } from '../../services/navigation.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

interface MapsetNode {
  id: string;
  name: string;
  category: string;
  children?: MapsetNode[];
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('container', { read: ViewContainerRef, static: true }) container: ViewContainerRef;
  @ViewChild('tree') tree: ElementRef;
  @ViewChild('navigation') navigation: ElementRef;

  @Output() checkedEvent = new EventEmitter<any>();
  @Output() visibleLayer = new EventEmitter<any>();
  @Output() clearGeometry = new EventEmitter<any>();
  @Output() showCoordinates = new EventEmitter<any>();
  @Output() geometryQueryBy = new EventEmitter<any>();

  choosedMapsetSubsctibtion: any;
  isActiveWaze = false;
  isCollapseMapsetList = [];
  isShowActiveLayer = [];
  listMapsetOnMap = [];
  listChoosedMapset = [];
  filterCardMapset = [];
  activeDetail: {};
  treeControl = new NestedTreeControl<MapsetNode>((node) => node.children);
  dataSource = new MatTreeNestedDataSource<MapsetNode>();
  minValue = 50;
  maxValue = 200;
  optTransparency: Options = {
    floor: 0,
    ceil: 100,
    vertical: true,
  };
  valTransparency = 100;
  closeResult: string;
  components = [];
  filtercardComponentClass = FiltercardComponent;
  filterCardService: any;
  exploreMapsetComponent = ExploreMapsetComponent;
  defListMapset: any;
  modalReference: any;
  geometryQuery: any;
  mapCenter = [-122.4194, 37.7749];
  basemapType = 'satellite';
  mapZoomLevel = 12;
  optionsChecked = [];
  htmlFilterLayer: any;
  options = ['Area', 'Point', 'Line', 'Feature'];
  optionsMap = {
    OptionA: false,
    OptionB: false,
    OptionC: false,
    OptionD: false,
  };

  role = 'admin';
  listMenu: any;
  mapset = [];

  icon = {
    faAngleRight,
    faAngleLeft,
    faLayerGroup,
  };
  isShowCoordinates: any;
  isShowTransparency = false;
  inputSearch: any;
  isValidMenu = true;
  unsubscribe$: Subject<void> = new Subject<void>();
  unikId = 0;

  mapsetStatistic = {
    mapset: 0,
    skpd: 0,
    regional: 0,
  };
  modalWazeInfo: NgbModalRef;

  // Log
  log = null;
  deviceInfo = null;
  token = null;

  hasChild = (_: number, node: MapsetNode) => !!node.children && node.children.length > 0;

  constructor(
    private route: ActivatedRoute,
    private logService: LogService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private modalService: NgbModal,
    private exploreMapsetService: ExploreMapsetService,
    private utilLayerService: UtilLayerService,
    private renderer: Renderer2,
    private wazeService: WazeService,
    private store: Store<any>,
    private cookieService: CookieService,
    private navigationService: NavigationService
  ) {}

  ngOnInit() {
    this.isShowCoordinates = false;
    this.exploreMapsetService.setDefaultMenu(null);
    this.activeDetail = {
      isActive: false,
    };

    this.initSubcribtion();
    this.getMapsetStatistic();
  }

  ngAfterViewInit(): void {
    this.navigationService.slideNav.subscribe((val) => {
      this.controlNav(val);
    });
    // this.controlNav('close');
  }

  ngOnDestroy() {
    this.choosedMapsetSubsctibtion.unsubscribe();
    if (this.modalReference) {
      this.modalReference.close();
    }
  }

  initSubcribtion() {
    this.choosedMapsetSubsctibtion = this.exploreMapsetService.choosedMapsetSubject.subscribe((data) => {
      this.filterCardMapset = [];
      this.listChoosedMapset = data;

      for (const key in data) {
        if (key !== undefined) {
          this.filterCardMapset[data[key].id] = [];
        }
      }
    });

    this.exploreMapsetService.isCloseModalSubject.pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
      if (res === true) {
        this.modalReference.close();
      }
    });

    this.wazeService.isCloseWazeInfo.subscribe((res) => {
      if (res === true) {
        if (this.modalWazeInfo !== undefined) {
          this.modalWazeInfo.close();
        }
      }
    });

    this.store
      .pipe(
        select(selectMapsetStatistic),
        filter((val) => val !== undefined)
      )
      .subscribe((result) => {
        this.mapsetStatistic = result;
      });

    this.route.paramMap.subscribe((params: ParamMap) => {
      const id = params.get('id');
      if (id) {
        this.searchByCategory('skpd');
        this.exploreMapsetService.setActiveDetailMap(parseInt(id));
      }
    });
  }

  open() {
    this.modalReference = this.modalService.open(this.exploreMapsetComponent, {
      size: 'xl',
      backdropClass: 'modal_backdrop-explorer-data',
      windowClass: 'modal-explorer-data',
      ariaLabelledBy: 'modal-explorer-data',
      centered: true,
    });
    this.modalReference.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );

    this.exploreMapsetService.setCloseModal(false);
    // setTimeout(this.modalReference.close(), 8000)
  }

  openExploreModal() {
    this.exploreMapsetService.setModal(true);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updateOptions(val, event: any) {
    const check = event.target.checked;
    const x = document.getElementsByClassName('cb-' + val) as HTMLCollectionOf<HTMLInputElement>;
    if (check === true) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < x.length; i++) {
        x[i].checked = true;
      }
    } else {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < x.length; i++) {
        x[i].checked = false;
      }
    }

    this.updateChecked(val);
    this.sendMapset();
  }

  updateChecked(val) {
    const index = this.optionsChecked.indexOf(val);
    if (index === -1) {
      // val not found, pushing onto array
      this.optionsChecked.push(val);
    } else {
      // val is found, removing from array
      this.optionsChecked.splice(index, 1);
    }
  }

  sendMapset() {
    this.checkedEvent.emit(this.listMapsetOnMap);
  }

  clickCollapsibleMenu(id) {
    const componentClicked = document.getElementById(id);
    componentClicked.classList.toggle('active');
    const content = componentClicked.nextElementSibling as HTMLElement;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = '100%';
    }
  }

  clickAboutMapset(content, id) {
    this.exploreMapsetService.setActiveDetailMap(id);

    this.open();
  }

  openNav() {
    document.getElementById('mySidenav').style.left = '0';
    document.getElementById('main').style.marginLeft = '250px';

    if (document.getElementById('gridDisplay')) {
      document.getElementById('gridDisplay').style.left = '250px';
      document.getElementById('map').style.marginLeft = '250px';
      document.getElementById('btnFilterOpen').style.left = '250px';
      document.getElementById('btnFilterClose').style.left = '250px';
      document.getElementById('btnFilterOpen').style.transition = '0.5s';
      document.getElementById('btnFilterClose').style.transition = '0.5s';
    }

    const x = document.getElementsByClassName('openBtn') as HTMLCollectionOf<HTMLElement>;
    x[0].style.display = 'none';

    const y = document.getElementsByClassName('closeBtn') as HTMLCollectionOf<HTMLElement>;
    y[0].style.display = 'inline';
  }

  closeNav() {
    document.getElementById('mySidenav').style.left = '-250px';
    document.getElementById('main').style.marginLeft = '0';

    if (document.getElementById('gridDisplay')) {
      document.getElementById('gridDisplay').style.left = '0';
      document.getElementById('btnFilterOpen').style.left = '0';
      document.getElementById('btnFilterClose').style.left = '0';
      document.getElementById('btnFilterOpen').style.transition = '0.5s';
      document.getElementById('btnFilterClose').style.transition = '0.5s';
    }

    const x = document.getElementsByClassName('openBtn') as HTMLCollectionOf<HTMLElement>;
    x[0].style.display = 'inline';

    const y = document.getElementsByClassName('closeBtn') as HTMLCollectionOf<HTMLElement>;
    y[0].style.display = 'none';
  }

  addComponent(componentClass: Type<any>, id = '') {
    this.unikId += 1;
    // Create component dynamically inside the ng-template

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentClass);
    const component = this.container.createComponent(componentFactory);

    // Push the component so that we can keep track of which components are created
    this.components.push(component);

    const unikId = this.unikId;
    component.instance.unikId = unikId;
    component.instance.clickRemove.subscribe((data) => this.removeComponent(component, unikId));
  }

  removeComponent(componentToRemove, unikId) {
    this.container.remove(this.container.indexOf(componentToRemove));
  }

  removeFilterCard(id) {
    id = id.split('_');
    // tslint:disable-next-line: radix
    const at = parseInt(id[1]);
    const index = this.filterCardMapset[id[0]].indexOf(at);

    this.filterCardMapset[id[0]].splice(index, 1);
  }

  setDetailMap(id) {
    const findMapset = this.mapset.find((element) => {
      return element.id === id;
    });

    this.activeDetail = {
      isActive: true,
      name: findMapset.name,
      description: findMapset.description,
    };
  }

  addLayerToMap(mapset) {
    const checkMapset = this.listMapsetOnMap.find((element) => {
      return element.id === mapset.id;
    });

    if (checkMapset === undefined) {
      this.listMapsetOnMap.push(mapset);
    }

    this.isShowActiveLayer[mapset.id] = true;

    this.sendMapset();
  }

  showHideLayer(mapset) {
    this.listMapsetOnMap = this.listMapsetOnMap.filter((obj) => {
      return obj.id !== mapset.id;
    });

    this.isShowActiveLayer[mapset.id] = false;
    this.checkedEvent.emit(this.listMapsetOnMap);
  }

  removeMapset(mapset) {
    // set filter
    this.filterCardMapset[mapset.id] = [];

    this.listChoosedMapset = this.listChoosedMapset.filter((obj) => {
      return obj.id !== mapset.id;
    });

    this.exploreMapsetService.setRemoveMapset(mapset);
    this.exploreMapsetService.setChoosedMapset(this.listChoosedMapset);
  }

  removeAllMapset() {
    this.isShowActiveLayer = [];
    this.listChoosedMapset = [];
    this.geometryQuery = '';
    this.exploreMapsetService.setRemoveAllMapset(true);
  }

  checkListLayer(mapset) {
    if (this.isShowActiveLayer[mapset.id]) {
      this.isShowActiveLayer[mapset.id] = false;
      // this.removeLayerFromMap(mapset)
    } else {
      this.isShowActiveLayer[mapset.id] = true;
      this.addLayerToMap(mapset);
    }
  }

  collapseMapsetList(id) {
    if (this.isCollapseMapsetList[id]) {
      this.isCollapseMapsetList[id] = false;
    } else {
      this.isCollapseMapsetList[id] = true;
    }
  }

  addFilter(id) {
    const countArr = this.filterCardMapset[id].length;
    this.filterCardMapset[id].push(countArr);
  }

  searchByCategory(category) {
    this.postLog('click|search-bar');
    this.exploreMapsetService.setActiveCategoryMenu(category);
    this.exploreMapsetService.setActiveDetailMap(null);
    this.utilLayerService.setSearchLayer({
      text: this.inputSearch !== undefined ? this.inputSearch.toLowerCase() : '',
      category,
    });

    this.open();
  }

  setClearGeometry(event) {
    this.clearGeometry.emit(event);
  }

  // show Coordinates
  toggleShowCoordinates() {
    this.postLog('click|show-coordinate');
    this.isShowCoordinates = !this.isShowCoordinates;

    this.showCoordinates.emit(this.isShowCoordinates);
  }

  // query by geometry
  geometryButtonsClickHandler(geometryType) {
    if (this.geometryQuery === geometryType) {
      this.geometryQuery = '';
      this.setClearGeometry(geometryType);
    } else {
      this.geometryQuery = geometryType;
      this.geometryQueryBy.emit(geometryType);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    const listChoosedMapset = this.listChoosedMapset;
    moveItemInArray(listChoosedMapset, event.previousIndex, event.currentIndex);

    listChoosedMapset.forEach((element, index) => {
      const data = {
        mapsetId: element.id,
        index: listChoosedMapset.length - index + 1,
      };

      this.utilLayerService.setReorderLayer(data);
    });
  }

  controlNav(status) {
    if (status === 'open') {
      this.renderer.setStyle(this.navigation.nativeElement, 'left', '0');
      document.getElementById('map').style.marginLeft = '350px';
      document.getElementById('main').style.marginLeft = '350px';
      const x = document.getElementsByClassName('openBtnNav') as HTMLCollectionOf<HTMLElement>;
      x[0].style.display = 'none';

      const y = document.getElementsByClassName('closeBtnNav') as HTMLCollectionOf<HTMLElement>;
      y[0].style.display = 'inline';
      if (document.getElementById('total-layer-info') !== null) {
        document.getElementById('total-layer-info').style.left = '-250px';
      }
    } else {
      this.renderer.setStyle(this.navigation.nativeElement, 'left', '-350px');
      document.getElementById('map').style.marginLeft = '0';
      document.getElementById('main').style.marginLeft = '0';
      const x = document.getElementsByClassName('openBtnNav') as HTMLCollectionOf<HTMLElement>;
      x[0].style.display = 'inline';

      const y = document.getElementsByClassName('closeBtnNav') as HTMLCollectionOf<HTMLElement>;
      y[0].style.display = 'none';
      if (document.getElementById('total-layer-info') !== null) {
        document.getElementById('total-layer-info').style.left = '0px';
      }
    }
  }

  postLog(page) {
    const pageInfo = {
      page,
    };

    const log = this.logService.getLog();
    const token = this.logService.getToken();

    const postLog = {
      log: { ...pageInfo, ...log },
      token,
    };

    const logPageWorker = new Worker(new URL('@workers/log-page.worker', import.meta.url), { type: 'module' });
    logPageWorker.postMessage(postLog);
  }

  toggleWaze() {
    const isAgree = this.cookieService.get('is-agree-waze-info')
      ? JSON.parse(this.cookieService.get('is-agree-waze-info'))
      : false;
    this.isActiveWaze = !this.isActiveWaze;
    if (this.isActiveWaze && !isAgree) {
      this.openWazeInfo();
    }

    this.wazeService.setIsActiveWaze(this.isActiveWaze);
  }

  openWazeInfo() {
    this.modalWazeInfo = this.modalService.open(WazeInfoComponent, {
      backdropClass: 'modal_backdrop-feedback',
      windowClass: 'modal-waze-info',
      ariaLabelledBy: 'modal-waze-info',
      centered: true,
    });
  }

  getMapsetStatistic() {
    const params = '';
    this.store.dispatch(
      fromMapsetStatisticActions.loadMapsetStatistic({
        params,
      })
    );
  }
}
