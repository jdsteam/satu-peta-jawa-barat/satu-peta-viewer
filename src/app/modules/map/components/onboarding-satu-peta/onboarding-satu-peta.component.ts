import { Component, OnDestroy, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AnimationOptions } from 'ngx-lottie';
import { OnboardingSatuPetaService } from '../../services/';
import { NgxSpinnerService } from 'ngx-spinner';
import { NavigationService } from '../../services/navigation.service';

@Component({
  selector: 'app-onboarding-satu-peta',
  templateUrl: './onboarding-satu-peta.component.html',
  styleUrls: ['./onboarding-satu-peta.component.scss'],
})
export class OnboardingSatuPetaComponent implements OnInit, OnDestroy {
  activePage = 1;
  isAgree = false;
  lottieOptions = [];
  lastPage = 7;

  constructor(
    private onboardingSatuPetaService: OnboardingSatuPetaService,
    private cookieService: CookieService,
    private spinner: NgxSpinnerService,
    private navigationService: NavigationService
  ) { }

  ngOnInit(): void {
    this.spinner.show();

    setTimeout(() => { this.spinner.hide(); }, 1000);
    // tslint:disable-next-line:max-line-length
    this.isAgree = this.cookieService.get('is-agree-onboarding-satu-peta') ? JSON.parse(this.cookieService.get('is-agree-onboarding-satu-peta')) : false;

    for (let i = 1; i <= this.lastPage; i++) {
      const option: AnimationOptions = {
        path: `assets/json/onboarding-satupeta/scene-${i}/scene-${i}.json`,
      };
      this.lottieOptions.push(
        option
      );
    }
  }

  ngOnDestroy() {
    this.navigationService.setSlideNav('open');
  }

  closeModal() {
    this.onboardingSatuPetaService.isCloseOnboardingSatuPeta.emit(true);
  }

  setActivePage(val) {
    this.activePage = val;
  }

  toggleDone() {
    const expiredDate = new Date();
    expiredDate.setDate(expiredDate.getDate() + 7);

    this.cookieService.set('is-agree-onboarding-satu-peta', this.isAgree.toString(), expiredDate);
    this.closeModal();
  }

  totalPage(i: number) {
    return new Array(i);
  }
}
