import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderSatupetaComponent } from './loader-satupeta.component';

describe('LoaderSatupetaComponent', () => {
  let component: LoaderSatupetaComponent;
  let fixture: ComponentFixture<LoaderSatupetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoaderSatupetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderSatupetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
