import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';
import { LoaderSatupetaService } from '../../services';

@Component({
  selector: 'app-loader-satupeta',
  templateUrl: './loader-satupeta.component.html',
  styleUrls: ['./loader-satupeta.component.scss'],
})
export class LoaderSatupetaComponent implements OnInit {
  AnimationOption: AnimationOptions = {
    path: `assets/json/SatuPetaLoader.json`,
  };
  isLoading = true;

  constructor(private loaderSatupetaService: LoaderSatupetaService) {
    this.loaderSatupetaService.isLoading.subscribe((data) => {
      this.isLoading = data;
    });
  }

  ngOnInit(): void {}
}
