import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExploreMapsetService } from '../../services/explore-mapset.service';
import { ExploreMapsetComponent } from './../explore-mapset/explore-mapset.component';

import { UtilLayerService } from '../../services/util-layer.service';
import { Options } from 'ng5-slider';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  faCaretDown,
  faCaretUp,
  faEye,
  faEyeSlash,
  faInfoCircle,
  faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';
import { LogService } from '@services';

@Component({
  selector: 'app-mapset-setting',
  templateUrl: './mapset-setting.component.html',
  styleUrls: ['./mapset-setting.component.scss'],
})
export class MapsetSettingComponent implements OnInit {
  @Input() mapset: any;
  @Output() isRemove = new EventEmitter<any>();

  exploreMapsetComponent = ExploreMapsetComponent;
  modalReference: any;
  unsubscribe$: Subject<void> = new Subject<void>();
  isCollapseMapset = true;
  isShowLayer = true;
  isShowTransparency = false;
  optTransparency: Options = {
    floor: 0,
    ceil: 100,
  };
  valTransparency = 100;
  filterCardMapset = [];
  icon = {
    faCaretDown,
    faCaretUp,
    faEyeSlash,
    faEye,
    faInfoCircle,
    faTrashAlt,
  };

  // Log
  log = null;
  deviceInfo = null;
  token = null;

  constructor(
    private logService: LogService,
    private modalService: NgbModal,
    private exploreMapsetService: ExploreMapsetService,
    private utilLayerService: UtilLayerService
  ) { }

  ngOnInit() {
    this.exploreMapsetService.isCloseModalSubject.pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
      if (res === true) {
        if (this.modalReference !== undefined) {
          this.modalReference.close();
        }
      }
    });

    if (this.mapset.mapset_type.name === 'Polygon') {
      this.valTransparency = 60;
    }
  }

  showHideLayer() {
    const data = {
      mapsetId: this.mapset.id,
      transparency: 0,
    };

    this.isShowLayer = !this.isShowLayer;

    if (this.isShowLayer) {
      data.transparency = this.valTransparency / 100;
    }

    this.utilLayerService.setTransparencyLayer(data);
  }

  collapseMapset() {
    this.isCollapseMapset = !this.isCollapseMapset;
  }

  toggleShowTransparency() {
    this.isShowTransparency = !this.isShowTransparency;
  }

  setTransparency(val) {
    const transparency = val / 100;
    const data = {
      mapsetId: this.mapset.id,
      transparency,
    };

    this.utilLayerService.setTransparencyLayer(data);
    this.isShowLayer = true;
  }

  setValTransparency(val) {
    this.postLog('change|transparency|layer-' + this.mapset.id);
    this.valTransparency = val.replace('%', '');
    this.isShowLayer = true;
  }

  clickZoomExtent() {
    this.utilLayerService.setZoomToLayer(this.mapset.id);
  }

  clickAboutMapset(id) {
    this.exploreMapsetService.setActiveDetailMap(id);
    this.exploreMapsetService.setActiveCategoryMenu(this.mapset.activeCategory);
    this.openModal();
  }

  openModal() {
    this.modalReference = this.modalService.open(this.exploreMapsetComponent, {
      size: 'xl',
      backdropClass: 'modal_backdrop-explorer-data',
      windowClass: 'modal-explorer-data',
      ariaLabelledBy: 'modal-explorer-data',
      centered: true,
    });
    this.exploreMapsetService.setCloseModal(false);
  }

  removeMapset() {
    this.isRemove.emit(this.mapset);
  }

  removeFilterCard(id) {
    id = id.split('_');
    // tslint:disable-next-line: radix
    const at = parseInt(id[1]);
    const index = this.filterCardMapset.indexOf(at);
    this.filterCardMapset.splice(index, 1);
  }

  addFilter() {
    this.postLog('click|add-filter|layer-' + this.mapset.id);
    const countArr = this.filterCardMapset.length;
    this.filterCardMapset.push(countArr);
  }

  postLog(page) {
    const pageInfo = {
      page,
    };

    const log = this.logService.getLog();
    const token = this.logService.getToken();

    const postLog = {
      log: { ...pageInfo, ...log },
      token,
    };

    const logPageWorker = new Worker(new URL('@workers/log-page.worker', import.meta.url), { type: 'module' });
    logPageWorker.postMessage(postLog);
  }
}
