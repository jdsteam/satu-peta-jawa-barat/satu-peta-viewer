import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MapsetSettingComponent } from './mapset-setting.component';

describe('MapsetSettingComponent', () => {
  let component: MapsetSettingComponent;
  let fixture: ComponentFixture<MapsetSettingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MapsetSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapsetSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
