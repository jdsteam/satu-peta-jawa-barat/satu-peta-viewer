import { Component, ElementRef, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { UtilLayerService } from '../../services/util-layer.service';
import { WazeService } from '../../services/waze.service';
import _ from 'lodash-es';
import { filter } from 'rxjs/internal/operators/filter';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { ExploreMapsetService } from '../../services';
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-detail-selected-mapset',
  templateUrl: './detail-selected-mapset.component.html',
  styleUrls: ['./detail-selected-mapset.component.scss'],
})
export class DetailSelectedMapsetComponent implements OnInit, AfterViewInit {
  @ViewChild('navheader') private navheaderEl: ElementRef;
  active = 3;
  startIndexMapset = 5;
  dataTableSelectedMapset = [];
  dataTableSelectedWazeCrash = {
    column: [],
    data: [],
  };
  dataTableSelectedWazeFlood = {
    column: [],
    data: [],
  };
  dataTableSelectedWazeJam = {
    column: [],
    data: [],
  };
  dataTableSelectedWazeAggregateJam = {
    column: [],
    data: [],
  };
  dataTableSelectedWazePothole = {
    column: [],
    data: [],
  };
  isActiveWaze = false;
  listChoosedMapset = [];
  icons = {
    faChevronLeft,
    faChevronRight,
  };
  isOverflow = false;
  scroll = 0;
  endScroll = 0;

  constructor(
    private utilLayerService: UtilLayerService,
    private wazeService: WazeService,
    private exploreMapsetService: ExploreMapsetService,
  ) { }

  ngOnInit() {
    this.initSubscribtion();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.isOverflow = this.navheaderEl.nativeElement.scrollWidth > this.navheaderEl.nativeElement.clientWidth;
      this.endScroll = this.navheaderEl.nativeElement.scrollWidth - this.navheaderEl.nativeElement.clientWidth;
    }, 200);

  }

  initSubscribtion() {
    this.wazeService.isActiveWaze.subscribe((data) => {
      this.isActiveWaze = data;
    });
    this.utilLayerService.detailSelectedMapset.subscribe((data) => {
      this.listChoosedMapset.forEach((element, index) => {
        const prevIndex = data.findIndex((mapset) => {
          return mapset.id === element.id.toString();
        });
        moveItemInArray(data, prevIndex, index);
      });
      this.setDataTable(data);
    });
    this.wazeService.selectedAggregateJam.pipe(filter((val) => val.length !== 0)).subscribe((data) => {
      if (this.isActiveWaze) {
        this.setDataTableWazeJam([]);
        this.setDataTableWazeAggregateJam(data);
      }
    });
    this.wazeService.selectedCrash.pipe(filter((val) => val.length !== 0)).subscribe((data) => {
      if (this.isActiveWaze) {
        this.setDataTableWazeCrash(data);
      }
    });
    this.wazeService.selectedFlood.pipe(filter((val) => val.length !== 0)).subscribe((data) => {
      if (this.isActiveWaze) {
        this.setDataTableWazeFlood(data);
      }
    });
    this.wazeService.selectedJam.pipe(filter((val) => val.length !== 0)).subscribe((data) => {
      if (this.isActiveWaze) {
        this.setDataTableWazeAggregateJam([]);
        this.setDataTableWazeJam(data);
      }
    });
    this.wazeService.selectedPothole.pipe(filter((val) => val.length !== 0)).subscribe((data) => {
      if (this.isActiveWaze) {
        this.setDataTableWazePothole(data);
      }
    });
    this.utilLayerService.reorderLayer.subscribe((data) => {
      const prevIndex = this.dataTableSelectedMapset.findIndex((mapset) => {
        return mapset.id === data.mapsetId.toString();
      });
      const index = data.index - 2;
      moveItemInArray(this.dataTableSelectedMapset, prevIndex, this.dataTableSelectedMapset.length - index - 1);
    });

    this.exploreMapsetService.choosedMapsetSubject.subscribe((data) => {
      this.listChoosedMapset = data;
    });
  }

  setDataTable(data) {
    const dataTableSelectedMapset = [];
    const excludes = ['esriFieldTypeOID', 'esriFieldTypeGeometry'];

    this.active = 6;
    this.dataTableSelectedMapset = dataTableSelectedMapset;
    _.each(data, (element) => {
      const dataTable = {
        id: element.id,
        name: element.name,
        data: [],
        column: [],
      };

      dataTable.data = element.data;
      element.fields.forEach((elField) => {
        if (!excludes.includes(elField.type)) {
          dataTable.column.push({
            id: elField.fieldName,
            name: elField.label,
          });
        }
      });

      dataTableSelectedMapset.push(dataTable);
    });

    this.dataTableSelectedMapset = dataTableSelectedMapset;
  }

  setDataTableWazeAggregateJam(data) {
    const column = [
      {
        id: 'id',
        name: 'Id',
      },
      {
        id: 'time',
        name: 'Time',
      },
      {
        id: 'level',
        name: 'Level',
      },
      {
        id: 'street',
        name: 'Nama Jalan',
      },
      {
        id: 'city',
        name: 'Wilayah',
      },
      {
        id: 'country',
        name: 'Kode Negara',
      },
      {
        id: 'avg_speed_kmh',
        name: 'Kecepatan Rata-Rata',
      },
    ];

    this.dataTableSelectedWazeAggregateJam = {
      column,
      data,
    };

    this.active = 5;
  }

  setDataTableWazeCrash(data) {
    const column = [
      {
        id: 'id',
        name: 'Id',
      },
      {
        id: 'time',
        name: 'Time',
      },
      {
        id: 'location',
        name: 'Koordinat X dan Koordinat Y',
      },
      {
        id: 'street',
        name: 'Nama Jalan',
      },
      {
        id: 'city',
        name: 'Wilayah',
      },
      {
        id: 'country',
        name: 'Kode Negara',
      },
      {
        id: 'reliability',
        name: 'Reliabilitas Laporan',
      },
      {
        id: 'report_rating',
        name: 'Rating Laporan',
      },
      {
        id: 'subtype',
        name: 'Tipe Laporan',
      },
    ];

    this.dataTableSelectedWazeCrash = {
      column,
      data,
    };

    this.active = 1;
  }

  setDataTableWazeFlood(data) {
    const column = [
      {
        id: 'id',
        name: 'Id',
      },
      {
        id: 'time',
        name: 'Time',
      },
      {
        id: 'location',
        name: 'Koordinat X dan Koordinat Y',
      },
      {
        id: 'street',
        name: 'Nama Jalan',
      },
      {
        id: 'city',
        name: 'Wilayah',
      },
      {
        id: 'country',
        name: 'Kode Negara',
      },
      {
        id: 'reliability',
        name: 'Reliabilitas Laporan',
      },
      {
        id: 'report_rating',
        name: 'Rating Laporan',
      },
      {
        id: 'subtype',
        name: 'Tipe Laporan',
      },
    ];

    this.dataTableSelectedWazeFlood = {
      column,
      data,
    };

    this.active = 2;
  }

  setDataTableWazeJam(data) {
    const column = [
      {
        id: 'id',
        name: 'Id',
      },
      {
        id: 'time',
        name: 'Time',
      },
      {
        id: 'level',
        name: 'Level',
      },
      {
        id: 'street',
        name: 'Nama Jalan',
      },
      {
        id: 'city',
        name: 'Wilayah',
      },
      {
        id: 'country',
        name: 'Kode Negara',
      },
      {
        id: 'speedkmh',
        name: 'Kecepatan',
      },
      {
        id: 'subtype',
        name: 'Tipe Laporan',
      },
    ];

    this.dataTableSelectedWazeJam = {
      column,
      data,
    };

    this.active = 3;
  }

  setDataTableWazePothole(data) {
    const column = [
      {
        id: 'id',
        name: 'Id',
      },
      {
        id: 'time',
        name: 'Time',
      },
      {
        id: 'location',
        name: 'Koordinat X dan Koordinat Y',
      },
      {
        id: 'street',
        name: 'Nama Jalan',
      },
      {
        id: 'city',
        name: 'Wilayah',
      },
      {
        id: 'country',
        name: 'Kode Negara',
      },
      {
        id: 'reliability',
        name: 'Reliabilitas Laporan',
      },
      {
        id: 'report_rating',
        name: 'Rating Laporan',
      },
      {
        id: 'subtype',
        name: 'Tipe Laporan',
      },
    ];

    this.dataTableSelectedWazePothole = {
      column,
      data,
    };

    this.active = 4;
  }

  scrollLeft() {
    this.navheaderEl.nativeElement.scrollTo({ left: (this.navheaderEl.nativeElement.scrollLeft - 150), behavior: 'smooth' });

    this.scroll = this.navheaderEl.nativeElement.scrollLeft - 150;
  }
  scrollRight() {
    this.navheaderEl.nativeElement.scrollTo({ left: (this.navheaderEl.nativeElement.scrollLeft + 150), behavior: 'smooth' });
    this.scroll = this.navheaderEl.nativeElement.scrollLeft + 150;
  }
}
