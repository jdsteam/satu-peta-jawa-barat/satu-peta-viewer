import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import {
  faCaretDown,
  faCaretUp,
  faCheck,
  faEye,
  faEyeSlash,
  faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';
import { Options } from 'ng5-slider';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LogService } from '@services';
import { WazeService } from '../../services/';
import { select, Store } from '@ngrx/store';
import { fromAlertCrashActions } from '@store/alert-crash/alert-crash.actions';
import { fromAlertFloodActions } from '@store/alert-flood/alert-flood.actions';
import { fromAlertPotholeActions } from '@store/alert-pothole/alert-pothole.actions';
import { WazeInfoComponent } from './../waze-info/waze-info.component';
import * as moment from 'moment';
import { skip } from 'rxjs/operators';
import { selectAllAlertCrash } from '@store/alert-crash/alert-crash.selectors';
import { selectAllAlertFlood } from '@store/alert-flood/alert-flood.selectors';
import { selectAllAlertPothole } from '@store/alert-pothole/alert-pothole.selectors';
import { WazeAlertInfoComponent } from '../waze-alert-info/waze-alert-info.component';

@Component({
  selector: 'app-waze-setting',
  templateUrl: './waze-setting.component.html',
  styleUrls: ['./waze-setting.component.scss'],
})
export class WazeSettingComponent implements OnInit, OnDestroy {
  moment: any = moment;
  icons = {
    faCaretDown,
    faCaretUp,
    faCheck,
    faEye,
    faEyeSlash,
    faInfoCircle,
  };
  isCollapsed = true;
  isActiveAlert = {
    crash: false,
    flood: false,
    pothole: false,
  };
  isNullAlert = {
    crash: false,
    flood: false,
    pothole: false,
  };
  isShowLayer = true;
  optTransparency: Options = {
    floor: 0,
    ceil: 100,
  };
  modalWazeInfo: NgbModalRef;
  modalWazeAlertInfo: NgbModalRef;
  opacity = 1;
  valTransparency = 100;
  wazeService: WazeService;

  constructor(
    private store: Store,
    private injector: Injector,
    private logService: LogService,
    private modalService: NgbModal
  ) {
    this.wazeService = injector.get<WazeService>(WazeService);
    this.initSubcription();
  }

  ngOnInit(): void {
    this.wazeService.setIsActiveWaze(true);

    this.wazeService.isCloseWazeInfo.subscribe((res) => {
      if (res === true) {
        if (this.modalWazeInfo !== undefined) {
          this.modalWazeInfo.close();
        }
      }
    });

    this.wazeService.isCloseWazeAlertInfo.subscribe((res) => {
      if (res === true) {
        if (this.modalWazeAlertInfo !== undefined) {
          this.modalWazeAlertInfo.close();
        }
      }
    });
  }

  ngOnDestroy() {}

  initSubcription() {
    this.subscribeWazeAlertCrash();
    this.subscribeWazeAlertFlood();
    this.subscribeWazeAlertPothole();
  }

  showHideLayer() {
    let transparency = 0;
    this.isShowLayer = !this.isShowLayer;

    if (this.isShowLayer) {
      transparency = this.opacity;
    }
    this.wazeService.setTransparencyAlerts(transparency);
  }

  setTransparency(val) {
    this.opacity = val / 100;
    this.wazeService.setTransparencyAlerts(this.opacity);
    this.isShowLayer = true;
  }

  setValTransparency(val) {
    this.postLog('change|transparency|layer-waze');
    this.valTransparency = val.replace('%', '');
    this.isShowLayer = true;
  }

  openWazeInfo() {
    this.modalWazeInfo = this.modalService.open(WazeInfoComponent, {
      backdropClass: 'modal_backdrop-feedback',
      windowClass: 'modal-waze-info',
      ariaLabelledBy: 'modal-waze-info',
      centered: true,
    });
  }

  openWazeAlertInfo() {
    if (!this.modalService.hasOpenModals()) {
      this.modalWazeAlertInfo = this.modalService.open(WazeAlertInfoComponent, {
        backdropClass: 'modal_backdrop-feedback',
        windowClass: 'modal-waze-alert-info',
        ariaLabelledBy: 'modal-waze-alert-info',
        centered: true,
      });
    }
  }

  toggleAlertCrash(e) {
    this.isActiveAlert.crash = e.target.checked;
    if (e.target.checked) {
      this.getAlertCrash();
    }

    this.wazeService.setIsActiveWazeAlertCrash(e.target.checked);
  }

  toggleAlertFlood(e) {
    this.isActiveAlert.flood = e.target.checked;
    if (e.target.checked) {
      this.getAlertFlood();
    }

    this.wazeService.setIsActiveWazeAlertFlood(e.target.checked);
  }

  toggleAlertPothole(e) {
    this.isActiveAlert.pothole = e.target.checked;
    if (e.target.checked) {
      this.getAlertPothole();
    }

    this.wazeService.setIsActiveWazeAlertPothole(e.target.checked);
  }

  postLog(page) {
    const pageInfo = {
      page,
    };

    const log = this.logService.getLog();
    const token = this.logService.getToken();

    const postLog = {
      log: { ...pageInfo, ...log },
      token,
    };

    const logPageWorker = new Worker(new URL('@workers/log-page.worker', import.meta.url), { type: 'module' });
    logPageWorker.postMessage(postLog);
  }

  getAlertCrash() {
    let params = `?subtype=accident`;

    this.store.dispatch(
      fromAlertCrashActions.loadAlertCrash({
        params,
      })
    );
  }

  getAlertFlood() {
    let params = `?subtype=flood`;

    this.store.dispatch(
      fromAlertFloodActions.loadAlertFlood({
        params,
      })
    );
  }

  getAlertPothole() {
    let params = `?subtype=pothole`;

    this.store.dispatch(
      fromAlertPotholeActions.loadAlertPothole({
        params,
      })
    );
  }

  subscribeWazeAlertCrash() {
    this.store
      .pipe(select(selectAllAlertCrash))
      .pipe(skip(1))
      .subscribe((result) => {
        if (result.length === 0) {
          this.isNullAlert.crash = true;
          this.openWazeAlertInfo();
        } else {
          this.isNullAlert.crash = false;
        }
        this.wazeService.setIsNullAlert(this.isNullAlert);
      });
  }

  subscribeWazeAlertFlood() {
    this.store
      .pipe(select(selectAllAlertFlood))
      .pipe(skip(1))
      .subscribe((result) => {
        if (result.length === 0) {
          this.isNullAlert.flood = true;
          this.openWazeAlertInfo();
        } else {
          this.isNullAlert.flood = false;
        }
        this.wazeService.setIsNullAlert(this.isNullAlert);
      });
  }

  subscribeWazeAlertPothole() {
    this.store
      .pipe(select(selectAllAlertPothole))
      .pipe(skip(1))
      .subscribe((result) => {
        if (result.length === 0) {
          this.isNullAlert.pothole = true;
          this.openWazeAlertInfo();
        } else {
          this.isNullAlert.pothole = false;
        }
        this.wazeService.setIsNullAlert(this.isNullAlert);
      });
  }
}
