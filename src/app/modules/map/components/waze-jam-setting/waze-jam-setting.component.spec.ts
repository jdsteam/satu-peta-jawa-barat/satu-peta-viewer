import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WazeJamSettingComponent } from './waze-jam-setting.component';

describe('WazeJamSettingComponent', () => {
  let component: WazeJamSettingComponent;
  let fixture: ComponentFixture<WazeJamSettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WazeJamSettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WazeJamSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
