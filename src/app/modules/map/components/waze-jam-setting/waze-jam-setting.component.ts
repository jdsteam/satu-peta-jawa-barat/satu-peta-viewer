import { Component, OnDestroy, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';
import { WazeService } from '../../services/';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { fromAggregateJamActions } from '@store/aggregate-jam/aggregate-jam.actions';
import { fromIrregularityActions } from '@store/irregularity/irregularity.actions';
import { fromJamActions } from '@store/jam/jam.actions';

@Component({
  selector: 'app-waze-jam-setting',
  templateUrl: './waze-jam-setting.component.html',
  styleUrls: ['./waze-jam-setting.component.scss'],
})
export class WazeJamSettingComponent implements OnInit, OnDestroy {
  activeDayIndex = 1;
  activeDayLabel = 'Senin';
  activeTimeLabel = '08.00';
  icons = {
    faEye,
    faEyeSlash,
  };
  isShowLayer = true;
  listDay = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  listShortDay = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
  listLastWeekDate = [];
  opacity = 100;
  optTime: Options = {
    floor: 6,
    ceil: 22,
  };
  selectedJamType = 'realtime';
  time = '2021-08-08 18:00:00';
  valTime = 8;

  constructor(private store: Store, private wazeService: WazeService, config: NgbTooltipConfig) {
    config.container = 'body';
  }

  ngOnInit(): void {
    this.initLoadJamRealtime();
    this.setLastWeekDate();
  }

  ngOnDestroy() {
    this.wazeService.setActiveWazeJamType('realtime');
  }

  initLoadJamRealtime() {
    if (this.selectedJamType === 'realtime') {
      this.getIrregularity();
      this.getJam();
    }
  }

  setLastWeekDate() {
    const listLastWeekDate = [];
    const startDate = moment().subtract(1, 'weeks').startOf('week').format('YYYY-MM-DD');
    for (let i = 0; i < 7; i++) {
      listLastWeekDate.push(moment(startDate, 'YYYY-MM-DD').add(i, 'days').format('YYYY-MM-DD'));
    }

    this.listLastWeekDate = listLastWeekDate;
  }

  setTime(valTime) {
    this.getAggregateJam();
  }

  showHideLayer() {
    let transparency = 0;
    this.isShowLayer = !this.isShowLayer;

    if (this.isShowLayer) {
      transparency = this.opacity;
    }
    this.wazeService.setTransparencyJams(transparency);
  }

  toggleDay(index) {
    this.activeDayIndex = index;
    this.activeDayLabel = this.listDay[index];
    this.getAggregateJam();
  }

  toggleJamType(jamType) {
    this.selectedJamType = jamType;
    this.wazeService.setActiveWazeJamType(jamType);

    if (jamType === 'past') {
      this.getAggregateJam();
    } else {
      this.getIrregularity();
      this.getJam();
    }
  }

  toggleTime(val) {
    this.activeTimeLabel = (val < 10 ? '0' + val : val) + '.00';
    this.setTime(val);
  }

  getAggregateJam() {
    // tslint:disable-next-line: max-line-length
    const day = this.listShortDay[this.activeDayIndex];
    const params = `?day=${day}&time=${this.valTime}`;

    this.store.dispatch(
      fromAggregateJamActions.loadAggregateJam({
        params,
      })
    );
  }

  getIrregularity() {
    // tslint:disable-next-line: max-line-length
    const params = ``;

    this.store.dispatch(
      fromIrregularityActions.loadIrregularity({
        params,
      })
    );
  }

  getJam() {
    const params = ``;

    this.store.dispatch(
      fromJamActions.loadJam({
        params,
      })
    );
  }
}
