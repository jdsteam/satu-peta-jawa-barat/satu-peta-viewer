import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { WazeService } from '../../services/waze.service';

@Component({
  selector: 'app-waze-alert-info',
  templateUrl: './waze-alert-info.component.html',
  styleUrls: ['./waze-alert-info.component.scss'],
})
export class WazeAlertInfoComponent {
  message = '';
  isNullAlert: any;

  constructor(private wazeService: WazeService) {
    this.wazeService.isNullAlert.subscribe((data) => {
      this.isNullAlert = data;
      this.setMessageNullAlert();
    });
  }

  closeModal() {
    this.wazeService.isCloseWazeAlertInfo.emit(true);
  }

  setMessageNullAlert() {
    const listMessage = [];
    let message = '';

    if (this.isNullAlert.crash) {
      listMessage.push('kecelakaan lalu lintas');
    }

    if (this.isNullAlert.flood) {
      listMessage.push('banjir');
    }

    if (this.isNullAlert.pothole) {
      listMessage.push('jalan berlubang');
    }

    listMessage.forEach((element, index) => {
      if (listMessage.length > 1) {
        if (index > 0) {
          if (index === listMessage.length - 1) {
            message += ', dan ';
          } else {
            message += ', ';
          }
        }
      }
      message += element;
    });

    this.message = message;
  }
}
