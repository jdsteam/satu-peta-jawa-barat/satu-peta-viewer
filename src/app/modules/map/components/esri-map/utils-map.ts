import Graphic from '@arcgis/core/Graphic';
import * as printTask from '@arcgis/core/rest/print';
import PrintTemplate from '@arcgis/core/rest/support/PrintTemplate';
import PrintParameters from '@arcgis/core/rest/support/PrintParameters';
import SimpleFillSymbol from '@arcgis/core/symbols/SimpleFillSymbol';
import * as watchUtils from '@arcgis/core/core/watchUtils';

export default class UtilsMap {
  static clearHiglighting(highlight) {
    for (const key in highlight) {
      if (highlight.hasOwnProperty(key)) {
        highlight[key].remove();
      }
    }
  }

  static goToLocation(mapView, data) {
    mapView.goTo(data.extent);

    mapView.popup.open({
      title: '<b>' + data.location.longitude + ', ' + data.location.latitude + '</b>',
      location: data.location,
      content: data.address,
    });
  }

  static async printMap(template, mapView, utilLayerService) {
    const printTemplate = new PrintTemplate(template);
    const params = new PrintParameters({
      view: mapView,
      template: printTemplate,
    });

    printTask
      .execute(
        'https://utility.arcgisonline.com/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task',
        params
      )
      .then(
        (resolveValue) => {
          const data = {
            url: resolveValue.url,
            error: 0,
          };
          utilLayerService.setURLDownload(data);
        },
        (error) => {
          return {
            error: 1,
            message: error,
          };
        }
      );
  }

  static reorderLayer(data, maps, createdLayer) {
    maps.reorder(createdLayer[data.mapsetId], data.index);
  }

  static setBasemap(data, maps, listBasemap) {
    maps.basemap = listBasemap[data];
  }

  static async setupMiniMap(mapView, minimapView) {
    const fillSymbol = new SimpleFillSymbol({
      color: [0, 0, 0, 0.5],
      outline: null,
    });

    const extent3Dgraphic = new Graphic({
      geometry: null,
      symbol: fillSymbol,
    });

    minimapView.graphics.removeAll();

    minimapView.graphics.add(extent3Dgraphic);

    watchUtils.init(mapView, 'extent', (extent) => {
      minimapView.goTo({
        center: mapView.center,
        scale:
          mapView.scale *
          2 *
          Math.max(mapView.width / minimapView.width, mapView.height / minimapView.height),
      });
      extent3Dgraphic.geometry = extent;
    });
  }
}
