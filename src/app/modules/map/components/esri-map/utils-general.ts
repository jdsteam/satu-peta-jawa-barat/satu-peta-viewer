export default class UtilsGeneral {
  static openModal(modalReference, modalService, modalComponent, modalName) {
    modalReference = modalService.open(modalComponent, {
      backdropClass: `modal_backdrop-${modalName}`,
      windowClass: `modal-${modalName}`,
      ariaLabelledBy: `modal-${modalName}`,
      centered: true,
    });
  }
}
