import Graphic from '@arcgis/core/Graphic';
import GraphicsLayer from '@arcgis/core/layers/GraphicsLayer';
import GeoJSONLayer from '@arcgis/core/layers/GeoJSONLayer';
import PictureMarkerSymbol from '@arcgis/core/symbols/PictureMarkerSymbol';
import Point from '@arcgis/core/geometry/Point';
import Polyline from '@arcgis/core/geometry/Polyline';
import PopupTemplate from '@arcgis/core/PopupTemplate';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';
import SimpleLineSymbol from '@arcgis/core/symbols/SimpleLineSymbol';
import ColorVariable from '@arcgis/core/renderers/visualVariables/ColorVariable';
import _ from 'lodash-es';
import * as moment from 'moment';

export default class UtilsWaze {
  static createAlertLayer(alertType, data) {
    const title = {
      crash: 'Kecelakaan Lalu Lintas',
      flood: 'Banjir',
      pothole: 'Jalan Berlubang',
    };

    const icons = {
      crash: 'assets/icons/crash.png',
      flood: 'assets/icons/flood.png',
      pothole: 'assets/icons/pothole.png',
    };

    const graphics = [];

    data = _.uniqBy(data, 'uuid');

    data.forEach((element) => {
      const point = new Point({
        longitude: element.location.x,
        latitude: element.location.y,
      });

      const symbol = new PictureMarkerSymbol({
        url: icons[alertType],
        width: '32px',
        height: '32px',
      });

      const fieldInfos = [];
      fieldInfos.push(
        {
          fieldName: 'id',
          label: 'ID',
          type: 'String',
        },
        {
          fieldName: 'time',
          label: 'Time',
          type: 'String',
        },
        {
          fieldName: 'location',
          label: 'Koordinat X dan Koordinat Y',
          type: 'String',
        },
        {
          fieldName: 'street',
          label: 'Nama Jalan',
          type: 'String',
        },
        {
          fieldName: 'city',
          label: 'Wilayah',
          type: 'String',
        },
        {
          fieldName: 'country',
          label: 'Kode Negara',
          type: 'String',
        },
        {
          fieldName: 'reliability',
          label: 'Reliabilitas Laporan',
          type: 'String',
        },
        {
          fieldName: 'report_rating',
          label: 'Rating Laporan',
          type: 'String',
        },
        {
          fieldName: 'subtype',
          label: 'Tipe Laporan',
          type: 'String',
        }
      );

      const popupTemplates = new PopupTemplate({
        title: title[alertType],
        content: [
          {
            type: 'fields',
            fieldInfos,
          },
        ],
      });

      // Create a graphic and add the geometry and symbol to it
      const pointGraphic = new Graphic({
        geometry: point,
        symbol,
        attributes: {
          id: element.id,
          time: moment(element.pub_millis).format('DD-MM-YYYY HH:mm:ss'),
          location: `(${element.location.x}, ${element.location.y})`,
          street: element.street,
          city: element.city,
          country: element.country,
          reliability: element.reliability,
          report_rating: element.report_rating,
          subtype: title[alertType],
        },
        popupTemplate: popupTemplates,
      });

      graphics.push(pointGraphic);
    });
    const layer = new GraphicsLayer({
      graphics,
    });

    return layer;
  }

  static createJamLayer(data) {
    const graphics = [];
    const colorLevel = ['#1EBE69', '#5fc362', '#f2c552', '#d39a3f', '#d14918', '#5a0b0b'];
    const levelLabel = {
      1: '1 (kelancaran jalan 80% - 61%)',
      2: '2 (kelancaran jalan 60% - 41%)',
      3: '3 (kelancaran jalan 40% - 21%)',
      4: '4 (kelancaran jalan 20% - 1%)',
      5: '5 (penutupan jalan)',
    };

    data = _.uniqBy(data, 'uuid');

    data.forEach((element) => {
      const paths = [];
      element.line.line.forEach((elPoint) => {
        paths.push([elPoint.x, elPoint.y]);
      });

      const polyline = new Polyline({
        paths,
      });

      const color = colorLevel[element.level];
      const symbol = new SimpleLineSymbol({
        color,
        width: 2,
        style: element.level === '5' ? 'short-dash' : 'solid',
      });

      const fieldInfos = [];
      fieldInfos.push(
        {
          fieldName: 'id',
          label: 'ID',
          type: 'String',
        },
        {
          fieldName: 'time',
          label: 'Time',
          type: 'String',
        },
        {
          fieldName: 'street',
          label: 'Nama Jalan',
          type: 'String',
        },
        {
          fieldName: 'city',
          label: 'Wilayah',
          type: 'String',
        },
        {
          fieldName: 'country',
          label: 'Kode Negara',
          type: 'String',
        },
        {
          fieldName: 'speedkmh',
          label: 'Kecepatan',
          type: 'String',
        },
        {
          fieldName: 'level',
          label: 'Level',
          type: 'String',
        }
      );

      const popupTemplates = new PopupTemplate({
        title: 'Kepadatan Lalu Lintas',
        content: [
          {
            type: 'fields',
            fieldInfos,
          },
        ],
      });

      // Create a graphic and add the geometry and symbol to it
      const polylineGraphic = new Graphic({
        geometry: polyline,
        symbol,
        attributes: {
          id: element.id,
          time: moment(element.pub_millis).format('DD-MM-YYYY HH:mm:ss'),
          street: element.street,
          city: element.city,
          country: element.country,
          speedkmh: element.speed_kmh + ' km/jam',
          level: levelLabel[element.level],
          subtype: 'Kemacetan',
        },
        popupTemplate: popupTemplates,
      });

      graphics.push(polylineGraphic);
    });
    const layer = new GraphicsLayer({
      graphics,
    });

    return layer;
  }

  static createAggregateJamLayer(data) {
    const features = [];

    data.forEach((element) => {
      features.push({
        geometry: element.geometry,
        properties: {
          id: element.id,
          time: element.time,
          street: element.street,
          city: element.kemendagri_kabupaten_nama,
          country: 'ID',
          avg_speed_kmh: element.avg_speed_kmh,
          level: element.level,
        },
      });
    });

    const geojson = {
      type: 'FeatureCollection',
      features,
    };

    const blob = new Blob([JSON.stringify(geojson)], {
      type: 'application/json',
    });

    const fieldInfos = [
      {
        fieldName: 'id',
        label: 'ID',
        type: 'String',
      },
      {
        fieldName: 'time',
        label: 'Time',
        type: 'String',
      },
      {
        fieldName: 'street',
        label: 'Nama Jalan',
        type: 'String',
      },
      {
        fieldName: 'city',
        label: 'Wilayah',
        type: 'String',
      },
      {
        fieldName: 'country',
        label: 'Kode Negara',
        type: 'String',
      },
      {
        fieldName: 'avg_speed_kmh',
        label: 'Rata-rata kecepatan',
        type: 'String',
      },
      {
        fieldName: 'level',
        label: 'Level',
        type: 'String',
      },
    ];

    const popupTemplate = {
      title: 'Kepadatan Lalu Lintas',
      content: [
        {
          type: 'fields',
          fieldInfos,
        },
      ],
    };

    const renderer = new SimpleRenderer({
      symbol: new SimpleLineSymbol({
        color: '#000000',
        width: 1,
      }),
      visualVariables: [
        new ColorVariable({
          field: 'level',
          stops: [
            {
              value: 1,
              color: '#5fc362',
            },
            {
              value: 2,
              color: '#f2c552',
            },
            {
              value: 3,
              color: '#d39a3f',
            },
            {
              value: 4,
              color: '#d14918',
            },
            {
              value: 5,
              color: '#5a0b0b',
            },
          ],
        }),
      ],
    });

    const layer = new GeoJSONLayer({
      url: URL.createObjectURL(blob),
      copyright: 'Jabar Digital Service',
      popupTemplate,
      renderer,
      outFields: ['*'],
    });

    return layer;
  }

  static setTransparencyLayerAlerts(wazeLayers, valTransparency) {
    Object.keys(wazeLayers).forEach((key) => {
      const layer = wazeLayers[key];

      if (layer !== null) {
        layer.opacity = valTransparency;
      }
    });
  }

  static setTransparencyLayerJams(jamsLayer, valTransparency) {
    if (jamsLayer !== null) {
      jamsLayer.opacity = valTransparency;
    }
  }

  static removeWazeLayer(maps, wazeLayer, wazeLayerView) {
    if (wazeLayer !== null) {
      maps.remove(wazeLayer);
      wazeLayer = null;
      wazeLayerView = null;
    }
  }
}
