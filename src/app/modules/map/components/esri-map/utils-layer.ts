import OGCFeatureLayer from '@arcgis/core/layers/OGCFeatureLayer';
import * as watchUtils from '@arcgis/core/core/watchUtils';

export default class UtilsLayer {
  static createOGCLayer(url) {
    return new OGCFeatureLayer({
      url,
      collectionId: 'dutch_windmills',
    });
  }

  static getAttrValues(response, fieldName) {
    const features = response.features;
    return features.map((feature) => {
      return feature.attributes[fieldName];
    });
  }

  static getAttrUniqueValues(values) {
    const uniqueValues = [];

    values.forEach((item, i) => {
      if ((uniqueValues.length < 1 || uniqueValues.indexOf(item) === -1) && item !== '') {
        uniqueValues.push(item);
      }
    });
    return uniqueValues;
  }

  static checkLayerTypeByFeatureInfo(layerType, layer, layerView) {
    if (layerType === 'feature') {
      return layer;
    } else {
      return layerView;
    }
  }

  static zoomToLayer(id, mapView, createdLayer) {
    mapView.goTo(createdLayer[id].fullExtent);
  }

  static setAllFilterLayer(data, mapView, createdLayer, featureInfo) {
    for (const key in featureInfo) {
      if (featureInfo.hasOwnProperty(key)) {
        const listFilter = data.filter((x) => x.sourceId === featureInfo[key].id.toString());

        if (listFilter) {
          this.setFilterLayer(featureInfo[key].id, listFilter, mapView, createdLayer, featureInfo);
        } else {
          this.deleteFilterLayer(featureInfo[key].id, mapView, createdLayer);
        }
      }
    }
  }

  static setFilterLayer(id, data, mapView, createdLayer, featureInfo) {
    const layer = createdLayer[id];

    const condition = [];
    data.forEach((element) => {
      if (element.attributeType === 'esriFieldTypeString' || element.attributeType === 'string') {
        let valAttributes = element.valAttributes;
        valAttributes = valAttributes.join(`', '`);
        condition.push(`${element.fieldName} in ('${valAttributes}')`);
      } else if (
        element.attributeType === 'esriFieldTypeInteger' ||
        element.attributeType === 'esriFieldTypeDouble' ||
        element.attributeType === 'double'
      ) {
        condition.push(
          element.fieldName +
            ' > ' +
            element.valAttributes.minValue +
            ' and ' +
            element.fieldName +
            ' < ' +
            element.valAttributes.maxValue
        );
      }
    });

    const conditionStr = condition.join(' or ');

    mapView.whenLayerView(layer).then((layerView) => {
      layerView.filter = {
        where: conditionStr,
      };
    });

    featureInfo[id].condition = conditionStr;
  }

  static deleteFilterLayer(data, mapView, createdLayer) {
    const layer = createdLayer[data.id];
    mapView.whenLayerView(layer).then((layerView) => {
      layerView.filter = null;
    });
  }

  static setTransparencyLayer(data, mapView, createdLayer) {
    const layer = createdLayer[data.mapsetId];
    if (layer !== undefined) {
      mapView.whenLayerView(layer).then(() => {
        layer.opacity = data.transparency;
      });
    }
  }

  static async fadeVisibilityOn(layer, mapView) {
    let animating = true;
    let opacity = 0;
    // fade layer's opacity from 0 to
    // whichever value the user has configured
    const finalOpacity = layer.opacity;
    layer.opacity = opacity;

    mapView.whenLayerView(layer).then((layerView) => {
      function incrementOpacityByFrame() {
        if (opacity >= finalOpacity && animating) {
          animating = false;
          return;
        }

        layer.opacity = opacity;
        opacity += 0.05;

        requestAnimationFrame(incrementOpacityByFrame);
      }

      // Wait for tiles to finish loading before beginning the fade
      watchUtils.whenFalseOnce(layerView, 'updating', (updating) => {
        requestAnimationFrame(incrementOpacityByFrame);
      });
    });
  }

  static setLayerView(mapView, layer, returnLayerView) {
    mapView
      .whenLayerView(layer)
      .then((layerView) => {
        layerView.watch('updating', (value) => {
          if (!value) {
            returnLayerView = layerView;
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static gotoLayer(mapView, layer) {
    const query = layer.createQuery();
    query.outSpatialReference = mapView.spatialReference;
    layer.queryExtent(query).then((response) => {
      mapView.goTo(response.extent).catch((error) => {
        console.error(error);
      });
    });
  }
}
