import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, Injector } from '@angular/core';
import esri = __esri;
import {
  FilterFeatureMapService,
  UtilLayerService,
  ExploreMapsetService,
  UtilComponentService,
  WazeService,
  LoaderSatupetaService,
} from '../../services/';
import { Router } from '@angular/router';
import { LocalGeojson } from '@shared/geojson/jabar';
import {
  faAngleRight,
  faAngleLeft,
  faAngleUp,
  faAngleDown,
  faPlus,
  faMinus,
  faCompass,
  faEnvelope,
  faTimes,
  faSearchPlus,
} from '@fortawesome/free-solid-svg-icons';
import _ from 'lodash-es';
import UtilsLayer from './utils-layer';
import UtilsMap from './utils-map';
import UtilsWaze from './utils-waze';
import UtilsProgram from './utils-program';
import UtilsGeneral from './utils-general';

import Map from '@arcgis/core/Map';
import Basemap from '@arcgis/core/Basemap';
import Graphic from '@arcgis/core/Graphic';
import Request from '@arcgis/core/request';
import MapView from '@arcgis/core/views/MapView';
import SceneView from '@arcgis/core/views/SceneView';
import GraphicsLayer from '@arcgis/core/layers/GraphicsLayer';
import FeatureLayer from '@arcgis/core/layers/FeatureLayer';
import TileLayer from '@arcgis/core/layers/TileLayer';
import WebTileLayer from '@arcgis/core/layers/WebTileLayer';
import WMSLayer from '@arcgis/core/layers/WMSLayer';
import Polygon from '@arcgis/core/geometry/Polygon';
import SimpleFillSymbol from '@arcgis/core/symbols/SimpleFillSymbol';
import Compass from '@arcgis/core/widgets/Compass';
import Legend from '@arcgis/core/widgets/Legend';
import ScaleBar from '@arcgis/core/widgets/ScaleBar';
import SketchViewModel from '@arcgis/core/widgets/Sketch/SketchViewModel';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';
import PopupTemplate from '@arcgis/core/PopupTemplate';
import IdentityManager from '@arcgis/core/identity/IdentityManager';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FeedbackComponent } from './../feedback/feedback.component';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { CitarumService } from '@services';
import { LoadingBarService } from '@ngx-loading-bar/core';

import {
  selectAllJam,
  selectIsLoading as selectJamIsLoading,
  selectError as selectJamError,
} from '@store/jam/jam.selectors';

import {
  selectAllIrregularity,
  selectIsLoading as selectIrregularityIsLoading,
  selectError as selectIrregularityError,
} from '@store/irregularity/irregularity.selectors';

import {
  selectAllAlertCrash,
  selectIsLoading as selectAlertCrashIsLoading,
  selectError as selectAlertCrashError,
} from '@store/alert-crash/alert-crash.selectors';

import {
  selectAllAlertFlood,
  selectIsLoading as selectAlertFloodIsLoading,
  selectError as selectAlertFloodError,
} from '@store/alert-flood/alert-flood.selectors';

import {
  selectAllAlertPothole,
  selectIsLoading as selectAlertPotholeIsLoading,
  selectError as selectAlertPotholeError,
} from '@store/alert-pothole/alert-pothole.selectors';

import { select, Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { selectAllAggregateJam } from '@store/aggregate-jam/aggregate-jam.selectors';

@Component({
  selector: 'app-esri-map',
  templateUrl: './esri-map.component.html',
  styleUrls: ['./esri-map.component.scss'],
})
export class EsriMapComponent implements OnInit, OnDestroy {
  @ViewChild('mapViewNode', { static: true }) private mapViewEl: ElementRef;
  @ViewChild('detailSelectedMapset') private detailSelectedMapsetEl;
  @ViewChild('minimapViewNode', { static: true }) private minimapViewEl: ElementRef;
  @ViewChild('tooltip') private tooltipEl: ElementRef;
  @ViewChild('navigation') navigation: ElementRef;

  params = {
    basemap: 'topo',
    center: [107.60981, -6.914744],
    zoom: 1,
  };

  maps: any;
  mapView: any;
  featureInfo: any; // for filter data source
  createdLayer = {
    layer: [],
    view: [],
    key: [],
  };
  wazeLayerAggregateJams = null;
  wazeLayerJams = null;
  wazeLayerIrregularities = null;
  wazeLayerAlerts = {
    crash: null,
    flood: null,
    pothole: null,
  };
  wazeLayerViewAggregateJams = null;
  wazeLayerViewJams = null;
  wazeLayerViewIrregularities = null;
  wazeLayerViewAlerts = {
    crash: null,
    flood: null,
    pothole: null,
  };
  wazeLayerTransparencyAlerts = 100;
  wazeLayerTransparencyJams = 100;
  wazeHighlightSelectedAggregateJam = [];
  wazeHighlightSelectedCrash = [];
  wazeHighlightSelectedFlood = [];
  wazeHighlightSelectedJam = [];
  wazeHighlightSelectedPothole = [];
  wazeJamType = 'realtime';
  isShowWazeFlyerWarning: boolean;
  sketchLayer: any;
  sketchViewModel: any;
  sketchGeometry: any;
  onDemandGrid: any;
  storeAdapter: any;
  selection: any;
  gridFields: any;
  dataStore: any;
  grid: any;
  highlight = [];
  isShowDisplayQuery: any;
  overviewMap: any;
  minimapView: any;
  switchButtonTxt = '2D';
  graphMinimap: any;
  isShowCoordinates: any;
  isShowMinimap: any;
  isActiveWaze = false;
  subscription: any;
  choosedMapsetSubscribtion: any;

  controlDetailSelectedLayer = {
    isActive: true,
    isOpen: true,
  };

  listBasemap = {
    rbi: {},
    cartodb_dark: {},
    cartodb_light: {},
    national_geographic: 'national-geographic',
    topo: 'topo',
    dark_gray: 'dark-gray',
    gray: 'gray',
    hybrid: 'hybrid',
    oceans: 'oceans',
    osm: 'osm',
    satellite: 'satellite',
    streets: 'streets',
    terrain: 'terrain',
  };

  localGeojson = new LocalGeojson();

  icon = {
    faAngleRight,
    faAngleLeft,
    faAngleUp,
    faAngleDown,
    faPlus,
    faMinus,
    faCompass,
    faEnvelope,
    faTimes,
    faSearchPlus,
  };
  modalReference: any;
  feedbackComponent = FeedbackComponent;
  selectFeaturesGeometry: any;

  // services
  cookieService: CookieService;
  authenticationService: AuthenticationService;
  filterFeatureMapService: FilterFeatureMapService;
  utilLayerService: UtilLayerService;
  exploreMapsetService: ExploreMapsetService;
  utilComponentService: UtilComponentService;
  wazeService: WazeService;
  mapsetData: any;
  citarumService: CitarumService;
  loaderSatupetaService: LoaderSatupetaService;

  constructor(
    private store: Store,
    private injector: Injector,
    private router: Router,
    private modalService: NgbModal,
    private loadingBar: LoadingBarService
  ) {
    this.cookieService = injector.get<CookieService>(CookieService);
    this.authenticationService = injector.get<AuthenticationService>(AuthenticationService);
    this.filterFeatureMapService = injector.get<FilterFeatureMapService>(FilterFeatureMapService);
    this.utilLayerService = injector.get<UtilLayerService>(UtilLayerService);
    this.exploreMapsetService = injector.get<ExploreMapsetService>(ExploreMapsetService);
    this.utilComponentService = injector.get<UtilComponentService>(UtilComponentService);
    this.wazeService = injector.get<WazeService>(WazeService);
    this.citarumService = injector.get<CitarumService>(CitarumService);
    this.loaderSatupetaService = injector.get<LoaderSatupetaService>(LoaderSatupetaService);
  }

  ngOnInit(): void {
    this.loadingBar.start();
    this.initVar();

    this.initMap().then((result) => {
      this.houseKeeping(result);
      this.createTerritoryJabar();
    });

    this.initSubscription();
    this.loadingBar.stop();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.choosedMapsetSubscribtion.unsubscribe();
  }

  initVar() {
    this.isShowWazeFlyerWarning = false;
    this.isShowCoordinates = false;
    this.isShowDisplayQuery = false;
    this.isShowMinimap = false;
    this.createdLayer.key = [];
    this.createdLayer.view = [];
    this.createdLayer.layer = [];
    this.featureInfo = [];
    this.gridFields = [];
    this.grid = [];
    this.highlight = [];
  }

  initSubscription() {
    this.choosedMapsetSubscribtion = this.exploreMapsetService.choosedMapsetSubject.subscribe((data) => {
      this.receiveOptions(data);
    });

    this.exploreMapsetService.removeAllMapset.subscribe((data) => {
      this.removeAll();
    });

    this.subscription = this.filterFeatureMapService.filterSubject.subscribe((data) => {
      UtilsLayer.setAllFilterLayer(data, this.mapView, this.createdLayer.layer, this.featureInfo);
    });

    this.utilLayerService.transparencyLayer.subscribe((data) => {
      UtilsLayer.setTransparencyLayer(data, this.mapView, this.createdLayer.layer);
    });

    this.utilLayerService.zoomToLayer.subscribe((data) => {
      UtilsLayer.zoomToLayer(data, this.mapView, this.createdLayer.layer);
    });

    this.utilLayerService.gotoLocation.subscribe((data) => {
      UtilsMap.goToLocation(this.mapView, data);
    });

    this.utilLayerService.reorderLayer.subscribe((data) => {
      UtilsMap.reorderLayer(data, this.maps, this.createdLayer.layer);
    });

    this.utilLayerService.is3DMap.subscribe((data) => {
      this.createViewMap(data);
    });

    this.utilLayerService.activeBasemap.subscribe((data) => {
      UtilsMap.setBasemap(data, this.maps, this.listBasemap);
    });

    this.utilLayerService.printTemplate.subscribe((data) => {
      UtilsMap.printMap(data, this.mapView, this.utilLayerService);
    });

    this.wazeService.isActiveWaze.subscribe((data) => {
      this.isActiveWaze = data;
      if (!data) {
        UtilsWaze.removeWazeLayer(
          this.maps,
          this.wazeLayerAlerts['crash'],
          this.wazeLayerViewAlerts['crash']
        );
        UtilsWaze.removeWazeLayer(
          this.maps,
          this.wazeLayerAlerts['flood'],
          this.wazeLayerViewAlerts['flood']
        );
        UtilsWaze.removeWazeLayer(
          this.maps,
          this.wazeLayerAlerts['pothole'],
          this.wazeLayerViewAlerts['pothole']
        );

        UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerJams, this.wazeLayerViewJams);
        UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerAggregateJams, this.wazeLayerViewAggregateJams);
        UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerIrregularities, this.wazeLayerViewIrregularities);
      }

      setTimeout(() => {
        this.isShowWazeFlyerWarning = data;
      }, 300);
    });

    this.wazeService.activeWazeJamType.subscribe((data) => {
      this.wazeJamType = data;
      if (data === 'past') {
        UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerJams, this.wazeLayerViewJams);
        UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerIrregularities, this.wazeLayerViewIrregularities);
      } else {
        UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerAggregateJams, this.wazeLayerViewAggregateJams);
      }
    });

    this.wazeService.isActiveWazeAlertCrash.subscribe((data) => {
      if (!data) {
        UtilsWaze.removeWazeLayer(
          this.maps,
          this.wazeLayerAlerts['crash'],
          this.wazeLayerViewAlerts['crash']
        );
      }
    });

    this.wazeService.isActiveWazeAlertFlood.subscribe((data) => {
      if (!data) {
        UtilsWaze.removeWazeLayer(
          this.maps,
          this.wazeLayerAlerts['flood'],
          this.wazeLayerViewAlerts['flood']
        );
      }
    });

    this.wazeService.isActiveWazeAlertPothole.subscribe((data) => {
      if (!data) {
        UtilsWaze.removeWazeLayer(
          this.maps,
          this.wazeLayerAlerts['pothole'],
          this.wazeLayerViewAlerts['pothole']
        );
      }
    });

    this.wazeService.transparencyAlerts.subscribe((data) => {
      this.wazeLayerTransparencyAlerts = data;
      UtilsWaze.setTransparencyLayerAlerts(this.wazeLayerAlerts, data);
    });

    this.wazeService.transparencyJams.subscribe((data) => {
      this.wazeLayerTransparencyJams = data;
      UtilsWaze.setTransparencyLayerJams(this.wazeLayerJams, data);
      UtilsWaze.setTransparencyLayerJams(this.wazeLayerIrregularities, data);
      UtilsWaze.setTransparencyLayerJams(this.wazeLayerAggregateJams, data);
    });

    this.utilComponentService.isCloseModal.subscribe((res) => {
      if (res === true) {
        if (this.modalReference !== undefined) {
          this.modalReference.close();
        }
      }
    });

    this.subscribeWazeAggregateJam();
    this.subscribeWazeAlertCrash();
    this.subscribeWazeAlertFlood();
    this.subscribeWazeAlertPothole();
    this.subscribeWazeJam();
    this.subscribeWazeIrregularity();
  }

  subscribeWazeAggregateJam() {
    this.store
      .pipe(
        select(selectAllAggregateJam),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        if (this.isActiveWaze && this.wazeJamType === 'past') {
          const layerAggregateJam = UtilsWaze.createAggregateJamLayer(result);
          UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerAggregateJams, this.wazeLayerViewAggregateJams);
          this.maps.add(layerAggregateJam);
          this.wazeLayerAggregateJams = layerAggregateJam;
          layerAggregateJam.when(() => {
            UtilsLayer.setLayerView(this.mapView, layerAggregateJam, this.wazeLayerViewAggregateJams);
          });
          UtilsWaze.setTransparencyLayerJams(this.wazeLayerAggregateJams, this.wazeLayerTransparencyJams);
        }
      });
  }

  subscribeWazeJam() {
    this.store
      .pipe(
        select(selectAllJam),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        if (this.isActiveWaze && this.wazeJamType === 'realtime') {
          const layerJam = UtilsWaze.createJamLayer(result);

          UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerJams, this.wazeLayerViewJams);
          UtilsWaze.removeWazeLayer(
            this.maps,
            this.wazeLayerIrregularities,
            this.wazeLayerViewIrregularities
          );
          this.maps.add(layerJam);
          this.wazeLayerJams = layerJam;
          layerJam.when(() => {
            UtilsLayer.setLayerView(this.mapView, layerJam, this.wazeLayerViewJams);
          });

          UtilsWaze.setTransparencyLayerJams(this.wazeLayerJams, this.wazeLayerTransparencyJams);
        }
      });
  }

  subscribeWazeIrregularity() {
    this.store
      .pipe(
        select(selectAllIrregularity),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        if (this.isActiveWaze && this.wazeJamType === 'realtime') {
          const layerIrregularity = UtilsWaze.createJamLayer(result);
          UtilsWaze.removeWazeLayer(this.maps, this.wazeLayerAggregateJams, this.wazeLayerViewAggregateJams);
          UtilsWaze.removeWazeLayer(
            this.maps,
            this.wazeLayerIrregularities,
            this.wazeLayerViewIrregularities
          );
          this.maps.add(layerIrregularity);
          this.wazeLayerIrregularities = layerIrregularity;

          layerIrregularity.when(() => {
            UtilsLayer.setLayerView(this.mapView, layerIrregularity, this.wazeLayerViewIrregularities);
          });

          UtilsWaze.setTransparencyLayerJams(this.wazeLayerIrregularities, this.wazeLayerTransparencyJams);
        }
      });
  }

  subscribeWazeAlertCrash() {
    this.store
      .pipe(
        select(selectAllAlertCrash),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        this.onSubscribeWazeAlert(result, 'crash');
      });
  }

  subscribeWazeAlertFlood() {
    this.store
      .pipe(
        select(selectAllAlertFlood),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        this.onSubscribeWazeAlert(result, 'flood');
      });
  }

  subscribeWazeAlertPothole() {
    this.store
      .pipe(
        select(selectAllAlertPothole),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        this.onSubscribeWazeAlert(result, 'pothole');
      });
  }

  onSubscribeWazeAlert(data, category) {
    if (this.isActiveWaze) {
      UtilsWaze.removeWazeLayer(
        this.maps,
        this.wazeLayerAlerts[category],
        this.wazeLayerViewAlerts[category]
      );
      const layer = UtilsWaze.createAlertLayer(category, data);

      this.maps.add(layer);
      this.wazeLayerAlerts[category] = layer;

      layer.when(() => {
        UtilsLayer.setLayerView(this.mapView, layer, this.wazeLayerViewAlerts[category]);
      });

      UtilsWaze.setTransparencyLayerAlerts(this.wazeLayerAlerts, this.wazeLayerTransparencyAlerts);
    }
  }

  loadMapsetData() {
    throw new Error('Method not implemented.');
  }

  async initMap() {
    this.maps = new Map({
      basemap: 'topo',
    });
    await this.initMapView();
    this.minimapView = new MapView({
      container: this.minimapViewEl.nativeElement,
      map: new Map({
        basemap: 'topo',
      }),
      constraints: {
        rotationEnabled: false,
      },
    });
    this.minimapView.ui.components = [];
  }

  async initMapView(is3D = false, position = 'top-right') {
    const parameter = {
      container: this.mapViewEl.nativeElement,
      center: this.params.center,
      zoom: this.params.zoom,
      map: this.maps,
      constraints: {
        snapToZoom: true,
      },
    };

    const mapView = is3D === true ? new SceneView(parameter) : new MapView(parameter);
    mapView.ui.move(['zoom', 'navigation-toggle', 'compass'], position);

    if (!is3D) {
      mapView.ui.add(
        new ScaleBar({
          view: mapView,
          unit: 'dual',
        }),
        'bottom-left'
      );

      mapView.ui.add(
        new Compass({
          view: mapView,
        }),
        position
      );
    }

    this.mapView = mapView;
    this.setPopUp();
  }

  initBasemapList() {
    const mapBaseLayerCartodbLightAll = new WebTileLayer({
      urlTemplate: 'https://cartodb-basemaps-d.global.ssl.fastly.net/light_all/{level}/{col}/{row}.png',
      subDomains: ['a', 'b', 'c', 'd'],
    });

    const cartodbLightAll = new Basemap({
      baseLayers: [mapBaseLayerCartodbLightAll],
      title: 'Cartodb Light All',
      id: 'cartodb_light',
      thumbnailUrl: 'https://cartodb-basemaps-d.global.ssl.fastly.net/light_all/14/13090/8509.png',
    });

    const mapBaseLayerCartodbDarkAll = new WebTileLayer({
      urlTemplate: 'https://cartodb-basemaps-d.global.ssl.fastly.net/dark_all/{level}/{col}/{row}.png',
      subDomains: ['a', 'b', 'c', 'd'],
    });

    const cartodbDarkAll = new Basemap({
      baseLayers: [mapBaseLayerCartodbDarkAll],
      title: 'Cartodb Dark All',
      id: 'cartodbDarkAll',
      thumbnailUrl: 'https://cartodb-basemaps-d.global.ssl.fastly.net/dark_all/14/13090/8509.png',
    });

    const rbi = new Basemap({
      baseLayers: [
        new TileLayer({
          url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer',
        }),
        new TileLayer({
          url: 'https://portal.ina-sdi.or.id/arcgis/rest/services/RBI/Basemap/MapServer',
        }),
      ],
      title: 'Rupa Bumi Indonesia',
      id: 'rbi',
      thumbnailUrl: 'https://portal.ina-sdi.or.id/arcgis/rest/services/RBI/Basemap/MapServer/info/thumbnail',
    });

    this.listBasemap.rbi = rbi;
    this.listBasemap.cartodb_dark = cartodbDarkAll;
    this.listBasemap.cartodb_light = cartodbLightAll;
  }

  async createViewMap(is3D = false) {
    this.clearGeometry();
    await this.initMapView(is3D);
    this.switchButtonTxt = is3D ? '2D' : '3D';
    this.overviewMap = [];
    this.houseKeeping(this.mapView);
  }

  async createGrid() {
    for (const key in this.featureInfo) {
      if (this.featureInfo.hasOwnProperty(key)) {
        this.gridFields[key] = [];
        this.featureInfo[key].attributes.forEach((data, i) => {
          this.gridFields[key].push(data.fieldName);
        });
        if (this.grid[key]) {
          this.grid[key].clearSelection();
        }
      }
    }
  }

  async selectFeatures(geometry) {
    this.selectFeaturesGeometry = geometry;
    this.selectLayerWaze(geometry);
    this.selectLayerMapset(geometry);
  }

  selectLayerMapset(geometry) {
    const detailSelectedMapset = [];

    if (this.featureInfo === undefined) {
      this.utilLayerService.setDetailSelectedMapset(detailSelectedMapset);
    }
    for (const key in this.featureInfo) {
      if (this.featureInfo.hasOwnProperty(key)) {
        const layer = UtilsLayer.checkLayerTypeByFeatureInfo(
          this.featureInfo[key].layerType,
          this.createdLayer.layer[key],
          this.createdLayer.view[key]
        );
        const query = {
          geometry,
          outFields: ['*'],
          where: this.featureInfo[key].condition,
        };

        layer
          .queryFeatures(query)
          .then((results) => {
            const graphics = results.features;

            if (this.highlight[key]) {
              this.highlight[key].remove();
            }

            const layerHighlight = this.createdLayer.view[key];

            if (layerHighlight !== undefined) {
              this.highlight[key] = layerHighlight.highlight(graphics);
            }

            const data = graphics.map((feature) => {
              return Object.keys(feature.attributes)
                .filter((keyFeature) => {
                  return this.gridFields[key].indexOf(keyFeature) !== -1;
                })
                .reduce((obj, keyFeature) => {
                  obj[keyFeature] = feature.attributes[keyFeature];
                  return obj;
                }, {});
            });

            const fields = this.featureInfo[key].attributes;

            detailSelectedMapset.push({
              id: key,
              name: this.featureInfo[key].featureName,
              fields,
              data,
            });
            this.utilLayerService.setDetailSelectedMapset(detailSelectedMapset);
          })
          .catch((error) => {
            console.log('query failed: ', error);
          });
      }
    }
  }

  selectLayerWaze(geometry) {
    const layerViewCrash = this.wazeLayerViewAlerts.crash;
    if (layerViewCrash !== null) {
      this.selectLayerWazeCrash(geometry, layerViewCrash);
    }
    const layerViewFlood = this.wazeLayerViewAlerts.flood;
    if (layerViewFlood !== null) {
      this.selectLayerWazeFlood(geometry, layerViewFlood);
    }
    const layerViewPothole = this.wazeLayerViewAlerts.pothole;
    if (layerViewPothole !== null) {
      this.selectLayerWazePothole(geometry, layerViewPothole);
    }

    const selectedLayer = [];
    const highlightSelectedLayer = [];
    const layerViewJam = this.wazeLayerViewJams;

    if (layerViewJam !== null) {
      this.selectLayerWazeJam(geometry, layerViewJam, selectedLayer, highlightSelectedLayer);
    }
    const layerViewIrregularities = this.wazeLayerViewIrregularities;
    if (layerViewIrregularities !== null) {
      this.selectLayerWazeJam(geometry, layerViewIrregularities, selectedLayer, highlightSelectedLayer);
    }

    const layerViewAggregateJams = this.wazeLayerViewAggregateJams;
    if (layerViewAggregateJams !== null) {
      this.selectLayerWazeAggregateJams(geometry, layerViewAggregateJams);
    }
  }

  selectLayerWazeAggregateJams(geometry, layerView) {
    const selectedLayer = [];
    const query = layerView.createQuery();
    query.geometry = geometry;
    query.spatialRelationship = 'intersects';
    layerView.queryFeatures(query).then((response) => {
      const highlightSelectedLayer = [];
      response.features.forEach((element) => {
        highlightSelectedLayer.push(layerView.highlight(element));
        selectedLayer.push(element.attributes);
      });

      if (selectedLayer.length > 0) {
        this.wazeHighlightSelectedAggregateJam = highlightSelectedLayer;
        this.wazeService.setSelectedAggregateJam(selectedLayer);
      }
    });
  }

  selectLayerWazeCrash(geometry, layerView) {
    const selectedLayer = [];
    this.wazeService.setSelectedCrash(selectedLayer);
    layerView.queryGraphics().then((results) => {
      const highlightSelectedLayer = [];
      if (results !== null && geometry.extent !== null) {
        results.items.forEach((element) => {
          if (geometry.extent.intersects(element.geometry)) {
            highlightSelectedLayer.push(layerView.highlight(element));
            selectedLayer.push(element.attributes);
          }
        });
      }

      if (selectedLayer.length > 0) {
        this.wazeHighlightSelectedCrash = highlightSelectedLayer;
        this.wazeService.setSelectedCrash(selectedLayer);
      }
    });
  }

  selectLayerWazeFlood(geometry, layerView) {
    const selectedLayer = [];
    this.wazeService.setSelectedFlood(selectedLayer);

    layerView.queryGraphics().then((results) => {
      const highlightSelectedLayer = [];
      if (results !== null && geometry.extent !== null) {
        results.items.forEach((element) => {
          if (geometry.extent.intersects(element.geometry)) {
            highlightSelectedLayer.push(layerView.highlight(element));
            selectedLayer.push(element.attributes);
          }
        });
      }
      if (selectedLayer.length > 0) {
        this.wazeHighlightSelectedFlood = highlightSelectedLayer;
        this.wazeService.setSelectedFlood(selectedLayer);
      }
    });
  }

  selectLayerWazeJam(geometry, layerView, selectedLayer, highlightSelectedLayer) {
    this.wazeService.setSelectedJam(selectedLayer);
    layerView.queryGraphics().then((results) => {
      if (results !== null && geometry.extent !== null) {
        results.items.forEach((element) => {
          if (geometry.extent.intersects(element.geometry)) {
            highlightSelectedLayer.push(layerView.highlight(element));
            selectedLayer.push(element.attributes);
          }
        });
      }
      if (selectedLayer.length > 0) {
        this.wazeHighlightSelectedJam = highlightSelectedLayer;
        this.wazeService.setSelectedJam(selectedLayer);
      }
    });
  }

  selectLayerWazePothole(geometry, layerView) {
    const selectedLayer = [];
    this.wazeService.setSelectedPothole(selectedLayer);
    layerView.queryGraphics().then((results) => {
      const highlightSelectedLayer = [];
      if (results !== null && geometry.extent !== null) {
        results.items.forEach((element) => {
          if (geometry.extent.intersects(element.geometry)) {
            highlightSelectedLayer.push(layerView.highlight(element));
            selectedLayer.push(element.attributes);
          }
        });
      }
      if (selectedLayer.length > 0) {
        this.wazeHighlightSelectedPothole = highlightSelectedLayer;
        this.wazeService.setSelectedPothole(selectedLayer);
      }
    });
  }

  clearGeometry() {
    UtilsMap.clearHiglighting(this.highlight);
    UtilsMap.clearHiglighting(this.wazeHighlightSelectedAggregateJam);
    UtilsMap.clearHiglighting(this.wazeHighlightSelectedCrash);
    UtilsMap.clearHiglighting(this.wazeHighlightSelectedFlood);
    UtilsMap.clearHiglighting(this.wazeHighlightSelectedPothole);
    UtilsMap.clearHiglighting(this.wazeHighlightSelectedJam);
    this.isShowDisplayQuery = false;
    this.sketchGeometry = null;
    this.sketchViewModel.cancel();
    this.sketchLayer.removeAll();
  }

  async createSketchView() {
    this.sketchLayer = new GraphicsLayer();
    this.mapView.map.addMany([this.sketchLayer]);

    this.sketchGeometry = null;
    this.sketchViewModel = new SketchViewModel({
      layer: this.sketchLayer,
      defaultUpdateOptions: {
        tool: 'reshape',
      },
      view: this.mapView,
    });

    this.sketchViewModel.on('create', async (event) => {
      this.maps.reorder(this.sketchLayer, 1);

      if (event.state === 'complete') {
        this.isShowDisplayQuery = false;
        this.sketchGeometry = event.graphic.geometry;
        if (!this.sketchGeometry) {
          return;
        } else {
          this.isShowDisplayQuery = true;
          await this.createGrid();
          this.selectFeatures(event.graphic.geometry);
        }
      }
    });
  }

  async createTerritoryJabar() {
    const polygon = new Polygon({
      rings: this.localGeojson.getJabarPolygon(),
    });

    const fillSymbol = new SimpleFillSymbol({
      color: [255, 128, 0, 0],
      outline: {
        color: '#e47946',
        width: 1,
      },
    });

    const graphicJabar = new Graphic({
      geometry: polygon,
      symbol: fillSymbol,
    });

    const layerRenderer = new SimpleRenderer({
      symbol: fillSymbol,
    });

    const layer = new FeatureLayer({
      source: [graphicJabar],
      objectIdField: 'ObjectID',
      geometryType: 'polygon',
      renderer: layerRenderer,
    });
    this.maps.add(layer);

    setTimeout(() => {
      layer.when(() => {
        this.mapView.goTo(layer.fullExtent);
      });
    }, 2000);
  }

  houseKeeping(mapView) {
    this.createSketchView();

    this.mapView.on('pointer-move', (evt) => {
      this.showCoordinates(this.mapView.toMap({ x: evt.x, y: evt.y }));
    });

    this.mapView.when(() => {
      this.wazeService.setMapView(this.mapView);
      this.mapView.watch('stationary', (isStationary) => {
        this.params.center = [this.mapView.center.longitude, this.mapView.center.latitude];
        this.params.zoom = this.mapView.zoom;
        this.showCoordinates(this.mapView.center);
      });
      this.minimapView.when(() => {
        UtilsMap.setupMiniMap(this.mapView, this.minimapView);
      });
      this.initBasemapList();
    });

    for (const key in this.featureInfo) {
      if (this.featureInfo.hasOwnProperty(key)) {
        const layer = this.createdLayer.layer[key];
        this.mapView.whenLayerView(layer).then((layerView) => {
          layerView.filter = {
            where: this.featureInfo[key].condition,
          };
        });
      }
    }

    this.mapView.watch('updating', (isUpdate) => {
      if (isUpdate) {
        this.loadingBar.start();
      } else {
        this.loadingBar.stop();
        setTimeout(() => {
          this.loaderSatupetaService.setIsLoading(false);
        }, 2000);
      }
    });
  }

  setPopUp() {
    this.setPopUpActions();
    this.setPopUpFields();
  }

  setPopUpActions() {
    this.mapView.popup.actions = null;
    this.mapView.popup.dockEnabled = true;
    this.mapView.popup.dockOptions = {
      buttonEnabled: false,
      breakpoint: false,
    };
    this.mapView.popup.on('trigger-action', (event) => {
      if (event.action.id === 'zoom-in') {
        this.zoomIn();
      }
    });
  }

  setPopUpFields() {
    this.mapView.popup.watch('selectedFeature', (graphic) => {
      const excludes = ['esriFieldTypeOID', 'esriFieldTypeGeometry'];
      let fieldInfos = [];

      if (graphic) {
        console.log(graphic);
        const graphicTemplate = graphic.getEffectivePopupTemplate();

        if (graphicTemplate.content[0] !== undefined) {
          fieldInfos = graphicTemplate.content[0].fieldInfos;
          fieldInfos = _.remove(fieldInfos, (element) => {
            return !excludes.includes(element.type);
          });

          graphicTemplate.content[0].fieldInfos = fieldInfos;
        }
      }
    });
  }

  zoomIn() {
    this.mapView.goTo({
      center: this.mapView.center,
      zoom: this.mapView.zoom + 2,
    });
  }

  async generateBaseFeature(url, uniqueId, title, opacity) {
    const fieldInfos = [];
    let token = this.cookieService.get('arcgis-token');

    IdentityManager.registerToken({
      server: 'https://arcgis.jabarprov.go.id/arcgis/rest/services',
      token,
    });

    Request(url, {
      query: {
        token,
      },
      responseType: 'json',
    }).then((feature) => {
      console.log(feature);
      Object.keys(feature.data.fields).forEach((index) => {
        fieldInfos.push({
          fieldName: feature.data.fields[index].name,
          label: feature.data.fields[index].alias,
          type: feature.data.fields[index].type,
        });
      });

      console.log(fieldInfos);
      const popupTemplates = {
        type: 'PopupTemplate',
        title,
        content: [
          {
            type: 'fields',
            fieldInfos,
          },
        ],
      };
      // tslint:disable-next-line: no-shadowed-variable
      const layer = new FeatureLayer({
        url,
        popupTemplate: popupTemplates,
      });
      this.maps.add(layer);
      layer.when(() => {
        UtilsLayer.gotoLayer(this.mapView, layer);
        UtilsLayer.setLayerView(this.mapView, layer, this.createdLayer.view[uniqueId]);
      });
      this.createdLayer.layer[uniqueId] = layer;
      this.createdLayer.key.push(uniqueId);
      this.createLegend(uniqueId, layer);
      this.mapView
        .when(() => {
          return layer.when(() => {
            return layer.queryFeatures();
          });
        })
        .then((res) => {
          const valAttributes = [];
          fieldInfos.forEach((data) => {
            const valAttr = UtilsLayer.getAttrValues(res, data.fieldName);
            valAttributes.push({
              fieldName: data.fieldName,
              uniqueValue: UtilsLayer.getAttrUniqueValues(valAttr),
            });
          });
          if (fieldInfos.length === valAttributes.length) {
            // set data source filter
            this.featureInfo[uniqueId] = {
              id: uniqueId,
              featureName: title,
              attributes: fieldInfos,
              valAttributes,
              layerType: 'feature',
            };
          }
        });
      this.filterFeatureMapService.setDefaultSource(this.featureInfo);
      layer.opacity = opacity;
      UtilsLayer.fadeVisibilityOn(layer, this.mapView);
    });
  }

  async generateBaseOGCFeatureLayer(id, url = '', title = '') {
    const layer = UtilsLayer.createOGCLayer(url);
    const fieldInfos = [];
    const valAttributes = [];

    const featureInfo = this.featureInfo;

    this.createdLayer.layer[id] = layer;
    this.createdLayer.key.push(id);
    this.maps.add(layer);

    layer.when(() => {
      this.mapView.goTo(layer.fullExtent.extent).catch((error) => {
        console.error(error);
      });

      layer.fields.forEach((field) => {
        fieldInfos.push({
          fieldName: field.name,
          label: field.alias,
          type: field.type,
        });
      });

      layer.popupTemplate = new PopupTemplate({
        title,
        content: [
          {
            type: 'fields',
            fieldInfos,
          },
        ],
      });
    });
    this.mapView.whenLayerView(layer).then((layerView) => {
      layerView.watch('updating', (value) => {
        if (!value) {
          this.createdLayer.view[id] = layerView;
          layerView
            .queryFeatures({
              outFields: ['*'],
            })
            .then((results) => {
              fieldInfos.forEach((data) => {
                const valAttr = UtilsLayer.getAttrValues(results, data.fieldName);
                valAttributes.push({
                  fieldName: data.fieldName,
                  uniqueValue: UtilsLayer.getAttrUniqueValues(valAttr),
                });
              });
              if (fieldInfos.length === valAttributes.length) {
                // set data source filter
                featureInfo[id] = {
                  id,
                  featureName: title,
                  attributes: fieldInfos,
                  valAttributes,
                  layerType: 'ogc',
                };
              }
            });
        }
      });

      this.createLegend(id, layer);
      UtilsLayer.fadeVisibilityOn(layer, this.mapView);
    });

    this.filterFeatureMapService.setDefaultSource(this.featureInfo);
  }

  async generateBaseCitarum(id, title = '', url = '') {
    const fieldInfos = [];
    const valAttributes = [];
    const featureInfo = this.featureInfo;
    const programCreateLayers = {
      390: 'createLayerWaterLevelDevice',
      391: 'createLayerOnlimo',
    };

    this.citarumService.getItem(url).subscribe((result) => {
      let layer = null;

      layer = UtilsProgram[programCreateLayers[id]](result.data);

      this.createdLayer.layer[id] = layer;
      this.createdLayer.key.push(id);
      this.maps.add(layer);

      layer.when(() => {
        this.mapView.goTo(layer.fullExtent.extent).catch((error) => {
          console.error(error);
        });

        layer.fields.forEach((field) => {
          fieldInfos.push({
            fieldName: field.name,
            label: field.alias,
            type: field.type,
          });
        });
      });
      this.mapView.whenLayerView(layer).then((layerView) => {
        layerView.watch('updating', (value) => {
          if (!value) {
            this.createdLayer.view[id] = layerView;
            layerView
              .queryFeatures({
                outFields: ['*'],
              })
              .then((results) => {
                fieldInfos.forEach((data) => {
                  const valAttr = UtilsLayer.getAttrValues(results, data.fieldName);
                  valAttributes.push({
                    fieldName: data.fieldName,
                    uniqueValue: UtilsLayer.getAttrUniqueValues(valAttr),
                  });
                });
                if (fieldInfos.length === valAttributes.length) {
                  // set data source filter
                  featureInfo[id] = {
                    id,
                    featureName: title,
                    attributes: fieldInfos,
                    valAttributes,
                    layerType: 'ogc',
                  };
                }
              });
          }
        });

        this.createLegend(id, layer);
        UtilsLayer.fadeVisibilityOn(layer, this.mapView);
      });

      this.filterFeatureMapService.setDefaultSource(this.featureInfo);
    });
  }

  createLegend(uniqueId, layer) {
    return new Legend({
      view: this.mapView,
      container: 'legend' + uniqueId,
      layerInfos: [
        {
          layer,
          title: '',
          sublayerIds: [],
        },
      ],
      respectLayerVisibility: false,
    });
  }

  receiveOptions($event) {
    if ($event.length >= 1) {
      $event.forEach((data) => {
        if (this.createdLayer.layer[data.id] === undefined) {
          this.generateLayer(data);
        }
      });
    }
    if (this.createdLayer.key !== undefined) {
      this.createdLayer.key.forEach((id) => {
        const isExistLayer = $event.findIndex((x) => x.id === id);

        if (isExistLayer < 0) {
          if (this.highlight.hasOwnProperty(id)) {
            this.highlight[id].remove();
          }
          this.maps.remove(this.createdLayer.layer[id]);

          delete this.featureInfo[id];
          delete this.createdLayer.layer[id];

          this.createdLayer.view.splice(id, 1);

          const index = this.createdLayer.key.indexOf(id, 0);
          if (index > -1) {
            this.createdLayer.key.splice(index, 1);
          }

          this.selectFeatures(this.sketchGeometry);
        }
      });
    }
  }

  generateLayer(data) {
    if (data.mapset_source.id === 2) {
      const url = data.app_link;
      const title = data.name;
      let opacity = 1;

      if (data.mapset_type.name === 'Polygon') {
        opacity = 0.6;
      }

      this.generateBaseFeature(url, data.id, title, opacity);
    }
    if (data.mapset_source.id === 3) {
      const id = data.id;
      const url = data.app_link;
      const title = data.name;
      this.generateBaseOGCFeatureLayer(id, url, title);
    }

    if (data.mapset_source.id === 9) {
      this.generateBaseCitarum(data.id, data.name, data.app_link);
    }
  }

  generateWazeLayer() {}

  removeAll() {
    this.clearGeometry();
    setTimeout(() => {
      if (this.createdLayer.key !== undefined && this.createdLayer.key !== null) {
        this.createdLayer.key.forEach((id) => {
          this.maps.remove(this.createdLayer.layer[id]);
          delete this.featureInfo[id];
          delete this.createdLayer.layer[id];
          this.createdLayer.view.splice(id, 1);
        });
      }

      this.exploreMapsetService.setChoosedMapset([]);
    }, 500);
  }

  receiveGeometryQuery($event) {
    this.geometryButtonsClickHandler($event);
  }

  geometryButtonsClickHandler(geometryType) {
    this.clearGeometry();
    this.sketchViewModel.create(geometryType);
  }

  closeNav() {
    const el = this.detailSelectedMapsetEl.nativeElement;
    el.style.height = '1px';
    el.style.overflow = 'hidden';
    this.controlDetailSelectedLayer.isOpen = false;
  }

  openNav() {
    const el = this.detailSelectedMapsetEl.nativeElement;
    el.style.height = '300px';
    this.controlDetailSelectedLayer.isOpen = true;
  }

  goToPage(page) {
    this.router.navigate(['/' + page]);
  }

  showCoordinates(pt) {
    if (pt) {
      const coords = 'Lat: ' + pt.latitude.toFixed(3) + ' Lng: ' + pt.longitude.toFixed(3);
      this.tooltipEl.nativeElement.innerHTML = coords;
    }
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e) {
    if (e !== undefined) {
      const x = e.clientX;
      const y = e.clientY;

      this.tooltipEl.nativeElement.style.top = y + 20 + 'px';
      this.tooltipEl.nativeElement.style.left = x + 20 + 'px';
    }
  }

  toggleShowCoordinates(event) {
    this.isShowCoordinates = event;

    if (!this.isShowCoordinates) {
      this.tooltipEl.nativeElement.style.display = 'none';
    } else {
      this.tooltipEl.nativeElement.style.display = 'block';
    }
  }

  toggleOpenModalFeedback() {
    UtilsGeneral.openModal(this.modalReference, this.modalService, this.feedbackComponent, 'feedback');
  }
}
