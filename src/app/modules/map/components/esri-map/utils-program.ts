import GeoJSONLayer from '@arcgis/core/layers/GeoJSONLayer';
import PictureMarkerSymbol from '@arcgis/core/symbols/PictureMarkerSymbol';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';

export default class UtilsProgram {
  static createLayerWaterLevelDevice(data) {
    const features = [];

    data.forEach((element) => {
      features.push({
        geometry: {
          type: 'Point',
          coordinates: [element.device.lon, element.device.lat],
        },
        properties: {
          id: element.device.device_id,
          name: element.device.name,
          lon: element.device.lon,
          lat: element.device.lat,
          current: element.current,
          alert: element.alert,
          critical: element.critical,
          max: element.max,
          date_source: element.date_source,
          created_at: element.created_at,
        },
      });
    });

    const geojson = {
      type: 'FeatureCollection',
      features,
    };

    const blob = new Blob([JSON.stringify(geojson)], {
      type: 'application/json',
    });

    const fieldInfos = [
      {
        fieldName: 'id',
        label: 'ID',
        type: 'String',
      },
      {
        fieldName: 'name',
        label: 'Nama',
        type: 'String',
      },
      {
        fieldName: 'lon',
        label: 'Longitude',
        type: 'String',
      },
      {
        fieldName: 'lat',
        label: 'Latitude',
        type: 'String',
      },
      {
        fieldName: 'current',
        label: 'Ketinggian Air',
        type: 'String',
      },
      {
        fieldName: 'alert',
        label: 'Batas Alert',
        type: 'String',
      },
      {
        fieldName: 'critical',
        label: 'Batas Critical',
        type: 'String',
      },
      {
        fieldName: 'max',
        label: 'Batas Alarm',
        type: 'String',
      },
      {
        fieldName: 'date_source',
        label: 'Tanggal',
        type: 'String',
      },
    ];

    const popupTemplate = {
      title: 'Titik Sensor Ketinggian Air',
      content: [
        {
          type: 'fields',
          fieldInfos,
        },
      ],
    };

    const renderer = new SimpleRenderer({
      label: 'Titik Sensor Ketinggian Air',
      symbol: new PictureMarkerSymbol({
        url: 'assets/img/icon-ketinggian-air.svg',
        width: 20,
        height: 20
      }),
    });

    const layer = new GeoJSONLayer({
      url: URL.createObjectURL(blob),
      copyright: 'Jabar Digital Service',
      popupTemplate,
      renderer,
      outFields: ['*'],
    });

    return layer;
  }

  static createLayerOnlimo(data) {
    const features = [];

    data.forEach((element) => {
      features.push({
        geometry: {
          type: 'Point',
          coordinates: [element.device.lon, element.device.lat],
        },
        properties: {
          id: element.device.device_id,
          stasiun: element.device.name,
          lokasi: element.device.stasiun,
          lon: element.device.lon,
          lat: element.device.lat,
          date_source: element.date_source,
          amonia: element.amonia,
          bod: element.bod,
          cod: element.cod,
          dhl: element.dhl,
          do: element.do,
          kedalaman: element.kedalaman,
          nistrat: element.nistrat,
          nitrit: element.nitrit,
          orp: element.orp,
          ph: element.ph,
          salinitas: element.salinitas,
          suhu: element.suhu,
          swsg: element.swsg,
        },
      });
    });

    const geojson = {
      type: 'FeatureCollection',
      features,
    };

    const blob = new Blob([JSON.stringify(geojson)], {
      type: 'application/json',
    });

    const fieldInfos = [
      {
        fieldName: 'id',
        label: 'ID',
        type: 'String',
      },
      {
        fieldName: 'stasiun',
        label: 'Stasiun',
        type: 'String',
      },
      {
        fieldName: 'lokasi',
        label: 'Lokasi',
        type: 'String',
      },
      {
        fieldName: 'lon',
        label: 'Longitude',
        type: 'String',
      },
      {
        fieldName: 'lat',
        label: 'Latitude',
        type: 'String',
      },
      {
        fieldName: 'amonia',
        label: 'Amonia',
        type: 'String',
      },
      {
        fieldName: 'bod',
        label: 'BOD',
        type: 'String',
      },
      {
        fieldName: 'cod',
        label: 'COD',
        type: 'String',
      },
      {
        fieldName: 'dhl',
        label: 'DHL',
        type: 'String',
      },
      {
        fieldName: 'do',
        label: 'DO',
        type: 'String',
      },
      {
        fieldName: 'kedalaman',
        label: 'Kedalaman',
        type: 'String',
      },
      {
        fieldName: 'nistrat',
        label: 'Nistrat',
        type: 'String',
      },
      {
        fieldName: 'nitrit',
        label: 'Nitrit',
        type: 'String',
      },
      {
        fieldName: 'ph',
        label: 'PH',
        type: 'String',
      },
      {
        fieldName: 'salinitas',
        label: 'Salinitas',
        type: 'String',
      },
      {
        fieldName: 'suhu',
        label: 'Suhu',
        type: 'String',
      },
      {
        fieldName: 'swsg',
        label: 'SWSG',
        type: 'String',
      },
      {
        fieldName: 'date_source',
        label: 'Tanggal',
        type: 'String',
      },
    ];

    const popupTemplate = {
      title: 'Sebaran Stasiun Online Monitoring (Onlimo)',
      content: [
        {
          type: 'fields',
          fieldInfos,
        },
      ],
    };

    const renderer = new SimpleRenderer({
      label: 'Sebaran Stasiun Online Monitoring (Onlimo)',
      symbol: new PictureMarkerSymbol({
        url: 'assets/img/icon-onlimo.svg',
        width: 20,
        height: 20
      }),
    });

    const layer = new GeoJSONLayer({
      url: URL.createObjectURL(blob),
      copyright: 'Jabar Digital Service',
      popupTemplate,
      renderer,
      outFields: ['*'],
    });

    return layer;
  }
}
