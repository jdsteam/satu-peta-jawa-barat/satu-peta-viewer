import { Component, OnInit, Output, Input, EventEmitter, OnDestroy } from '@angular/core';
import { faFilter, faBalanceScale, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FilterFeatureMapService } from '../../services/filter-feature-map.service';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-filtercard',
  templateUrl: './filtercard.component.html',
  styleUrls: ['./filtercard.component.scss'],
})
export class FiltercardComponent implements OnInit, OnDestroy {
  isActive = true;

  // font awesome
  faFilter = faFilter;
  faBalanceScale = faBalanceScale;
  faPlus = faPlus;
  faTrash = faTrash;
  // end fa

  // Filters
  typeAttr: any;
  optAttr: any;
  optSource: any;
  optValue: any;
  settingSourceOptions: {};
  // end

  // service
  filterFeatureMapService: any;
  serviceSource: any;

  // choosed filter
  dataSource: any;
  attribute: any;
  fieldName: any;
  attributeType: any;

  // option Value attribute

  // sliderControl
  minValue = 50;
  maxValue = 200;
  options: Options = {
    floor: 0,
    ceil: 250,
  };

  @Input() unikId: string;
  @Input() mapsetId: string;
  @Output() clickRemove = new EventEmitter<any>();

  constructor(filterFeatureMapService: FilterFeatureMapService) {
    this.filterFeatureMapService = filterFeatureMapService;
  }

  ngOnInit(): void {
    this.optSource = [];

    this.setSourceFilter();

    this.optAttr = [
      {
        id: '',
        text: '-Pilih atribut data-',
      },
    ];

    this.settingSourceOptions = {
      multiple: true,
      dropdownParent: '',
    };

    this.setDataSource(this.mapsetId);
  }

  ngOnDestroy() {
    this.isActive = false;
  }

  setSourceFilter() {
    this.serviceSource = this.filterFeatureMapService.getDefaultSource();
    console.log(this.serviceSource);

    this.optSource.push({
      id: '',
      text: '-Pilih Data Source-',
    });
    for (const propertyName of this.serviceSource) {
      if (this.serviceSource[propertyName] !== undefined) {
        this.optSource.push({
          id: this.serviceSource[propertyName].id,
          text: this.serviceSource[propertyName].featureName,
        });
      }
    }
  }

  setDataSource(value) {
    const id = value;
    this.dataSource = value;

    const attributes = this.serviceSource[id].attributes;
    const optAttr = [];

    attributes.forEach((data) => {
      optAttr.push({
        id: data.fieldName,
        text: data.label,
      });
    });

    this.optAttr = optAttr;
  }

  setAttribute(event) {
    const fieldName = event.id;
    const id = this.dataSource;

    const valAttribute = this.serviceSource[id].valAttributes.find(
      (x) => x.fieldName === fieldName
    ).uniqueValue;

    this.attributeType = this.serviceSource[id].attributes.find((x) => x.fieldName === fieldName).type;

    if (
      this.attributeType === 'esriFieldTypeInteger' ||
      this.attributeType === 'esriFieldTypeDouble' ||
      this.attributeType === 'double'
    ) {
      const maxValue = Math.max.apply(null, valAttribute);
      const minValue = Math.min.apply(null, valAttribute);
      this.options.floor = minValue;
      this.options.ceil = maxValue;

      this.minValue = minValue;
      this.maxValue = maxValue;
    }

    this.fieldName = fieldName;
    this.optValue = valAttribute;
  }

  setValueFilter(event) {
    if (this.attributeType === 'esriFieldTypeString' || this.attributeType === 'string') {
      this.setValueFilterString(event);
    } else if (
      this.attributeType === 'esriFieldTypeDouble' ||
      this.attributeType === 'esriFieldTypeInteger' ||
      this.attributeType === 'double'
    ) {
      this.setValueFilterNumber();
    }
  }

  setValueFilterString(event) {
    const valAttr = [];
    event.forEach((data) => {
      valAttr.push(data);
    });
    const filter = {
      id: this.unikId,
      sourceId: this.dataSource,
      fieldName: this.fieldName,
      valAttributes: valAttr,
      attributeType: this.attributeType,
    };
    this.filterFeatureMapService.setFilterData(filter);
  }

  setValueFilterNumber() {
    const valAttr = {
      minValue: this.minValue,
      maxValue: this.maxValue,
    };

    const filter = {
      id: this.unikId,
      sourceId: this.dataSource,
      fieldName: this.fieldName,
      valAttributes: valAttr,
      attributeType: this.attributeType,
    };
    this.filterFeatureMapService.setFilterData(filter);
  }

  deleteComponent(unikId) {
    const valAttr = [];
    const filter = {
      id: unikId,
    };

    // console.log(unikId)

    this.filterFeatureMapService.setFilterData(filter);
    this.clickRemove.emit(unikId);
  }

  checkAllowedType() {
    switch (this.attributeType) {
      case 'esriFieldTypeString':
        return true;
      case 'esriFieldTypeInteger':
        return true;
      case 'esriFieldTypeDouble':
        return true;
      case 'string':
        return true;
      case 'double':
        return true;
      default:
        return false;
    }
  }
}
