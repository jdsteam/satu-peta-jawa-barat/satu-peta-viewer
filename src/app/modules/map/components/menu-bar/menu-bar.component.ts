import { Component, OnInit, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import {
  faDownload,
  faMap,
  faInfoCircle,
  faQuestionCircle,
  faWrench,
  faUser,
  faChevronRight,
  faSearch,
} from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { UtilLayerService } from '../../services/util-layer.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import LocatorSearchSource from '@arcgis/core/widgets/Search/LocatorSearchSource';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss'],
})
export class MenuBarComponent implements OnInit {
  @ViewChild('failSwal') private failSwal: SwalComponent;
  @ViewChild('successSwal') private successSwal: SwalComponent;
  @Output() is3DMap = new EventEmitter<any>();
  @Output() showCoordinates = new EventEmitter<any>();
  @Output() geometryQueryBy = new EventEmitter<any>();
  @Output() clearGeometry = new EventEmitter<any>();

  EsriLocator: any;
  activeMenu = '';
  icon = {
    faDownload,
    faQuestionCircle,
    faInfoCircle,
    faMap,
    faWrench,
    faUser,
    faChevronRight,
    faSearch,
  };
  isActive3D = true;
  urlBaseMap = 'https://js.arcgis.com/4.14/esri/images/basemap/';
  isActiveBasemap = 'rbi';
  closeResult: string;
  loading = false;
  logoutSubcription: any;
  flashMessage = 'Username / password tidak sesuai';
  loginUser = {
    name: '',
    role_name: '',
    role_title: '',
  };
  isLoggedIn = false;
  isShowCoordinates = false;
  isShowQuery = false;
  listLocation = [];
  locatorTask: any;
  searchLocation = {
    address: '',
    isActive: false,
  };
  isActiveLoginButton = true;

  constructor(
    private authenticationService: AuthenticationService,
    private utilLayerService: UtilLayerService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.isLoggedIn = this.authenticationService.isLoggedIn();
    this.initEsri();
    if (this.isLoggedIn) {
      this.loginUser = JSON.parse(localStorage.getItem('satudata-user'));
    } else {
      this.authenticationService.logout();
    }
  }

  async initEsri() {
    // locatortask
    this.locatorTask = new LocatorSearchSource({
      url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer',
    });
  }

  setActiveMenu(value) {
    if (this.activeMenu === value) {
      this.activeMenu = '';
    } else {
      this.activeMenu = value;
    }
  }

  onClickedOutside(e: Event) {
    // console.log(e)
    this.activeMenu = '';
  }

  openModal(content: TemplateRef<any>) {
    this.modalService
      .open(content, {
        size: 's',
        windowClass: 'modal-login',
        ariaLabelledBy: 'modal-explorer-data',
        centered: true,
      })
      .result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      });
  }

  // logout
  logout() {
    this.loading = true;
    this.authenticationService.logout();
    window.location.reload();
  }

  // OUTPUT FUNCTION
  set3DMap(value) {
    this.is3DMap.emit(value);

    this.isActive3D = value;
  }
  // show Coordinates
  toggleShowCoordinates() {
    this.showCoordinates.emit(this.isShowCoordinates);
  }

  // query by geometry
  geometryButtonsClickHandler(event) {
    this.geometryQueryBy.emit(event);
  }

  // clear geometry
  setClearGeometry(event) {
    this.clearGeometry.emit(event);
  }

  // ====================== search location ====================
  findSuggestLocation(event) {
    const text = event.target.value;
    this.searchLocation.address = text;
    this.searchLocation.isActive = true;

    this.locatorTask
      .suggestLocations({
        text,
      })
      .then((response) => {
        this.listLocation = response;
      });
  }

  goToLocation(magicKey) {
    this.searchLocation.isActive = false;
    this.locatorTask
      .addressToLocations({
        magicKey,
      })
      .then((response) => {
        if (response.length > 0) {
          this.utilLayerService.setGotoLocation(response[0]);
        }
      });
  }
}
