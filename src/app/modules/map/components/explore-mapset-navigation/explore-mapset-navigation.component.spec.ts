import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreMapsetNavigationComponent } from './explore-mapset-navigation.component';

describe('ExploreMapsetNavigationComponent', () => {
  let component: ExploreMapsetNavigationComponent;
  let fixture: ComponentFixture<ExploreMapsetNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExploreMapsetNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreMapsetNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
