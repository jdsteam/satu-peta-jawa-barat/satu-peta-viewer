import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ExploreMapsetService, UtilLayerService } from '../../services';
import { faAngleRight, faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import _ from 'lodash-es';
import {
  selectIsLoading as selectMapsetIsLoading,
  selectError as selectMapsetError,
  selectAllMapset,
} from '@store/mapset/mapset.selectors';
import { filter } from 'rxjs/operators';

interface MapsetNode {
  id: string;
  name: string;
  children?: MapsetNode[];
}

@Component({
  selector: 'app-explore-mapset-navigation',
  templateUrl: './explore-mapset-navigation.component.html',
  styleUrls: ['./explore-mapset-navigation.component.scss'],
})
export class ExploreMapsetNavigationComponent implements OnInit, OnChanges {
  @Input() activeCategory = 'skpd';

  activeDetailMapsetId = '';
  activeTree = 0;
  inputSearch = '';
  icons = {
    faAngleRight,
    faAngleDown,
  };
  listMapset = [];
  listMapsetByCategory = [];
  listMapsetChoosed = [];
  treeControl = new NestedTreeControl<MapsetNode>((node) => node.children);
  treeDataSource = new MatTreeNestedDataSource<MapsetNode>();
  treeListMapset: MapsetNode[];

  error$: Observable<string>;
  isLoading$: Observable<boolean>;
  searchMapsetTimer: any;

  // tslint:disable-next-line: variable-name
  hasChild = (_NumberNode: number, node: MapsetNode) => !!node.children && node.children.length > 0;

  constructor(
    private store: Store<any>,
    private exploreMapsetService: ExploreMapsetService,
    private utilLayerService: UtilLayerService
  ) {}

  ngOnInit(): void {
    this.error$ = this.store.pipe(select(selectMapsetError));
    this.isLoading$ = this.store.pipe(select(selectMapsetIsLoading));
    const searchLayer = this.utilLayerService.getSearchLayer();

    this.inputSearch = searchLayer.text;
    this.initSubscribtion();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes.activeCategory.isFirstChange()) {
      this.searchMapset();
    }
  }

  initSubscribtion() {
    this.initSubscribeMapsetByCategory();
    this.initSubscribeExploreMapset();
  }

  initSubscribeMapsetByCategory() {
    this.store.pipe(select(selectAllMapset)).subscribe((result) => {
      this.listMapsetByCategory = result;
      this.setTreeMapset();
      if (this.inputSearch !== '') {
        this.searchMapset();
      }
    });
  }

  initSubscribeExploreMapset() {
    this.exploreMapsetService.choosedMapsetSubject.subscribe((data) => {
      this.listMapsetChoosed = data;
    });

    this.exploreMapsetService.activeDetailMapSubject.subscribe((mapsetId) => {
      this.activeDetailMapsetId = mapsetId;
      this.setActiveTreeByMapsetId(mapsetId);
    });
  }

  checkActiveMapset(id) {
    const mapset = _.find(this.listMapsetChoosed, (o) => {
      return o.id === id;
    });

    if (mapset !== undefined) {
      return true;
    }
    return false;
  }

  checkChildMapset(title, childMapset, index, treeListMapset) {
    if (!title.includes(this.inputSearch)) {
      const findInChild = this.searchChildMapset(childMapset);
      if (findInChild.length > 0) {
        treeListMapset[index].children = findInChild;
      } else {
        treeListMapset.splice(index, 1);
        index = index - 1;
      }
    }

    return [treeListMapset, index];
  }

  logCountMapset(id) {
    const logCountMapsetWorker = new Worker(new URL('@workers/log-count-mapset.worker', import.meta.url), { type: 'module' });
    logCountMapsetWorker.postMessage(id);
  }

  searchChildMapset(children = []) {
    let treeListMapset = JSON.parse(JSON.stringify(children));
    let i = 0;

    children.forEach((element) => {
      const checkChildMenu = this.checkChildMapset(
        element.name.toLowerCase(),
        element.children,
        i,
        treeListMapset
      );

      treeListMapset = checkChildMenu[0];
      i = checkChildMenu[1];
      i++;
    });

    return treeListMapset;
  }

  searchMapset() {
    let childListMapset = [];
    this.treeDataSource.data = this.treeListMapset;

    if (this.treeListMapset.length > 0) {
      let i = 0;
      childListMapset = JSON.parse(JSON.stringify(this.treeListMapset));
      this.treeListMapset.forEach((element) => {
        if (element.name !== undefined && element.name !== null) {
          const checkChildMenu = this.checkChildMapset(
            element.name.toLowerCase(),
            element.children,
            i,
            childListMapset
          );
          [childListMapset, i] = checkChildMenu;
        }
        i++;
      });
      this.treeDataSource.data = childListMapset;
      this.treeControl.dataNodes = childListMapset;

      if (this.inputSearch) {
        this.treeControl.expandAll();
      } else {
        this.treeControl.collapseAll();
      }
    }
  }

  setActiveDetail(id) {
    this.exploreMapsetService.setActiveDetailMap(id);
  }

  setActiveMapset(mapsetId) {
    const isChoosed = _.find(this.listMapsetChoosed, (element) => {
      return element.id === mapsetId;
    });

    const mapset = this.listMapset.find((element) => {
      return element.id === mapsetId;
    });

    if (isChoosed === undefined) {
      const choosedMapset = this.setChoosedMapset(mapset);
      this.listMapsetChoosed.unshift(choosedMapset);
      this.logCountMapset(mapsetId);
    } else {
      this.listMapsetChoosed = this.listMapsetChoosed.filter((obj) => {
        return obj.id !== mapsetId;
      });
      this.exploreMapsetService.setActiveDetailMap(null);
    }

    this.exploreMapsetService.setChoosedMapset(this.listMapsetChoosed);
    this.exploreMapsetService.setCloseModal(true);
  }

  setActiveTree(id) {
    this.activeTree = id;
  }

  setActiveTreeByMapsetId(mapsetId) {
    const mapset = this.listMapset.find((element) => {
      return element.id === mapsetId;
    });

    if (mapset !== undefined) {
      switch (this.activeCategory) {
        case 'sektoral':
          this.activeTree = mapset.sektoral.id;
          break;
        case 'skpd':
          this.activeTree = mapset.skpd.kode_skpd;
          break;
        case 'regional':
          this.activeTree = mapset.regional.kode_bps;
          break;
        case 'program':
          this.activeTree = mapset.mapset_program.id;
          break;
        default:
          this.activeTree = mapset.sektoral.id;
          break;
      }
    }
  }

  setChoosedMapset(mapset) {
    let choosedMapset = {};

    if (mapset.app_service?.is_program) {
      choosedMapset = {
        id: mapset.id,
        name: mapset.name,
        activeCategory: this.activeCategory,
        app_link: mapset.app_link,
        app_id: mapset.app_service.app_id,
        app_url: mapset.app_service.app_url,
        mapset_type: mapset.mapset_type,
      };
    } else {
      choosedMapset = {
        id: mapset.id,
        name: mapset.name,
        activeCategory: this.activeCategory,
        mapset_source: mapset.mapset_source,
        app_link: mapset.app_link,
        app_id: 'arcgis',
        mapset_type: mapset.mapset_type,
      };
    }

    return choosedMapset;
  }

  setTreeMapset() {
    this.listMapset = [];
    switch (this.activeCategory) {
      case 'skpd':
        this.setTreeMapsetSkpd();
        break;
      case 'sektoral':
        this.setTreeMapsetSektoral();
        break;
      case 'regional':
        this.setTreeMapsetRegion();
        break;
      case 'program':
        this.setTreeMapsetProgram();
        break;
      default:
        this.setTreeMapsetSkpd();
        break;
    }
  }

  setTreeMapsetSkpd() {
    const dataListMapset = [];
    this.listMapsetByCategory.forEach((elParent) => {
      const excludes = ['3.07.01'];

      if (!excludes.includes(elParent.kode_skpd)) {
        const listMapset = this.setListMapset(elParent);

        dataListMapset.push({
          name: elParent.nama_skpd,
          id: elParent.kode_skpd,
          children: listMapset,
        });
      }
    });

    this.treeListMapset = dataListMapset;
    this.treeDataSource.data = dataListMapset;
  }

  setTreeMapsetSektoral() {
    const dataListMapset = [];
    this.listMapsetByCategory.forEach((elParent) => {
      const listMapset = this.setListMapset(elParent);

      dataListMapset.push({
        name: elParent.name,
        id: elParent.id,
        children: listMapset,
      });
    });
    this.treeListMapset = dataListMapset;
    this.treeDataSource.data = dataListMapset;
  }

  setTreeMapsetRegion() {
    const dataListMapset = [];
    this.listMapsetByCategory.forEach((elParent) => {
      const listMapset = this.setListMapset(elParent);
      dataListMapset.push({
        name: elParent.nama_kemendagri,
        id: elParent.kode_bps,
        children: listMapset,
      });
    });

    this.treeListMapset = dataListMapset;
    this.treeDataSource.data = dataListMapset;
  }

  setTreeMapsetProgram() {
    const dataListMapset = [];
    this.listMapsetByCategory.forEach((elParent) => {
      const listMapset = this.setListMapset(elParent);
      dataListMapset.push({
        name: elParent.mapset_program_name,
        id: elParent.mapset_program_id,
        children: listMapset,
      });
    });
    this.treeListMapset = dataListMapset;
    this.treeDataSource.data = dataListMapset;
  }

  setListMapset(elParent) {
    let listMapset = [];
    if (elParent.mapset) {
      elParent.mapset.forEach((elMapset) => {
        if (elMapset.is_active) {
          this.listMapset.push(elMapset);
          listMapset.push({
            id: elMapset.id,
            name: elMapset.name,
            isCollapse: true,
            children: [],
          });
        }
      });
    }

    listMapset = _.orderBy(listMapset, ['name'], ['asc']);

    return listMapset;
  }

  onInputSearch() {
    clearTimeout(this.searchMapsetTimer);
    this.searchMapsetTimer = setTimeout(() => {
      this.searchMapset();
    }, 300);
  }
}
