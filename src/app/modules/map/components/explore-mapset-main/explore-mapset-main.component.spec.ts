import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreMapsetMainComponent } from './explore-mapset-main.component';

describe('ExploreMapsetMainComponent', () => {
  let component: ExploreMapsetMainComponent;
  let fixture: ComponentFixture<ExploreMapsetMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExploreMapsetMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreMapsetMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
