import GeoJSONLayer from '@arcgis/core/layers/GeoJSONLayer';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';
import PictureMarkerSymbol from '@arcgis/core/symbols/PictureMarkerSymbol';

export default class UtilsProgram {
  static createLayerWaterLevelDevice(data) {
    const features = [];

    data.forEach((element) => {
      features.push({
        geometry: {
          type: 'Point',
          coordinates: [element.device.lon, element.device.lat],
        },
      });
    });

    const geojson = {
      type: 'FeatureCollection',
      features,
    };

    const blob = new Blob([JSON.stringify(geojson)], {
      type: 'application/json',
    });

    const renderer = new SimpleRenderer({
      symbol: new PictureMarkerSymbol({
        url: 'assets/img/icon-ketinggian-air.svg',
      }),
    });

    const layer = new GeoJSONLayer({
      url: URL.createObjectURL(blob),
      copyright: 'Jabar Digital Service',
      renderer,
    });

    return layer;
  }

  static createLayerOnlimo(data) {
    const features = [];

    data.forEach((element) => {
      features.push({
        geometry: {
          type: 'Point',
          coordinates: [element.device.lon, element.device.lat],
        },
        properties: {
          id: element.id,
          stasiun: element.device.name,
          lokasi: element.device.stasiun,
          date_source: element.date_source,
          amonia: element.amonia,
          bod: element.bod,
          cod: element.cod,
          dhl: element.dhl,
          do: element.do,
          kedalaman: element.kedalaman,
          nistrat: element.nistrat,
          nitrit: element.nitrit,
          orp: element.orp,
          ph: element.ph,
          salinitas: element.salinitas,
          suhu: element.suhu,
          swsg: element.swsg,
        },
      });
    });

    const geojson = {
      type: 'FeatureCollection',
      features,
    };

    const blob = new Blob([JSON.stringify(geojson)], {
      type: 'application/json',
    });

    const renderer = new SimpleRenderer({
      label: 'Sebaran Stasiun Online Monitoring (Onlimo)',
      symbol: new PictureMarkerSymbol({
        url: 'assets/img/icon-onlimo.svg',
      }),
    });

    const layer = new GeoJSONLayer({
      url: URL.createObjectURL(blob),
      copyright: 'Jabar Digital Service',
      renderer,
      outFields: ['*'],
    });

    return layer;
  }
}
