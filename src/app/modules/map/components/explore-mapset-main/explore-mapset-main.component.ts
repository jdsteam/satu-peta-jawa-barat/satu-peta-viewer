import { Component, Injector, OnInit } from '@angular/core';
import { filter, map, take } from 'rxjs/operators';
import { ExploreMapsetService } from '../../services';
import { environment } from '../../../../../environments/environment';
import FeatureLayer from '@arcgis/core/layers/FeatureLayer';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import * as watchUtils from '@arcgis/core/core/watchUtils';
import _ from 'lodash-es';
import { CitarumService } from '@services';
import UtilsProgram from './utils-program';
import { faChevronUp, faChevronDown, faCopy } from '@fortawesome/free-solid-svg-icons';
import { AuthenticationService } from '@services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-explore-mapset-main',
  templateUrl: './explore-mapset-main.component.html',
  styleUrls: ['./explore-mapset-main.component.scss'],
})
export class ExploreMapsetMainComponent implements OnInit {
  activeMapsetId = 0;
  activeDetailMapset = null;
  apiData = environment.apiData;
  listMapset = [];
  listMapsetChoosed = [];
  map: any;
  mapView: any;
  icons = {
    faChevronUp,
    faChevronDown,
    faCopy,
  };
  isCollapsedGroup = {
    infoDataset: false,
    responsiblePerson: true,
  };
  isCopyUrl = false;
  isLoggedIn = false;

  // services
  citarumService: CitarumService;
  constructor(
    private authService: AuthenticationService,
    private exploreMapsetService: ExploreMapsetService,
    private injector: Injector,
    private toastr: ToastrService
  ) {
    this.citarumService = injector.get<CitarumService>(CitarumService);
    this.isLoggedIn = this.authService.isLoggedIn();
  }

  ngOnInit(): void {
    this.initMap();
    this.initSubcribtion();
  }

  initMap() {
    this.map = new Map({
      basemap: 'topo',
    });

    this.mapView = new MapView({
      container: 'mapView',
      map: this.map,
      center: [107.60981, -6.914744],
      zoom: 7,
      ui: {
        components: ['attribution'],
      },
    });

    this.mapDisableZoom();
  }

  initSubcribtion() {
    this.exploreMapsetService.choosedMapsetSubject.subscribe((data) => {
      this.listMapsetChoosed = data;
    });

    this.exploreMapsetService.activeDetailMapSubject.subscribe((mapsetId) => {
      this.activeMapsetId = mapsetId;
      this.setActiveDetailMapset();
    });

    this.exploreMapsetService.defaultMapsetSubject.pipe(filter((val) => val.length > 0)).subscribe((data) => {
      this.listMapset = data;
      this.setActiveDetailMapset();
    });
  }

  activateMapset() {
    const isChoosed = _.find(this.listMapsetChoosed, (element) => {
      return element.id === this.activeMapsetId;
    });

    const mapset = this.listMapset.find((element) => {
      return element.id === this.activeMapsetId;
    });

    if (isChoosed === undefined) {
      const choosedMapset = this.setChoosedMapset(mapset);
      this.listMapsetChoosed.unshift(choosedMapset);
      this.logCountMapset(this.activeMapsetId);
    } else {
      this.listMapsetChoosed = this.listMapsetChoosed.filter((obj) => {
        return obj.id !== this.activeMapsetId;
      });
      this.exploreMapsetService.setActiveDetailMap(null);
    }

    this.exploreMapsetService.setChoosedMapset(this.listMapsetChoosed);
    this.exploreMapsetService.setCloseModal(true);
  }

  checkActiveMapset() {
    const mapset = _.find(this.listMapsetChoosed, (o) => {
      return o.id === this.activeMapsetId;
    });

    if (mapset !== undefined) {
      return true;
    }
    return false;
  }

  copyMapURL(text) {
    navigator.clipboard.writeText(text);
    this.toastr.success('Tautan berhasil disalin!');
  }

  getMapsetClassColor(className) {
    switch (className) {
      case 'Publik':
        return 'class-color-public';
      case 'Internal':
        return 'class-color-internal';
      default:
        return 'class-color-private';
    }
  }

  gotoDataURL(url) {
    return fetch(url)
      .then((response) => {
        return response.blob();
      })
      .then((blob) => {
        return URL.createObjectURL(blob);
      });
  }

  logCountMapset(id) {
    const logCountMapsetWorker = new Worker(new URL('@workers/log-count-mapset.worker', import.meta.url), { type: 'module' });
    logCountMapsetWorker.postMessage(id);
  }

  mapAddLayer(url) {
    if (url !== undefined) {
      const layer = new FeatureLayer({
        url,
      });

      this.map.add(layer);
      this.mapFadeVisibility(layer);
    }
  }

  mapDisableZoom() {
    // disable +/- gestures
    this.mapView.on('key-down', (event) => {
      const prohibitedKeys = ['+', '-', 'Shift', '_', '='];
      const keyPressed = event.key;
      if (prohibitedKeys.indexOf(keyPressed) !== -1) {
        event.stopPropagation();
      }
    });

    // disable scroll
    this.mapView.on('mouse-wheel', (event) => {
      event.stopPropagation();
    });

    // disable double click
    this.mapView.on('double-click', (event) => {
      event.stopPropagation();
    });

    // disable drag
    this.mapView.on('drag', (event) => {
      event.stopPropagation();
    });
  }

  mapFadeVisibility(layer) {
    let animating = true;
    let opacity = 0;
    const finalOpacity = layer.opacity;
    layer.opacity = opacity;
    layer.when(() => {
      this.mapView.whenLayerView(layer).then((layerView) => {
        function incrementOpacityByFrame() {
          if (opacity >= finalOpacity && animating) {
            animating = false;
            return;
          }

          layer.opacity = opacity;
          opacity += 0.05;

          requestAnimationFrame(incrementOpacityByFrame);
        }

        watchUtils.whenFalseOnce(layerView, 'updating', (updating) => {
          requestAnimationFrame(incrementOpacityByFrame);
        });
      });
    });
  }

  setActiveDetailMapset() {
    const mapset = this.listMapset.find((element) => {
      return element.id === this.activeMapsetId;
    });

    if (mapset === undefined) {
      this.activeDetailMapset = null;
    } else {
      this.activeDetailMapset = mapset;
    }

    if (this.activeDetailMapset?.mapset_source.id === 9) {
      this.map.removeAll();
      this.getProgram();
    } else {
      this.map.removeAll();
      this.mapAddLayer(mapset?.app_link);
    }
  }

  setChoosedMapset(mapset) {
    const choosedMapset = {
      id: mapset.id,
      name: mapset.name,
      mapset_source: mapset.mapset_source,
      app_link: mapset.app_link,
      mapset_type: mapset.mapset_type,
    };

    return choosedMapset;
  }

  getProgram() {
    this.getProgramCitarum();
  }

  async getProgramCitarum() {
    const url = this.activeDetailMapset.app_link;
    const programCreateLayers = {
      390: 'createLayerWaterLevelDevice',
      391: 'createLayerOnlimo',
    };

    this.citarumService.getItem(url).subscribe((result) => {
      let layer = null;
      layer = UtilsProgram[programCreateLayers[this.activeMapsetId]](result.data);
      this.map.add(layer);
      this.mapFadeVisibility(layer);

      layer.when(() => {
        this.mapView.goTo(layer.fullExtent);
      });
    });
  }

  async downloadFile(url) {
    const a = document.createElement('a');
    a.href = await this.gotoDataURL(url);
    a.download = '';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}
