import { Component } from '@angular/core';
import { LogService } from '@services';
import { UtilLayerService } from '../../services/util-layer.service';

@Component({
  selector: 'app-basemap-setting',
  templateUrl: './basemap-setting.component.html',
  styleUrls: ['./basemap-setting.component.scss'],
})
export class BasemapSettingComponent {
  isActive3D = false;
  isActiveBasemap = 'topo';
  urlBaseMap = 'https://js.arcgis.com/4.14/esri/images/basemap/';
  listBasemap = [
    {
      id: 'rbi',
      name: 'Rupa Bumi Indonesia',
      thumbnailUrl: 'assets/img/basemap-thumbnail/rbi.png',
    },
    {
      id: 'cartodb_light',
      name: 'Cartodb Light All',
      thumbnailUrl: 'assets/img/basemap-thumbnail/cartodb_light.png',
    },
    {
      id: 'cartodb_dark',
      name: 'Cartodb Dark All',
      thumbnailUrl: 'assets/img/basemap-thumbnail/cartodb_dark.png',
    },
    {
      id: 'national_geographic',
      name: 'National Geographic',
      thumbnailUrl: this.urlBaseMap + 'national-geographic.jpg',
    },
    {
      id: 'topo',
      name: 'Topographic',
      thumbnailUrl: 'assets/img/basemap-thumbnail/topo.png',
    },
    {
      id: 'dark_gray',
      name: 'Dark Gray',
      thumbnailUrl: this.urlBaseMap + 'dark-gray.jpg',
    },
    {
      id: 'gray',
      name: 'Light Gray',
      thumbnailUrl: this.urlBaseMap + 'gray.jpg',
    },
    {
      id: 'hybrid',
      name: 'Imagery',
      thumbnailUrl: this.urlBaseMap + 'hybrid.jpg',
    },
    {
      id: 'oceans',
      name: 'Oceans',
      thumbnailUrl: this.urlBaseMap + 'oceans.jpg',
    },
    {
      id: 'osm',
      name: 'Open Street Map',
      thumbnailUrl: this.urlBaseMap + 'osm.jpg',
    },
    {
      id: 'satellite',
      name: 'Satellite',
      thumbnailUrl: this.urlBaseMap + 'satellite.jpg',
    },
    {
      id: 'streets',
      name: 'Streets',
      thumbnailUrl: this.urlBaseMap + 'streets.jpg',
    },
    {
      id: 'terrain',
      name: 'Terrain',
      thumbnailUrl: this.urlBaseMap + 'terrain.jpg',
    },
  ];
  basemapName = this.listBasemap.find((x) => x.id === this.isActiveBasemap).name;

  constructor(private utilLayerService: UtilLayerService, private logService: LogService) {}

  set3DMap(value) {
    this.utilLayerService.setIs3DMap(value);
    this.isActive3D = value;
    this.isActive3D ? this.postLog('click|3d-map') : this.postLog('click|2d-map');
  }

  setBasemap(value) {
    this.postLog('click|basemap|' + value);
    this.utilLayerService.setActiveBasemap(value);
    this.isActiveBasemap = value;
    this.basemapName = this.listBasemap.find((x) => x.id === this.isActiveBasemap).name;
  }

  postLog(page) {
    const pageInfo = {
      page,
    };

    const log = this.logService.getLog();
    const token = this.logService.getToken();

    const postLog = {
      log: { ...pageInfo, ...log },
      token,
    };

    const logPageWorker = new Worker(new URL('@workers/log-page.worker', import.meta.url), { type: 'module' });
    logPageWorker.postMessage(postLog);
  }
}
