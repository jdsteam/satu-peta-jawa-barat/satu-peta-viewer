import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ExploreMapsetService } from '../../services/explore-mapset.service';
import { UtilLayerService } from '../../services/util-layer.service';
import { filter, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { AuthenticationService } from '@services';

import _ from 'lodash-es';

import { select, Store } from '@ngrx/store';

import {
  selectAllMapset,
  selectIsLoading as selectMapsetIsLoading,
  selectError as selectMapsetError,
} from '@store/mapset/mapset.selectors';
import { fromMapsetActions } from '@store/mapset/mapset.actions';
import { MapsetData } from '@models';

interface MapsetNode {
  id: string;
  name: string;
  children?: MapsetNode[];
}

@Component({
  selector: 'app-explore-mapset',
  templateUrl: './explore-mapset.component.html',
  styleUrls: ['./explore-mapset.component.scss'],
})
export class ExploreMapsetComponent implements OnInit {
  @ViewChild('content', { static: true }) content: ElementRef;
  @ViewChild('detailData') detailDataEl: ElementRef;
  @ViewChild('detailNoData') detailNoDataEl: ElementRef;
  @Output() zoomToLayer = new EventEmitter<any>();
  @Output() checkedEvent = new EventEmitter<any>();

  apiData = environment.apiData;
  isOpenModalSubscribtion: any;
  choosedMapsetSubscribtion: any;
  unsubscribe$: Subject<void> = new Subject<void>();
  isLoading$: Observable<boolean>;
  error$: Observable<string>;

  mapset = [];
  listMapset: MapsetNode[];

  activeCategoryMenu = 'skpd';
  activeDetailMap = null;
  isLoggedIn = false;

  mapsetData: MapsetData[];
  // tslint:disable-next-line: no-shadowed-variable
  // tslint:disable-next-line: variable-name
  hasChild = (_NumberNode: number, node: MapsetNode) => !!node.children && node.children.length > 0;

  constructor(
    private store: Store<any>,
    private authService: AuthenticationService,
    private exploreMapsetService: ExploreMapsetService,
    private utilLayerService: UtilLayerService
  ) {
    this.isLoggedIn = this.authService.isLoggedIn();
  }

  ngOnInit() {
    const searchLayer = this.utilLayerService.getSearchLayer();
    this.initSubscribtion();

    if (searchLayer.category !== undefined && this.activeDetailMap === null) {
      this.activeCategoryMenu = searchLayer.category;
    }

    this.getData();
  }

  loadMapsetData() {
    this.mapset = [];
    switch (this.activeCategoryMenu) {
      case 'skpd':
        this.loadMapsetSkpd();
        break;
      case 'sektoral':
        this.loadMapsetSektoral();
        break;
      case 'regional':
        this.loadMapsetRegion();
        break;
      case 'program':
        this.loadMapsetProgram();
        break;
      default:
        this.loadMapsetSkpd();
        break;
    }
  }

  loadMapsetSkpd() {
    const mapsetData = [...this.mapsetData];
    mapsetData.forEach((elParent) => {
      const excludes = ['3.07.01'];

      if (!excludes.includes(elParent.kode_skpd)) {
        this.setListMapset(elParent);
      }
    });

    this.exploreMapsetService.setDefaultMapset(this.mapset);
  }

  loadMapsetSektoral() {
    const mapsetData = [...this.mapsetData];
    mapsetData.forEach((elParent) => {
      this.setListMapset(elParent);
    });

    this.exploreMapsetService.setDefaultMapset(this.mapset);
  }

  loadMapsetRegion() {
    const mapsetData = [...this.mapsetData];
    mapsetData.forEach((elParent) => {
      this.setListMapset(elParent);
    });
    this.exploreMapsetService.setDefaultMapset(this.mapset);
  }

  loadMapsetProgram() {
    const mapsetData = [...this.mapsetData];
    mapsetData.forEach((elParent) => {
      this.setListMapset(elParent);
    });
    this.exploreMapsetService.setDefaultMapset(this.mapset);
  }

  setActiveCategoryMenu(category) {
    this.activeCategoryMenu = category;
    this.getMapset();
  }

  setListMapset(elParent) {
    if (elParent.mapset) {
      elParent.mapset.forEach((elMapset) => {
        if (elMapset.is_active) {
          this.mapset.push(elMapset);
        }
      });
    }
  }

  closeModal() {
    this.exploreMapsetService.setCloseModal(true);
  }

  initSubscribtion() {
    this.subscribeDataMapsetCity();
    this.subscribeExploreMapset();
  }

  subscribeDataMapsetCity() {
    this.store
      .pipe(
        select(selectAllMapset),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        this.mapsetData = result;
        this.loadMapsetData();
      });
  }

  subscribeExploreMapset() {
    this.exploreMapsetService.activeCategoryMenuSubject
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data) => {
        if (data !== null) {
          this.activeCategoryMenu = data;
        }
      });

    this.exploreMapsetService.activeDetailMapSubject.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      if (data !== null) {
        this.activeDetailMap = data;
      }
    });
  }

  getData() {
    this.getMapset();
  }

  getMapset() {
    const params = `?groupby=${this.activeCategoryMenu}&distinct=true&where={ 'mapset_source_id' : '2'}`;

    this.store.dispatch(
      fromMapsetActions.loadMapset({
        params,
      })
    );

    this.error$ = this.store.pipe(select(selectMapsetError));
    this.isLoading$ = this.store.pipe(select(selectMapsetIsLoading));
  }
}
