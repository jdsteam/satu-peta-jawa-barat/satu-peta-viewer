import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExploreMapsetComponent } from './explore-mapset.component';

describe('ExploreMapsetComponent', () => {
  let component: ExploreMapsetComponent;
  let fixture: ComponentFixture<ExploreMapsetComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExploreMapsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreMapsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
