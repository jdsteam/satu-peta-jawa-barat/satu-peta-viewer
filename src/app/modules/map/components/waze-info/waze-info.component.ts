import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { WazeService } from '../../services/';

@Component({
  selector: 'app-waze-info',
  templateUrl: './waze-info.component.html',
  styleUrls: ['./waze-info.component.scss'],
})
export class WazeInfoComponent implements OnInit {
  activePage = 1;
  isAgree = false;

  constructor(
    private wazeService: WazeService,
    private cookieService: CookieService,
  ) { }

  ngOnInit(): void {
    this.isAgree = this.cookieService.get('is-agree-waze-info') ? JSON.parse(this.cookieService.get('is-agree-waze-info')) : false;
  }

  closeModal() {
    this.wazeService.isCloseWazeInfo.emit(true);
  }

  setActivePage(val) {
    this.activePage = val;
  }

  toggleDone() {
    const expiredDate = new Date();
    expiredDate.setDate(expiredDate.getDate() + 7);

    this.cookieService.set('is-agree-waze-info', this.isAgree.toString(), expiredDate);

    this.closeModal();
  }
}
