import { Component, OnInit } from '@angular/core';
import { UtilLayerService } from '../../services/util-layer.service';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { LogService } from '@services';

@Component({
  selector: 'app-export-map',
  templateUrl: './export-map.component.html',
  styleUrls: ['./export-map.component.scss'],
})
export class ExportMapComponent implements OnInit {
  titleExport = '';
  formatExport = 'pdf';
  isLoading = false;
  isError = false;
  urlDownload = '';
  icon = {
    faInfoCircle,
  };

  constructor(private utilLayerService: UtilLayerService, private logService: LogService) {}

  ngOnInit() {
    this.initSubscription();
  }

  initSubscription() {
    this.utilLayerService.urlDownload.subscribe((data) => {
      this.setDownloadLink(data);
    });
  }

  async setDownloadLink(data) {
    this.isLoading = false;
    if (data.error !== 1) {
      this.urlDownload = data.url;
      this.downloadFile();
    } else {
      this.isError = data.error;
    }
  }

  toDataURL(url) {
    return fetch(url)
      .then((response) => {
        return response.blob();
      })
      .then((blob) => {
        return URL.createObjectURL(blob);
      });
  }

  setFormatExport(val) {
    this.formatExport = val;
    this.urlDownload = '';
  }

  toggleExportMap() {
    this.postLog('click|export-map');
    const printTemplate = {
      format: this.formatExport,
      exportOptions: {
        dpi: 300,
      },
      layout: 'a4-portrait',
      layoutOptions: {
        titleText: this.titleExport,
        authorText: 'JDS',
      },
    };

    this.utilLayerService.setPrintTemplate(printTemplate);
    this.isLoading = true;
    this.isError = false;
  }

  async downloadFile() {
    const a = document.createElement('a');
    a.href = await this.toDataURL(this.urlDownload);
    a.download = this.titleExport;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  onClickOutside() {
    this.urlDownload = '';
  }

  postLog(page) {
    const pageInfo = {
      page,
    };

    const log = this.logService.getLog();
    const token = this.logService.getToken();

    const postLog = {
      log: { ...pageInfo, ...log },
      token,
    };

    const logPageWorker = new Worker(new URL('@workers/log-page.worker', import.meta.url), { type: 'module' });
    logPageWorker.postMessage(postLog);
  }
}
