import { Component, OnInit, ViewChild } from '@angular/core';
import { FeedbackMapService, LogService, TokenService } from '@services';
import { FeedbackMap } from '@models';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { UtilComponentService } from '../../services/util-component.service';
import { select, Store } from '@ngrx/store';
import { selectAllIp } from '@store/ip/ip.selectors';
import { filter } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import _ from 'lodash-es';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {
  @ViewChild('failSwal') private failSwal: SwalComponent;
  @ViewChild('successSwal') private successSwal: SwalComponent;

  feedbackInfo = new FeedbackMap();
  formFeedback = {
    score: null,
    tujuan: null,
    tujuan_tercapai: true,
    tujuan_mudah_ditemukan: true,
    sektor: null,
    saran: null,
    masalah: [],
  };
  optTujuan = [];
  optSektor = [];
  isValidForm = false;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  token = null;

  constructor(
    private store: Store<any>,
    private feedbackService: FeedbackMapService,
    private tokenService: TokenService,
    private deviceService: DeviceDetectorService,
    private logService: LogService,
    private utilComponentService: UtilComponentService
  ) {}

  ngOnInit(): void {
    this.getFeedbackInfo();
    this.initLogPage();
  }

  initLogPage() {
    const device = this.deviceService.getDeviceInfo();
    _.set(device, 'isMobile', this.deviceService.isMobile());
    _.set(device, 'isTablet', this.deviceService.isTablet());
    _.set(device, 'isDesktop', this.deviceService.isDesktop());

    this.deviceInfo = this.logService.getDevice(device);

    this.subscribeIp();
  }

  setMasalah(masalah, isChecked) {
    if (isChecked) {
      this.formFeedback.masalah.push(masalah);
    } else {
      const index = this.formFeedback.masalah.findIndex((x) => x === masalah);
      this.formFeedback.masalah.splice(index, 1);
    }

    this.validateForm();
  }

  setScore(val) {
    this.formFeedback.score = val;
    this.validateForm();
  }

  setTujuanTercapai(val) {
    this.formFeedback.tujuan_tercapai = val;
    if (val) {
      this.formFeedback.masalah = [];
    }
    this.validateForm();
  }

  setTujuanMudahDitemukan(val) {
    this.formFeedback.tujuan_mudah_ditemukan = val;
    this.validateForm();
  }

  submitForm() {
    this.postFeedback();
  }

  validateForm() {
    const arrCheckNull = [this.formFeedback.score, this.formFeedback.tujuan, this.formFeedback.sektor];
    const checkIsNull = arrCheckNull.includes(null);

    if (this.formFeedback.tujuan_tercapai) {
      if (!checkIsNull) {
        this.isValidForm = true;
      } else {
        this.isValidForm = false;
      }
    } else {
      if (!checkIsNull && this.formFeedback.masalah.length > 0) {
        this.isValidForm = true;
      } else {
        this.isValidForm = false;
      }
    }
  }

  closeModal() {
    this.utilComponentService.setCloseModal(true);
  }

  subscribeIp() {
    this.store
      .pipe(
        select(selectAllIp),
        filter((val) => val.length !== 0)
      )
      .subscribe((result) => {
        const res = result[0];
        const token = this.tokenService.createToken(res.ip);

        this.token = token;
        this.log = { ...this.deviceInfo, ...res };
      });
  }

  postFeedback() {
    this.feedbackService.postFeedback(this.formFeedback).subscribe(
      (res) => {
        this.utilComponentService.setIsLoading(false);
        this.successSwal.fire().then((result) => {
          if (result.isConfirmed) {
            this.utilComponentService.setCloseModal(true);
          }
        });
      },
      (err) => {
        this.utilComponentService.setIsLoading(false);
        this.failSwal.fire();
      }
    );
  }

  getFeedbackInfo() {
    this.feedbackService.getInfo().subscribe((res) => {
      this.feedbackInfo = res.data;
    });
  }
}
