import { Component, OnDestroy, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AnimationOptions } from 'ngx-lottie';
import { MaintenancePopUpService } from '../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { NavigationService } from '../../services/navigation.service';

@Component({
  selector: 'app-maintenance-popup',
  templateUrl: './maintenance-popup.component.html',
  styleUrls: ['./maintenance-popup.component.scss'],
})
export class MaintenancePopUpComponent implements OnInit, OnDestroy {
  constructor(
    private maintenancePopUpService: MaintenancePopUpService,
    private spinner: NgxSpinnerService,
    private navigationService: NavigationService
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    setTimeout(() => { this.spinner.hide(); }, 1000);
  }

  ngOnDestroy() {
    this.navigationService.setSlideNav('open');
  }

  closeModal() {
    this.maintenancePopUpService.isCloseMaintenancePopUp.emit(true);
  }
}
