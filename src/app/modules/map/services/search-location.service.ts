import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SearchLocationService {
  findLocation = new EventEmitter();
  listLocation = new EventEmitter();
  gotoLocation = new EventEmitter();

  public setGotoLocation(data) {
    this.gotoLocation.emit(data);
  }
}
