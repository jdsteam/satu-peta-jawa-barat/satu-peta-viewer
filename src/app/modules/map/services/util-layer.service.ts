import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

interface DetailSelectedMapset {
  id: string;
  name: string;
  fields: [];
  data: [];
}

@Injectable({
  providedIn: 'root',
})
export class UtilLayerService {
  transparencyLayer = new EventEmitter();
  zoomToLayer = new EventEmitter();
  searchLayer: any;
  gotoLocation = new EventEmitter();
  reorderLayer = new EventEmitter();
  is3DMap = new EventEmitter();
  activeBasemap = new EventEmitter();
  printTemplate = new EventEmitter();
  urlDownload = new EventEmitter();
  detailSelectedMapset = new Subject<DetailSelectedMapset[]>();

  public setTransparencyLayer(data) {
    this.transparencyLayer.emit(data);
  }

  public setZoomToLayer(data) {
    this.zoomToLayer.emit(data);
  }

  public setSearchLayer(data) {
    this.searchLayer = data;
  }

  public setGotoLocation(data) {
    this.gotoLocation.emit(data);
  }

  public setReorderLayer(data) {
    this.reorderLayer.emit(data);
  }

  public setIs3DMap(data) {
    this.is3DMap.emit(data);
  }

  public setActiveBasemap(data) {
    this.activeBasemap.emit(data);
  }

  public setPrintTemplate(data) {
    this.printTemplate.emit(data);
  }

  public setURLDownload(data) {
    this.urlDownload.emit(data);
  }

  public setDetailSelectedMapset(data) {
    this.detailSelectedMapset.next(data);
  }

  public getSearchLayer() {
    return this.searchLayer;
  }
}
