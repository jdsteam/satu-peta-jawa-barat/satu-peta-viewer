import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UtilComponentService {
  isCloseModal = new EventEmitter();
  isLoading = new EventEmitter();

  setCloseModal(isClose) {
    this.isCloseModal.emit(isClose);
  }

  setIsLoading(val) {
    this.isLoading.emit(val);
  }
}
