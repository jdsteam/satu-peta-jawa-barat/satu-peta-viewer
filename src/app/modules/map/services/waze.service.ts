import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class WazeService {
  activeWazeJamType = new EventEmitter();
  isActiveWaze = new BehaviorSubject<boolean>(false);
  isActiveWazeAlertCrash = new EventEmitter();
  isActiveWazeAlertFlood = new EventEmitter();
  isActiveWazeAlertPothole = new EventEmitter();
  isCloseWazeInfo = new EventEmitter();
  isCloseWazeAlertInfo = new EventEmitter();
  isNullAlert = new BehaviorSubject<object>({
    crash: false,
    flood: false,
    pothole: false,
  });
  transparencyAlerts = new EventEmitter();
  transparencyJams = new EventEmitter();
  mapView: any;
  selectedAggregateJam = new BehaviorSubject<[]>([]);
  selectedCrash = new BehaviorSubject<[]>([]);
  selectedFlood = new BehaviorSubject<[]>([]);
  selectedJam = new BehaviorSubject<[]>([]);
  selectedPothole = new BehaviorSubject<[]>([]);

  public setActiveWazeJamType(data) {
    this.activeWazeJamType.emit(data);
  }

  public setIsActiveWaze(data) {
    this.isActiveWaze.next(data);
  }

  public setIsActiveWazeAlertCrash(data) {
    this.isActiveWazeAlertCrash.emit(data);
  }

  public setIsActiveWazeAlertFlood(data) {
    this.isActiveWazeAlertFlood.emit(data);
  }

  public setIsActiveWazeAlertPothole(data) {
    this.isActiveWazeAlertPothole.emit(data);
  }

  public setIsNullAlert(data) {
    this.isNullAlert.next(data);
  }

  public setTransparencyAlerts(data) {
    this.transparencyAlerts.emit(data);
  }

  public setTransparencyJams(data) {
    this.transparencyJams.emit(data);
  }

  public setMapView(data) {
    this.mapView = data;
  }

  public setSelectedAggregateJam(data) {
    this.selectedAggregateJam.next(data);
  }

  public setSelectedCrash(data) {
    this.selectedCrash.next(data);
  }

  public setSelectedFlood(data) {
    this.selectedFlood.next(data);
  }

  public setSelectedJam(data) {
    this.selectedJam.next(data);
  }

  public setSelectedPothole(data) {
    this.selectedPothole.next(data);
  }
}
