import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderSatupetaService {
  isLoading = new BehaviorSubject<boolean>(true);

  public setIsLoading(val) {
    this.isLoading.next(val);
  }
}
