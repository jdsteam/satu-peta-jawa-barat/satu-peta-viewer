import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ExploreMapsetService {
  isOpenModalSubject = new Subject<any>();
  choosedMapsetSubject = new BehaviorSubject([]);
  removeMapsetSubject = new Subject<any>();
  removeAllMapset = new EventEmitter();
  isCloseModalSubject = new Subject<any>();
  defaultMenuSubject = new BehaviorSubject<any>(null);
  defaultMapsetSubject = new BehaviorSubject<[]>([]);
  activeDetailMapSubject = new BehaviorSubject<any>(null);
  activeCategoryMenuSubject = new BehaviorSubject<any>(null);

  public setModal(isOpenModal) {
    this.isOpenModalSubject.next(isOpenModal);
  }

  public setChoosedMapset(mapset) {
    this.choosedMapsetSubject.next(mapset);
  }

  public setRemoveMapset(mapset) {
    this.removeMapsetSubject.next(mapset);
  }

  public setCloseModal(isClose) {
    this.isCloseModalSubject.next(isClose);
  }

  public setDefaultMenu(menu) {
    this.defaultMenuSubject.next(menu);
  }

  public setDefaultMapset(mapset) {
    this.defaultMapsetSubject.next(mapset);
  }

  public setRemoveAllMapset(data) {
    this.removeAllMapset.emit(data);
  }

  public setActiveDetailMap(id) {
    this.activeDetailMapSubject.next(id);
  }

  public setActiveCategoryMenu(category) {
    this.activeCategoryMenuSubject.next(category);
  }

  public getChoosedMapset() {
    return this.choosedMapsetSubject;
  }
}
