import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  slideNav = new BehaviorSubject('close');

  public setSlideNav(value) {
    this.slideNav.next(value);
  }

}
