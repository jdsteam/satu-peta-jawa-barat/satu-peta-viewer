import { EventEmitter, Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FilterFeatureMapService {
  filterUpdated = new EventEmitter();
  filterSubject = new Subject<any>();
  deleteFilterSubject = new Subject<any>();
  filterData: any;
  deleteFilterData: any;
  defaultSource: any;

  constructor() {
    this.filterData = [];
  }

  public setDefaultSource(source) {
    this.defaultSource = source;
    return this.defaultSource;
  }

  public getDefaultSource() {
    return this.defaultSource;
  }

  public setFilterData(data) {
    let filterData = this.filterData;

    filterData = filterData.filter((x) => {
      return x.id !== data.id;
    });

    filterData.push(data);
    this.filterData = filterData;
    this.filterSubject.next(this.filterData);
    this.filterUpdated.emit(this.filterData);
  }
}
