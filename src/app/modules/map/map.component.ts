import { Component, OnDestroy, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import {
  LoaderSatupetaService,
  MaintenancePopUpService,
  OnboardingSatuPetaService,
  UtilComponentService,
} from './services';
import { Store, select } from '@ngrx/store';
import { selectAllIp } from '@store/ip/ip.selectors';
import { filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';
import { fromIpActions } from '@store/ip/ip.actions';
import _ from 'lodash-es';
import { DeviceDetectorService } from 'ngx-device-detector';
import { LogService, TokenService } from '@services';
import { OnboardingSatuPetaComponent } from './components/onboarding-satu-peta/onboarding-satu-peta.component';
import { MaintenancePopUpComponent } from './components/maintenance-popup/maintenance-popup.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NavigationService } from './services/navigation.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit, OnDestroy {
  isLoading = false;
  cookiesKeyName = {
    ipaddress: 'od_df_i',
  };
  expiredDate = new Date();
  deviceInfo = null;
  modalonboardingSatuPeta: NgbModalRef;
  modalMaintenancePopUp: NgbModalRef;
  paramMapsetId = '';

  constructor(
    private route: ActivatedRoute,
    private utilComponentService: UtilComponentService,
    private tokenService: TokenService,
    private cookieService: CookieService,
    private store: Store<any>,
    private deviceService: DeviceDetectorService,
    private logService: LogService,
    private modalService: NgbModal,
    private onboardingSatuPetaService: OnboardingSatuPetaService,
    private maintenancePopUpService: MaintenancePopUpService,
    private navigationService: NavigationService,
    private loaderSatupetaService: LoaderSatupetaService
  ) {
    this.onboardingSatuPetaService.isCloseOnboardingSatuPeta.subscribe((res) => {
      if (res === true) {
        if (this.modalonboardingSatuPeta !== undefined) {
          this.modalonboardingSatuPeta.close();
        }
      }
    });

    this.maintenancePopUpService.isCloseMaintenancePopUp.subscribe((res) => {
      if (res === true) {
        if (this.modalMaintenancePopUp !== undefined) {
          this.modalMaintenancePopUp.close();
        }
      }
    });
  }

  ngOnInit() {
    this.initSubscription();
    this.initLogPage();
    this.loadIp();
    this.openStartUpPopUp();
  }

  ngOnDestroy() {
    if (this.modalonboardingSatuPeta !== undefined) {
      this.modalonboardingSatuPeta.close();
    }
  }

  initLogPage() {
    const device = this.deviceService.getDeviceInfo();
    _.set(device, 'isMobile', this.deviceService.isMobile());
    _.set(device, 'isTablet', this.deviceService.isTablet());
    _.set(device, 'isDesktop', this.deviceService.isDesktop());

    this.deviceInfo = this.logService.getDevice(device);
  }

  loadIp() {
    this.store.dispatch(fromIpActions.loadIp());
  }

  initSubscription() {
    this.utilComponentService.isLoading.subscribe((res) => {
      this.isLoading = res;
    });

    this.route.paramMap.subscribe((params: ParamMap) => {
      this.paramMapsetId = params.get('id');
    });

    this.subscribeIp();
  }

  subscribeIp() {
    if (!this.cookieService.hasKey(this.cookiesKeyName.ipaddress)) {
      this.store
        .pipe(
          select(selectAllIp),
          filter((val) => val.length !== 0)
        )
        .subscribe((result) => {
          const res = result[0];

          this.cookieService.put(
            this.cookiesKeyName.ipaddress,
            CryptoJS.AES.encrypt(res.ip, environment.jwtSecretKey).toString(),
            {
              expires: this.expiredDate,
            }
          );
          const token = this.tokenService.createToken(res.ip);
          this.logService.setLog({ ...this.deviceInfo, ...res });
          this.logService.setToken(token);
        });
    }
  }

  openStartUpPopUp() {
    if (environment.maintenanceMode) {
      this.openMaintenancePopUp();
    } else {
      this.openOnboardingSatuPeta();
    }
  }

  openOnboardingSatuPeta() {
    // tslint:disable-next-line:max-line-length
    const isAgree = this.cookieService.get('is-agree-onboarding-satu-peta')
      ? JSON.parse(this.cookieService.get('is-agree-onboarding-satu-peta'))
      : false;

    if (!isAgree && !this.paramMapsetId) {
      this.modalonboardingSatuPeta = this.modalService.open(OnboardingSatuPetaComponent, {
        backdropClass: 'modal_backdrop-feedback',
        windowClass: 'modal-onboarding-satupeta',
        ariaLabelledBy: 'modal-onboarding-satupeta',
        centered: true,
      });
    } else {
      this.navigationService.setSlideNav('open');
    }
  }

  openMaintenancePopUp() {
    this.modalMaintenancePopUp = this.modalService.open(MaintenancePopUpComponent, {
      backdropClass: 'modal_backdrop-feedback',
      windowClass: 'modal-maintenance-satupeta',
      ariaLabelledBy: 'modal-maintenance-satupeta',
      centered: true,
    });
  }
}
