import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { MapComponent } from './map.component';
import { EsriMapComponent } from './components/esri-map/esri-map.component';
import { DetailSelectedMapsetComponent } from './components/detail-selected-mapset/detail-selected-mapset.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { ExploreMapsetComponent } from './components/explore-mapset/explore-mapset.component';
import { MapsetSettingComponent } from './components/mapset-setting/mapset-setting.component';
import { FormLoginComponent } from './components/form-login/form-login.component';
import { BasemapSettingComponent } from './components/basemap-setting/basemap-setting.component';
import { ExportMapComponent } from './components/export-map/export-map.component';
import { NgxSpinnerService } from 'ngx-spinner';

import { FiltercardComponent } from './components/filtercard/filtercard.component';
import { DatatablexComponent } from '../../shared/components/datatablex/datatablex.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { WazeSettingComponent } from './components/waze-setting/waze-setting.component';
import { WazeInfoComponent } from './components/waze-info/waze-info.component';
import { WazeJamSettingComponent } from './components/waze-jam-setting/waze-jam-setting.component';
import { WazeAlertInfoComponent } from './components/waze-alert-info/waze-alert-info.component';
import { ExploreMapsetNavigationComponent } from './components/explore-mapset-navigation/explore-mapset-navigation.component';
import { ExploreMapsetMainComponent } from './components/explore-mapset-main/explore-mapset-main.component';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { OnboardingSatuPetaComponent } from './components/onboarding-satu-peta/onboarding-satu-peta.component';
import { LoaderSatupetaComponent } from './components/loader-satupeta/loader-satupeta.component';

export const routes: Routes = [
  {
    path: '',
    component: MapComponent,
  },
];

@NgModule({
  declarations: [
    MapComponent,
    NavigationComponent,
    FiltercardComponent,
    EsriMapComponent,
    DetailSelectedMapsetComponent,
    MenuBarComponent,
    ExploreMapsetComponent,
    MapsetSettingComponent,
    FormLoginComponent,
    BasemapSettingComponent,
    ExportMapComponent,
    DatatablexComponent,
    FeedbackComponent,
    WazeSettingComponent,
    WazeInfoComponent,
    WazeJamSettingComponent,
    WazeAlertInfoComponent,
    ExploreMapsetNavigationComponent,
    ExploreMapsetMainComponent,
    OnboardingSatuPetaComponent,
    LoaderSatupetaComponent,
  ],
  entryComponents: [FiltercardComponent, ExploreMapsetComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes), LoadingBarModule],
  providers: [NgxSpinnerService],
  bootstrap: [MapComponent],
})
export class MapModule { }
