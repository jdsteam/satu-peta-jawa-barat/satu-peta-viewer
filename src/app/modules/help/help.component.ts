import { Component } from '@angular/core';
import { faAngleUp, faAngleDown, faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
})
export class HelpComponent {
  isCollapsed = {
    goals: false,
    regulation: false,
  };

  icon = {
    faAngleUp,
    faAngleDown,
    faEnvelope,
    faPhone,
  };

  activeMenu = {
    parentId: 1,
    id: 1,
  };

  menus = [
    {
      id: 1,
      label: 'Satu Peta Jabar',
      isCollapsed: true,
      children: [
        {
          parentId: 1,
          id: 1,
          label: 'Apa itu Satu Peta Jabar',
        },
        {
          parentId: 1,
          id: 2,
          label: 'Mekanisme Pengumpulan Data',
        },
        {
          parentId: 1,
          id: 3,
          label: 'Klasifikasi Data',
        },
      ],
    },
    {
      id: 2,
      label: 'Data Geospasial',
      isCollapsed: true,
      children: [
        {
          parentId: 2,
          id: 1,
          label: 'Dataset Geospasial',
        },
        {
          parentId: 2,
          id: 2,
          label: 'Cara Mencari Dataset Geospasial',
        },
        {
          parentId: 2,
          id: 3,
          label: 'Pengaturan Data Geospasial yang ditampilkan',
        },
        {
          parentId: 2,
          id: 4,
          label: 'Melakukan Ekspor Data',
        },
        {
          parentId: 2,
          id: 5,
          label: 'Pengaturan Peta',
        },
      ],
    },
  ];

  setActiveMenu(parentId, id) {
    this.scroll(parentId, id);

    this.activeMenu = {
      parentId,
      id,
    };
  }

  checkActiveMenu(parentId, id) {
    if (parentId === this.activeMenu.parentId && id === this.activeMenu.id) {
      return true;
    }

    return false;
  }

  scroll(parentId, id) {
    const section = 'section-' + parentId + '-' + id;

    if (this.activeMenu.parentId !== parentId) {
      setTimeout(() => {
        const el = document.getElementById(section);
        window.scrollTo({
          top: el.offsetTop + 180,
          behavior: 'smooth',
        });
      }, 200);
    } else {
      const el = document.getElementById(section);
      window.scrollTo({
        top: el.offsetTop + 180,
        behavior: 'smooth',
      });
    }
  }
}
