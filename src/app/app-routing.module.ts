import { Routes } from '@angular/router';
import { RedirectGuard } from './core/guards/redirect.guard';

import { ContainerHomeComponent } from '@components/layout-2/container/container.component';
import { ContainerComponent } from '@components/layout-1/container/container.component';

export const AppRoutingModule: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: ContainerHomeComponent,
    loadChildren: () => import('./modules/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'map',
    loadChildren: () => import('./modules/map/map.module').then((m) => m.MapModule),
  },
  {
    path: 'thematic',
    component: ContainerHomeComponent,
    loadChildren: () => import('./modules/thematic/thematic.module').then((m) => m.ThematicModule),
  },
  {
    path: 'map/:id',
    loadChildren: () => import('./modules/map/map.module').then((m) => m.MapModule),
  },
  {
    path: 'about',
    component: ContainerComponent,
    loadChildren: () => import('./modules/about/about.module').then((m) => m.AboutModule),
  },
  {
    path: 'help',
    component: ContainerComponent,
    loadChildren: () => import('./modules/help/help.module').then((m) => m.HelpModule),
  },
  {
    path: 'webgis_desdm',
    canActivate: [RedirectGuard],
    component: RedirectGuard,
    data: {
      externalUrl:
        'https://arcgis.jabarprov.go.id/portal/sharing/oauth2/authorize?canHandleCrossOrgSignin=true&client_id=arcgisonline&response_type=token&state=%7B%22portalUrl%22%3A%22https%3A%2F%2Farcgis.jabarprov.go.id%2Fportal%22%7D&expiration=20160&redirect_uri=https%3A%2F%2Farcgis.jabarprov.go.id%2Fportal%2Fapps%2Fwebappviewer%2Findex.html%3Fid%3D5c4e21cec5654900800980816645c0bc&redirectToUserOrgUrl=true',
    },
  },
];
