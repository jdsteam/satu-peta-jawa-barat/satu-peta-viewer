import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { Ng5SliderModule } from 'ng5-slider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxLoadingModule } from 'ngx-loading';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatTreeModule } from '@angular/material/tree';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgClickOutsideDirective } from 'ng-click-outside2';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelectModule } from '@ng-select/ng-select';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { ContentLoaderModule } from '@ngneat/content-loader';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TruncatePipe } from './pipe/truncate.pipe';
import { ErrorRequestComponent } from './components/error-request/error-request.component';
import { SwiperModule } from 'swiper/angular';
import { Select2Module } from 'ng-select2-component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TableHeadComponent } from './components/table-head/table-head.component';
import { PaginationComponent } from './../core/components/pagination/pagination.component';

// Note we need a separate function as it's required
// by the AOT compiler.
export function playerFactory() {
  return player;
}

@NgModule({
  imports: [
    CommonModule,
    NgxSpinnerModule,
    Ng5SliderModule,
    NgbModule,
    FormsModule,
    FontAwesomeModule,
    Ng5SliderModule,
    NgxSkeletonLoaderModule,
    SweetAlert2Module,
    MatTreeModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    NgClickOutsideDirective,
    NgSelectModule,
    ContentLoaderModule,
    SwiperModule,
    LottieModule.forRoot({ player: playerFactory }),
    AngularSvgIconModule.forRoot(),
    NgxLoadingModule.forRoot({}),
    Select2Module,
    DragDropModule,
    NgbDropdownModule,
  ],
  exports: [
    Ng5SliderModule,
    NgxSpinnerModule,
    NgbModule,
    FormsModule,
    FontAwesomeModule,
    NgxSkeletonLoaderModule,
    MatTreeModule,
    MatIconModule,
    SweetAlert2Module,
    MatButtonModule,
    MatButtonToggleModule,
    NgClickOutsideDirective,
    NgSelectModule,
    TruncatePipe,
    LottieModule,
    ContentLoaderModule,
    ErrorRequestComponent,
    AngularSvgIconModule,
    SwiperModule,
    NgxLoadingModule,
    Select2Module,
    DragDropModule,
    TableHeadComponent,
    PaginationComponent,
  ],
  declarations: [TruncatePipe, ErrorRequestComponent, TableHeadComponent, PaginationComponent],
})
export class SharedModule {}
