import { Deserializable } from './deserializable';

export class App implements Deserializable {
  id: number;
  name: string;
  url: string;
  code: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
