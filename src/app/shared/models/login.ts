import { Deserializable } from './deserializable';
import { User } from './user';

export class Login implements Deserializable {
  data: User;
  error: number;
  messages: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
