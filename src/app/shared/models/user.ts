import { Deserializable } from './deserializable';
import { App } from './app';
import { Role } from './role';

export class User implements Deserializable {
  id: number;
  username: string;
  email: string;
  name: string;
  regional_level: string;
  kode_kemendagri: string;
  nama_kemendagri: string;
  kode_skpd: string;
  nama_skpd: string;
  kode_skpdsub: string;
  nama_skpdsub: string;
  kode_skpdunit: string;
  nama_skpdunit: string;
  cdate: Date;
  last_login: Date;
  is_deleted: boolean;
  role: Role;
  app: App;
  jwt?: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  get dateCreated(): Date {
    return new Date(this.cdate);
  }

  get dateLastLogin(): Date {
    return new Date(this.last_login);
  }
}
