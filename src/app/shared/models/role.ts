import { Deserializable } from './deserializable';

export class Role implements Deserializable {
  id: number;
  name: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
