import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-error-request',
  templateUrl: './error-request.component.html',
  styleUrls: ['./error-request.component.scss']
})
export class ErrorRequestComponent implements OnInit {
  @Input() errorMessages!: any;
  @Output() reloadTrigger = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  reload(): void {
    this.reloadTrigger.emit();
  }

}
