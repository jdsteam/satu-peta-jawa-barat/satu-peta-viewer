import { Component, Input, Output, EventEmitter } from '@angular/core';

import { map, filter } from 'lodash';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[jds-table-head]',
  templateUrl: './table-head.component.html',
  styleUrls: ['./table-head.component.scss'],
})
export class TableHeadComponent {
  @Input()
  get column(): string {
    return this._column;
  }
  set column(column: string) {
    this._column = column;
  }

  @Input()
  get currentSort(): any {
    return this._currentSort;
  }
  set currentSort(currentSort: any) {
    this._currentSort = currentSort;
  }

  @Input()
  get isSortable(): boolean {
    return this._isSortable;
  }
  set isSortable(isSortable: boolean) {
    this._isSortable = isSortable;
  }

  @Input()
  get isFilterable(): boolean {
    return this._isFilterable;
  }
  set isFilterable(isFilterable: boolean) {
    this._isFilterable = isFilterable;
  }

  @Input()
  get filterType(): string {
    return this._filterType;
  }
  set filterType(filterType: string) {
    this._filterType = filterType;
  }

  @Input()
  get filterOptions(): any[] {
    return this._filterOptions;
  }
  set filterOptions(filterOptions: any[]) {
    this._filterOptions = filterOptions;
    this.setFilter(filterOptions);
  }

  @Output() filterSort = new EventEmitter();
  @Output() filterFilter = new EventEmitter();

  private _column = '';
  private _isSortable = false;
  private _currentSort = {};
  private _isFilterable = false;
  private _filterType = 'checkbox';
  private _filterOptions: any[] = [];

  filterActive: any[] = [];

  sortClassname(column: string): string {
    if (this.currentSort.sort !== column) {
      return '';
    }
    return this.currentSort.direction;
  }

  filterClassname(filterActive: any): string {
    return filterActive.length > 0 ? 'active' : '';
  }

  changeSort(event: string): void {
    this.filterSort.emit(event);
  }

  setFilter(options: any[]): void {
    this.filterActive = map(
      filter(options, 'checked'),
      (result: any) => result.value,
    );
  }

  changeFilter(): void {
    this.setFilter(this.filterOptions);
    this.filterFilter.emit(this.filterActive);
  }
}
