import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { faSort, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';
import _ from 'lodash-es';

@Component({
  selector: 'app-datatablex',
  templateUrl: './datatablex.component.html',
  styleUrls: ['./datatablex.component.scss'],
})
export class DatatablexComponent implements OnInit, OnChanges {
  @Input() data = [];
  @Input() column = [];

  sort = {
    by: 'nama',
    order: 'desc',
  };
  perPage = 10;
  page = 1;
  startNum = 1;
  tableData = [];

  numberPagination = [];

  ngOnInit() {
    this.setPageDataTable();
    this.setNumberPagination();
  }

  ngOnChanges() {
    this.setPageDataTable();
    this.setNumberPagination();
  }

  setPageDataTable() {
    const data = [...this.data];
    this.tableData = data;
  }

  setNumberPagination() {
    const lastnum = this.data.length / this.perPage;
    const numberPagination = [];

    for (let i = 1; i <= lastnum; i++) {
      numberPagination.push(i);
    }

    this.numberPagination = numberPagination;
  }

  checkSortIcon(val) {
    if (val === this.sort.by) {
      if (this.sort.order === 'asc') {
        return faSortUp;
      } else {
        return faSortDown;
      }
    } else {
      return faSort;
    }
  }

  sortBy(val) {
    if (this.sort.by === val) {
      if (this.sort.order === 'asc') {
        this.sort.order = 'desc';
      } else {
        this.sort.order = 'asc';
      }
    } else {
      this.sort.by = val;
      this.sort.order = 'desc';
    }

    this.data = _.orderBy(this.data, [this.sort.by], [this.sort.order]);
    this.setPageDataTable();
  }

  changePage(ev) {
    this.page = ev;
    this.startNum = (this.page - 1) * this.perPage + 1;
    this.setPageDataTable();
  }

  changePerPage(ev) {
    this.perPage = ev;
    this.startNum = (this.page - 1) * this.perPage + 1;
    this.setPageDataTable();
  }
}
