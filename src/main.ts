import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import * as Sentry from '@sentry/angular';
import { Integrations } from '@sentry/tracing';

Sentry.init({
  environment: environment.environment,
  dsn: environment.sentryURL,
  integrations: [
    new Integrations.BrowserTracing({
      tracingOrigins: [
        'localhost',
        'https://satupeta-dev.digitalservice.id/api-backend',
        'https://satudata-dev.digitalservice.id/api-log',
        'https://satudata.digitalservice.id/api-backend',
        'https://satudata.digitalservice.id/api-log',
        'https://satudata.jabarprov.go.id/api-backend',
        'https://satudata.jabarprov.go.id/api-log',
      ],
      routingInstrumentation: Sentry.routingInstrumentation,
    }),
  ],

  tracesSampleRate: 0.1,
});

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .then((success) => console.log(`Bootstrap success`))
  .catch((err) => console.error(err));

