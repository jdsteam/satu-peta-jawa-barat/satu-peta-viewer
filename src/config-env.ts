const { writeFile } = require('fs');
const { argv } = require('yargs');

require('dotenv').config();

const environment = argv.environment;
const isProduction = environment === 'production';

let targetPath = '';
if (environment === 'development') {
  targetPath = `./src/environments/environment.dev.ts`;
} else if (environment === 'staging') {
  targetPath = `./src/environments/environment.stag.ts`;
} else if (environment === 'production') {
  targetPath = `./src/environments/environment.prod.ts`;
} else {
  targetPath = `./src/environments/environment.ts`;
}

const environmentFileContent = `
export const environment = {
  appCode: '${process.env.APP_CODE}',
  production: ${isProduction},
  refreshData: 30 * 60 * 1000,
  apiData: '${process.env.API_DATA}',
  apiAuth: '${process.env.API_AUTH}',
  apiLog: '${process.env.API_LOG}',
  apiCitarum: '${process.env.API_CITARUM}',
  apiCitarumKey: '${process.env.API_CITARUM_KEY}',
  apiWaze: '${process.env.API_WAZE}',
  apiWazeKey: '${process.env.API_WAZE_KEY}',
  hotjarId: '${process.env.HOTJAR_ID}',
  jwtAlgorithm: '${process.env.JWT_ALGORITHM}',
  jwtSecretKey: '${process.env.JWT_SECRET_KEY}',
  sentryURL: '${process.env.SENTRY_URL}',
  sipentasURL: '${process.env.SIPENTAS_URL}',
  sikambingURL: '${process.env.SIKAMBING_URL}',
  tematikURL: '${process.env.TEMATIK_URL}',
  environment: '${environment}',
  maintenanceMode: ${process.env.MAINTENANCE_MODE === 'true' ? true : false},
};
`;

writeFile(targetPath, environmentFileContent, (err) => {
  if (err) {
    console.log(err);
  }
  console.log(`Wrote variables to ${targetPath}`);
});
