FROM  node:14.15.0-alpine AS build-stage
WORKDIR /app/
COPY package*.json /app/
RUN apk update && apk upgrade && apk add --no-cache git
RUN npm install -g @angular/cli 
RUN npm install && npm cache clean --force
COPY ./ /app/
ARG configuration=development
RUN npm run build-dev


FROM nginx:1.13.3-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html/
